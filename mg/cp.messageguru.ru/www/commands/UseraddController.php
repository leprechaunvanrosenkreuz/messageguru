<?php

namespace app\commands;

use app\modules\user\models\User;
use yii\base\Model;
use yii\console\Controller;
use yii\console\Exception;
use yii\helpers\Console;
use Yii;
use yii\validators\EmailValidator;


class UseraddController extends Controller
{
    public function actionIndex()
    {
        echo 'yii users/create' . PHP_EOL;
        echo 'yii users/remove' . PHP_EOL;
        echo 'yii users/activate' . PHP_EOL;
        echo 'yii users/change-password' . PHP_EOL;
    }

    public function actionAddRole()
    {
        $auth = Yii::$app->authManager;


        $analyst = $auth->createRole('analyst');
        $auth->add($analyst);

        $admin = $auth->createRole('admin');
        $auth->add($admin);

        $createUser = $auth->createPermission('createUser');
        $createUser->description = 'Create a user';
        $auth->add($createUser);

        $updateUser = $auth->createPermission('updateUser');
        $updateUser->description = 'Update user';
        $auth->add($updateUser);

        $deleteUser = $auth->createPermission('deleteUser');
        $deleteUser->description = 'delete a user';
        $auth->add($deleteUser);

        $auth->addChild($admin, $createUser);
        $auth->addChild($admin, $updateUser);
        $auth->addChild($admin, $deleteUser);
    }

    public function actionAddManager()
    {
        $auth = Yii::$app->authManager;
        $manager = $auth->createRole('manager');
        $auth->add($manager);

        $chartMessage = $auth->createPermission('chartMessage');
        $chartMessage->description = 'chart messages';
        $auth->add($chartMessage);

        $auth->addChild($manager, $chartMessage);

        $admin = $auth->getRole('admin');
        $auth->addChild($admin, $chartMessage);
    }

    public function actionAddPermission()
    {
        $auth = Yii::$app->authManager;

        $createUser = $auth->createPermission('createUser');
        $createUser->description = 'Create a user';
        $auth->add($createUser);

        $updateUser = $auth->createPermission('updateUser');
        $updateUser->description = 'Update user';
        $auth->add($updateUser);

        $deleteUser = $auth->createPermission('deleteUser');
        $deleteUser->description = 'delete a user';
        $auth->add($deleteUser);

        $admin = $auth->getRole('admin');

        $auth->addChild($admin, $createUser);
        $auth->addChild($admin, $updateUser);
        $auth->addChild($admin, $deleteUser);
    }

    /**
     * доступ для всех разделов, используется в отображении меню
    */
    public function actionAddFullPermission()
    {
        $auth = Yii::$app->authManager;

        $systemRules = $auth->createPermission('system_rules');
        $systemRules->description = 'system rules list and create system rule';
        $auth->add($systemRules);

        $leadgens = $auth->createPermission('leadgens');
        $leadgens->description = 'leadgens management';
        $auth->add($leadgens);

        $callCenters = $auth->createPermission('call_centers');
        $callCenters->description = 'call centers management';
        $auth->add($callCenters);

        $smsRules = $auth->createPermission('sms_rules');
        $smsRules->description = 'sms rules management';
        $auth->add($smsRules);

        $claimsCities = $auth->createPermission('claims_cities');
        $claimsCities->description = 'claims cities management';
        $auth->add($claimsCities);

        $analytics = $auth->createPermission('analytics');
        $analytics->description = 'analytics management';
        $auth->add($analytics);

        $retrieval = $auth->createPermission('retrieval');
        $retrieval->description = 'retrieval sms, calls, claims';
        $auth->add($retrieval);

        $statistics = $auth->createPermission('statistics');
        $statistics->description = 'statistics';
        $auth->add($statistics);

        $test = $auth->createPermission('test');
        $test->description = 'autoinformators test';
        $auth->add($test);

        $applications = $auth->createPermission('applications');
        $applications->description = 'applications management';
        $auth->add($applications);

        $users = $auth->createPermission('users');
        $users->description = 'users list';
        $auth->add($users);


        $admin = $auth->getRole('admin');
        $auth->addChild($admin, $systemRules);
        $auth->addChild($admin, $leadgens);
        $auth->addChild($admin, $callCenters);
        $auth->addChild($admin, $smsRules);
        $auth->addChild($admin, $claimsCities);
        $auth->addChild($admin, $analytics);
        $auth->addChild($admin, $retrieval);
        $auth->addChild($admin, $statistics);
        $auth->addChild($admin, $test);
        $auth->addChild($admin, $applications);
        $auth->addChild($admin, $users);

        $analyst = $auth->getRole('analyst');
        $auth->addChild($analyst, $systemRules);
        $auth->addChild($analyst, $leadgens);
        $auth->addChild($analyst, $callCenters);
        $auth->addChild($analyst, $smsRules);
        $auth->addChild($analyst, $claimsCities);
        $auth->addChild($analyst, $analytics);
        $auth->addChild($analyst, $retrieval);
        $auth->addChild($analyst, $statistics);
        $auth->addChild($analyst, $test);
        $auth->addChild($analyst, $applications);
        $auth->addChild($analyst, $users);
    }

    public function actionCreate()
    {
        $model = new User();
        $type = $this->prompt('Type: ', ['required' => true]);

        $model->type = $type;

        $this->readValue($model, 'fio');
        $this->readValue($model, 'email');
        $this->readValue($model, 'phone');
        $model->setPassword($this->prompt('Password:', [
            'required' => true,
            'pattern' => '#^.{6,255}$#i',
            'error' => 'More than 6 symbols',
        ]));
        $model->generateAuthKey();

        if ($model->save(false)) {
            //$role = User::$roleByType[$type];

            $auth = Yii::$app->authManager;
            $partner = $auth->getRole($type);
            $auth->assign($partner, $model->getId());

            $this->log(true);
        }
        //$this->log($model->save());
    }

    public function actionRemove()
    {
        $email = $this->prompt('Email:', ['required' => true]);
        $model = $this->findModel($email);
        $this->log($model->delete());
    }

    public function actionActivate()
    {
        $email = $this->prompt('Email:', ['required' => true]);
        $model = $this->findModel($email);
        $model->removeEmailConfirmToken();
        $this->log($model->save(false));
    }

    public function actionChangePassword()
    {
        $email = $this->prompt('Email:', ['required' => true]);
        $model = $this->findModel($email);
        $model->setPassword($this->prompt('New password:', [
            'required' => true,
            'pattern' => '#^.{8,255}$#i',
            'error' => 'More than 8 symbols',
        ]));
        $this->log($model->save(false));
    }

    /**
     * @param string $email
     * @throws \yii\console\Exception
     * @return User the loaded model
     */
    private function findModel($email)
    {
        if (!$model = User::findOne(['email' => $email])) {
            throw new Exception('User not found');
        }
        return $model;
    }

    /**
     * @param Model $model
     * @param string $attribute
     */
    private function readValue($model, $attribute)
    {
        $model->$attribute = $this->prompt(mb_convert_case($attribute, MB_CASE_TITLE, 'utf-8') . ':', [
            'validator' => function ($input, &$error) use ($model, $attribute) {
                $model->$attribute = $input;
                if ($model->validate([$attribute])) {
                    return true;
                } else {
                    $error = implode(',', $model->getErrors($attribute));
                    return false;
                }
            },
        ]);
    }

    /**
     * Массовая регистрация
     */
    public function actionMassregister()
    {
        $file1 = $this->openCsv('list_users.csv');

        $i = 1;
        while (($filedata = fgetcsv($file1, 5000, ";")) !== false) {
            $name = trim($this->convertToUTF($filedata[0]));
            $email = trim($filedata[1]);

            $password = Yii::$app->security->generateRandomString(8);

            $validator = new EmailValidator();

            if (!$validator->validate($email, $error)) {
                $this->stderr('Error! Email ' . $email . ' is not valid' . PHP_EOL, Console::FG_RED, Console::BOLD);
                continue;
            }

            $model = new User();

            $model->type = User::TYPE_ANALYST;
            $model->fio = $name;
            $model->email = $email;
            $model->phone = '79000000000';
            $model->setPassword($password);
            $model->generateAuthKey();


            if ($model->save(false)) {
                $auth = Yii::$app->authManager;
                $partner = $auth->getRole('analyst');
                $auth->assign($partner, $model->getId());

                $this->stdout('Success! Email ' . $email . ' is successfully register' . PHP_EOL, Console::FG_GREEN, Console::BOLD);

                //Активация
                $model_act = $this->findModel($email);
                $model_act->removeEmailConfirmToken();
                if ($model_act->save(false)) {
                    $this->stdout('Success! User ' . $email . ' is successfully activate' . PHP_EOL, Console::FG_BLUE, Console::BOLD);

                    $text = "Добрый день, " . $name . '<br/><br/>';
                    $text .= "Вы зарегистрированы в системе СДП<br/>";
                    $text .= "Запишите реквизиты ниже или сохраните это письмо: <br/><br/>";
                    $text .= "<b>Логин: " . $email . "</b><br/>";
                    $text .= "<b>Пароль: " . $password . "</b><br/>";
                    $text .= "Ссылка для входа: https://cp.landings.skbbank.ru/login<br/><br/>";


                    Yii::$app->mailer->compose()
                        //->setFrom('mamonovsv@skbbank.ru')
                        ->setFrom('site2@skbbank.ru')
                        ->setTo($email)
                        //->setTo('manchetten@gmail.com')
                        ->setSubject('Ваши реквизиты для доступа к СДП')
                        ->setHtmlBody($text)
                        ->send();
                    $this->stdout('Success! Email confirm ' . $email . ' is successfully sent' . PHP_EOL, Console::FG_PURPLE, Console::BOLD);
                }

            }
            $i++;
        }

    }

    /**
     * Открывает csv и возвращает указатель на файл
     * @param $file
     * @return bool|resource
     */
    public function openCsv($file)
    {
        if (!empty($file)) {
            $file_name = Yii::getAlias('@runtime') . '/' . $file;

            $dirname = dirname($file_name);

            if (!is_dir($dirname)) {
                mkdir($dirname, 0777, true);
            }

            $handle = fopen($file_name, 'r');

            return $handle;

        } else
            return false;
    }

    /**
     * Конвертиция форматов из windows-1251 в utf8
     * @param $data
     * @return string
     */
    public function convertToUTF($data)
    {
        return iconv('windows-1251', 'utf8', $data);
    }

    /**
     * @param bool $success
     */
    private function log($success)
    {
        if ($success) {
            $this->stdout('Success!', Console::FG_GREEN, Console::BOLD);
        } else {
            $this->stderr('Error!', Console::FG_RED, Console::BOLD);
        }
        echo PHP_EOL;
    }
    /**
     * create new Permission to select roles
    */
    public function actionAddNewPermission(){
        $auth = Yii::$app->authManager;

        $permissionName = $this->prompt('Permission: ', ['required' => true]);
        $permission =  $auth->createPermission($permissionName);
        $permissionDesc = $this->prompt('Permission description: ', ['required' => true]);
        $permission->description = $permissionDesc;
        $auth->add($permission);

        $rolesName = $this->prompt('Roles (enter through the gap): ', ['required' => true]);
        $rolesNameArray = explode(' ', $rolesName);
        foreach ($rolesNameArray as $roleName)
        {
            $role = $auth->getRole($roleName);
            $auth->addChild($role, $permission);
            $this->stdout("For role $roleName was add permission $permissionName \n", Console::FG_GREEN);
        }
        $this->stdout("The end \n", Console::FG_PURPLE);
    }
}