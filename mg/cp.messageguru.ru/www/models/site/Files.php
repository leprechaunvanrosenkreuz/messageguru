<?php

namespace app\models\site;

use app\components\MgHelpers;
use Yii;
use app\modules\user\models\User;
use app\models\Likes;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\db\Command;
use himiklab\yii2\search\behaviors\SearchBehavior;
use himiklab\thumbnail\EasyThumbnailImage;
use yii\helpers\ArrayHelper;


/**
 * This is the model class for table "files".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $cat_id
 * @property string $name
 * @property string $desc
 * @property integer $type
 * @property integer $views
 * @property integer $image
 * @property string $title
 * @property string $created_at
 * @property string $updated_at
 * @property integer $status
 * @property string $slug
 * @property string $mail
 *
 * @property FilesTags[] $filesTags
 */
class Files extends \yii\db\ActiveRecord
{
    public $presFile;
    public $imageFile;
    public $tags;

    public $loadStep;

    public $mail;

    const TYPE_PRESENTATION = 1;
    const TYPE_YOUTUBE = 2;
    const TYPE_ARTICLE = 3;

    const STATUS_OFF = 0;
    const STATUS_ON = 1;

    const LOAD_STEP_FIRST = 1;
    const LOAD_STEP_SECOND = 2;

    public static function getTypesList()
    {
        return [
            self::TYPE_PRESENTATION => 'Презентация',
            //self::TYPE_YOUTUBE => 'Youtube',
            //self::TYPE_ARTICLE => 'Статья',
        ];
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db_site');
    }

    public static function getStatusList()
    {
        return [
            self::STATUS_OFF => 0,
            self::STATUS_ON => 1
        ];
    }

    public static function getFilesList()
    {
        $f = self::find()->select(['id', 'title'])->where(['status' => self::STATUS_ON])->all();
        return ArrayHelper::map($f, 'id', 'title');
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
                'value' => new Expression('NOW()'),
            ],
//            'search' => [
//                'class' => SearchBehavior::className(),
//                'searchScope' => function ($model) {
//                    /** @var \yii\db\ActiveQuery $model */
//                    $model->select(['title', 'desc']);
//                    //$model->andWhere(['indexed' => true]);
//                },
//                'searchFields' => function ($model) {
//                    /** @var self $model */
//                    return [
//                        ['name' => 'title', 'value' => $model->title, 'type' => SearchBehavior::FIELD_KEYWORD],
//                        ['name' => 'desc', 'value' => strip_tags($model->desc), 'type' => SearchBehavior::FIELD_TEXT],
////                        ['name' => 'alias', 'value' => $model->alias],
////                        ['name' => 'cat_id', 'value' => $model->cat_id],
////                        ['name' => 'city', 'value' => $model->city],
////                        ['name' => 'date', 'value' => $model->date],
////                        ['name' => 'image', 'value' => $model->image],
////                        ['name' => 'type', 'value' => 'event'],
//                        // ['name' => 'model', 'value' => 'page', 'type' => SearchBehavior::FIELD_UNSTORED],
//                    ];
//                }
//            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%files}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'cat_id', 'type', 'views', 'status'], 'integer'],
            [['desc', 'title', 'created_at', 'updated_at', 'slug'], 'string'],
            ['tags', 'each', 'rule' => ['string'], 'on' => 'step2'],
            [['name', 'image', 'title'], 'string', 'max' => 255],
            [['user_id'], 'required', 'message' => 'Заполните поле'],
            [['image'], 'required', 'message' => 'Выберите главное изображение презентации из списка слева', 'on' => 'step2'],
            [['name', 'title'], 'required', 'message' => 'Заполните поле', 'on' => 'step2'],
            ['desc', 'string', 'max' => 1000, 'message' => 'Заполните описание от 500 до 1000 знаков', 'on' => 'step2'],
            [['presFile'], 'file', 'extensions' => ['pdf', 'pptx', 'ppt'],
                'checkExtensionByMimeType' => true,
                'mimeTypes' => [
                    'application/pdf',
                    'application/vnd.oasis.opendocument.presentation',
                    'application/vnd.ms-powerpoint',
                    'application/mspowerpoint',
                    'application/powerpoint',
                    'application/x-mspowerpoint',
                    'application/vnd.openxmlformats-officedocument.presentationml.presentation'
                ],
                'skipOnEmpty' => false
            ],
            [['presFile', 'tags'], 'safe', 'on' => 'step2'],
            [['presFile'], 'safe', 'on' => 'step1video'],
            [['tags', 'loadStep'], 'safe']
        ];
    }

    public function beforeValidate()
    {
        return parent::beforeValidate();
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'cat_id' => 'Cat ID',
            'name' => 'Name',
            'desc' => 'Desc',
            'title' => 'Title',
            'status' => 'Status',
            'type' => 'Type'
        ];
    }

    public function saveTags()
    {
        if (!empty($this->tags)) {
            foreach ($this->tags AS $val) {
                if (!is_numeric($val)) {
                    $stripTag = MgHelpers::removeHashtags($val);

                    //Добавляем новый тег
                    $modelTags = new Tags();
                    $modelTags->alias = $stripTag;
                    $modelTags->name = $stripTag;
                    $modelTags->save();

                    $this->link('filesTags', $modelTags); //и линкуем
                } else {
                    $tags_db = Tags::findOne($val);

                    if (!empty($tags_db)) {
                        $this->link('filesTags', $tags_db); //линкуем с каждым найденным тегом
                    }
                }
            }
            return true;
        } else {
            return false;
        }
    }

    public static function removeFiles($file_id)
    {
        $model = self::findOne($file_id);
        $model->status = self::STATUS_OFF;

        return $model->update(false);
    }

    public static function getThumbName($filename)
    {
        if (!is_file($filename)) {
            return self::getFileName($filename) . "_thumb.png";
        } else {
            //todo сюда картинку по дефолту поставить
            return "";
        }
    }


    public static function getFileName($filename)
    {
        if (!is_file($filename)) {
            return explode(".", $filename)[0];
        } else {
            return "";
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFilesTags()
    {
        return $this
            ->hasMany(Tags::className(), ['id' => 'tags_id'])
            ->viaTable(FilesTags::tableName(), ['files_id' => 'id']);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this
            ->hasMany(User::className(), ['id' => 'user_id']);
    }

    /*    public function getFilesTags()
        {
            return $this
                ->hasMany(Tags::className(), ['files_id' => 'id' ])
                ->viaTable(FilesTags::tableName(), ['files_tags' => 'id']);
        }*/
}