<?php

namespace app\models\site;

use Yii;

/**
 * This is the model class for table "files_tags".
 *
 * @property integer $id
 * @property integer $files_id
 * @property integer $tags_id
 *
 * @property Tags $tags
 * @property Files $files
 */
class FilesTags extends \yii\db\ActiveRecord
{
    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db_site');
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'files_tags';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['files_id', 'tags_id'], 'integer'],
            [['tags_id'], 'exist', 'skipOnError' => true, 'targetClass' => Tags::className(), 'targetAttribute' => ['tags_id' => 'id']],
            [['files_id'], 'exist', 'skipOnError' => true, 'targetClass' => Files::className(), 'targetAttribute' => ['files_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'files_id' => 'Files ID',
            'tags_id' => 'Tags ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTags()
    {
        return $this->hasOne(Tags::className(), ['id' => 'tags_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFiles()
    {
        return $this->hasOne(Files::className(), ['id' => 'files_id']);
    }
}
