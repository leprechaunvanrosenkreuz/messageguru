<?php

namespace app\models\site;

use Yii;

/**
 * This is the model class for table "{{%likes}}".
 *
 * @property integer $id
 * @property integer $obj_id
 * @property integer $count
 * @property integer $user_by
 * @property string $created_at
 * @property string $updated_at
 */
class Likes extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%likes}}';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db_site');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['obj_id', 'user_by'], 'required'],
            [['obj_id', 'count', 'user_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'obj_id' => 'Obj ID',
            'count' => 'Count',
            'user_by' => 'User By',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
