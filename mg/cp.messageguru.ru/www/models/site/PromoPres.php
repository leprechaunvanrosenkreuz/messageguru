<?php

namespace app\models\site;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;


/**
 * This is the model class for table "promo_pres".
 *
 * @property integer $id
 * @property integer $order
 * @property integer $file_id
 * @property string $created_at
 * @property string $updated_at
 */
class PromoPres extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'promo_pres';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db_site');
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order', 'file_id'], 'integer'],
            [['file_id'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'order' => 'Порядок',
            'file_id' => 'Файл презентации',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    public function behaviors()
    {
        return
            [
                'sortable' => [
                    'class' => \kotchuprik\sortable\behaviors\Sortable::className(),
                    'query' => self::find(),
                ],
                [
                    'class' => TimestampBehavior::className(),
                    'attributes' => [
                        ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                        ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                    ],
                    'value' => new Expression('NOW()'),
                ],
            ];
    }
}