<?php

namespace app\models\site;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%user}}".
 *
 * @property integer $id
 * @property integer $type
 * @property integer $status
 * @property string $auth_key
 * @property string $email_confirm_token
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $avatar
 * @property string $login
 * @property string $first_name
 * @property string $last_name
 * @property string $middle_name
 * @property string $email
 * @property string $phone
 * @property string $organization
 * @property string $site
 * @property string $vk
 * @property string $fb
 * @property string $ok
 * @property string $youtube
 * @property string $lk
 * @property string $tw
 * @property string $profession
 * @property integer $city_id
 * @property integer $rating
 * @property string $created_at
 * @property string $updated_at
 * @property string $desc
 *
 * @property UserAchievements[] $userAchievements
 * @property UserEvents[] $userEvents
 */
class User extends \yii\db\ActiveRecord
{
    //User status
    const STATUS_BLOCKED = 0;
    const STATUS_ACTIVE = 1;
    const STATUS_WAIT = 2;

    public function getStatusName()
    {
        return ArrayHelper::getValue(self::getStatusesArray(), $this->status);
    }

    public static function getStatusesArray()
    {
        return [
            self::STATUS_BLOCKED => 'Заблокирован',
            self::STATUS_ACTIVE => 'Активен',
            self::STATUS_WAIT => 'Ожидает подтверждения',
        ];
    }


    //User types
    const TYPE_SPEAKER = 1;
    const TYPE_VIEWER = 2;

    public static function getTypesArray()
    {
        return [
            self::TYPE_SPEAKER => 'Спикер',
            self::TYPE_VIEWER => 'Гость',
        ];
    }

    public static function getUsersList()
    {
        return ArrayHelper::map(self::find()->all(), 'id', 'login');
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user}}';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db_site');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type', 'status', 'city_id', 'rating'], 'integer'],
            [['password_hash', 'login', 'first_name', 'last_name', 'middle_name', 'email', 'organization'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['auth_key'], 'string', 'max' => 32],
            [['email_confirm_token', 'password_hash', 'password_reset_token', 'avatar', 'login', 'first_name', 'last_name', 'middle_name', 'email', 'phone', 'organization', 'site', 'vk', 'fb', 'ok', 'youtube', 'lk', 'tw', 'profession', 'desc'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Тип',
            'status' => 'Статус',
            'auth_key' => 'Auth Key',
            'email_confirm_token' => 'Email Confirm Token',
            'password_hash' => 'Password Hash',
            'password_reset_token' => 'Password Reset Token',
            'avatar' => 'Аватар',
            'login' => 'Логин',
            'first_name' => 'Имя',
            'last_name' => 'Фамилия',
            'middle_name' => 'Отчество',
            'email' => 'Email',
            'phone' => 'Телефон',
            'organization' => 'Организация',
            'site' => 'Сайт',
            'vk' => 'Vk',
            'fb' => 'Fb',
            'ok' => 'Ok',
            'youtube' => 'Youtube',
            'lk' => 'LinkedIn',
            'tw' => 'Twitter',
            'profession' => 'Профессия',
            'city_id' => 'Город',
            'rating' => 'Рейтинг',
            'created_at' => 'Создан',
            'updated_at' => 'Обновлен',
            'desc' => 'Описание',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserAchievements()
    {
        return $this->hasMany(UserAchievements::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserEvents()
    {
        return $this->hasMany(UserEvents::className(), ['user_id' => 'id']);
    }
}
