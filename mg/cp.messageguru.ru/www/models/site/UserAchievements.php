<?php

namespace app\models\site;

use Yii;

/**
 * This is the model class for table "{{%user_achievements}}".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $ach_id
 *
 * @property Achievements $ach
 * @property User $user
 */
class UserAchievements extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user_achievements}}';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db_site');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'ach_id'], 'integer'],
            [['ach_id'], 'exist', 'skipOnError' => true, 'targetClass' => Achievements::className(), 'targetAttribute' => ['ach_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'ach_id' => 'Ach ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAch()
    {
        return $this->hasOne(Achievements::className(), ['id' => 'ach_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
