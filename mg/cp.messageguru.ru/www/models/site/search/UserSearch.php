<?php

namespace app\models\site\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\site\User;

/**
 * UserSearch represents the model behind the search form about `app\models\site\User`.
 */
class UserSearch extends User
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'type', 'status', 'city_id', 'rating'], 'integer'],
            [['auth_key', 'email_confirm_token', 'password_hash', 'password_reset_token', 'avatar', 'login', 'first_name', 'last_name', 'middle_name', 'email', 'phone', 'organization', 'site', 'vk', 'fb', 'ok', 'youtube', 'lk', 'tw', 'profession', 'created_at', 'updated_at', 'desc'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = User::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'type' => $this->type,
            'status' => $this->status,
            'city_id' => $this->city_id,
            'rating' => $this->rating,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'auth_key', $this->auth_key])
            ->andFilterWhere(['like', 'email_confirm_token', $this->email_confirm_token])
            ->andFilterWhere(['like', 'password_hash', $this->password_hash])
            ->andFilterWhere(['like', 'password_reset_token', $this->password_reset_token])
            ->andFilterWhere(['like', 'avatar', $this->avatar])
            ->andFilterWhere(['like', 'login', $this->login])
            ->andFilterWhere(['like', 'first_name', $this->first_name])
            ->andFilterWhere(['like', 'last_name', $this->last_name])
            ->andFilterWhere(['like', 'middle_name', $this->middle_name])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'phone', $this->phone])
            ->andFilterWhere(['like', 'site', $this->site])
            ->andFilterWhere(['like', 'vk', $this->vk])
            ->andFilterWhere(['like', 'fb', $this->fb])
            ->andFilterWhere(['like', 'ok', $this->ok])
            ->andFilterWhere(['like', 'youtube', $this->youtube])
            ->andFilterWhere(['like', 'lk', $this->lk])
            ->andFilterWhere(['like', 'tw', $this->tw])
            ->andFilterWhere(['like', 'profession', $this->profession])
            ->andFilterWhere(['like', 'desc', $this->desc]);

        return $dataProvider;
    }
}
