<?php
/**
 * Created by PhpStorm.
 * User: manchetten
 * Date: 08.07.16
 * Time: 11:06
 */
namespace app\modules\admin;

use yii;

/**
 * User module
 */
class Module extends \yii\base\Module
{

    public $controllerNamespace = 'app\modules\admin\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
        \Yii::$app->view->theme = new \yii\base\Theme([
            //'pathMap' => ['@app/views' => '@vendor/dmstr/yii2-adminlte-asset/example-views/yiisoft/yii2-app'],
            'pathMap' => [
                '@app/views' => '@app/views/admin_lte',
            ]

            //'baseUrl' => '@web/themes/admin',
        ]);

        //$this->layoutPath = \Yii::getAlias('@app/views/classic/views/layouts/');
        //$this->layout = 'userLayout';
    }
}