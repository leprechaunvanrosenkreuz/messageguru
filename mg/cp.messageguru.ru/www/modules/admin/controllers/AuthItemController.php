<?php

namespace app\modules\admin\controllers;

use Yii;
use app\models\admin\AuthItem;
use app\models\admin\AuthItemChild;
use app\models\admin\AuthItemSearch;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\db\Expression;

/**
 * AuthItemController implements the CRUD actions for AuthItem model.
 */
class AuthItemController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all AuthItem models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new AuthItemSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single AuthItem model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new AuthItem model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new AuthItem();
        $data = Yii::$app->request->post();
        $postModel = new AuthItemChild();
        //$model->rule_name ? $model->rule_name : new Expression('NULL');
        if ($data) {
            //$model->rule_name = new Expression('NULL');
            //$model->data = new Expression('NULL');
        }
        //$model->data ? $model->data : new Expression('NULL');
        if ($model->load(Yii::$app->request->post()) && $model->save()) {

            if ($model->type == 1) {
                $rules = $data['AuthItem']['rules'];
                $postModel::deleteAll(['parent' => $model->name]);
                $bulkInsertArray = array();
                foreach ($rules as $rule) {
                    array_push($bulkInsertArray, [
                        'parent' => $model->name,
                        'child' => $rule,
                    ]);
                }
                Yii::$app->db->createCommand()->batchInsert($postModel::tableName(), $postModel->attributes(), $bulkInsertArray)->execute();
            }
            return $this->redirect(['view', 'id' => $model->name]);
        } else {
            $model->type = 1;
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing AuthItem model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $data = Yii::$app->request->post();
        $loaded = $model->load($data);

        $postModel = new AuthItemChild();
        if ($loaded && $model->save()) {
            if ($model->type == 1) {
                $rules = $data['AuthItem']['rules'];
                $postModel::deleteAll(['parent' => $model->name]);
                $bulkInsertArray = array();
                foreach ($rules as $rule) {
                    array_push($bulkInsertArray, [
                        'parent' => $model->name,
                        'child' => $rule,
                    ]);
                }
                Yii::$app->db->createCommand()->batchInsert($postModel::tableName(), $postModel->attributes(), $bulkInsertArray)->execute();
            }
            return $this->redirect(['view', 'id' => $model->name]);
        } else {
            $model->rules = ArrayHelper::map(AuthItemChild::find()->where(['parent' => $model->name])->all(), 'child', 'child');
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing AuthItem model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        /*if ($model->type == 1) {
            AuthItemChild::deleteAll(['parent' => $model->name]);
        }*/
        $model->delete();
        return $this->redirect(['index']);
    }

    /**
     * Finds the AuthItem model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return AuthItem the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = AuthItem::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
