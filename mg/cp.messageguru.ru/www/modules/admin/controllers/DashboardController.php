<?php
/**
 * Created by PhpStorm.
 * User: manchetten
 * Date: 26.07.16
 * Time: 2:08
 */
namespace app\modules\admin\controllers;

use app\models\site\Files;
use app\models\site\Likes;
use app\models\site\User;
use app\modules\admin\components\AdminController;

class DashboardController extends AdminController
{
    public function actionIndex()
    {
        $stat = [
            'users' => User::find()->count(),
            'files' => Files::find()->count(),
            'likes' => Likes::find()->count()
        ];

        return $this->render('index', ['stat' => $stat]);
    }

}