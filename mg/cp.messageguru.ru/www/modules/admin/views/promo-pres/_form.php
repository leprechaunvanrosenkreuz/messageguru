<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model app\models\site\PromoPres */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="promo-pres-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'file_id')->widget(Select2::classname(), [
        'data' => \app\models\site\Files::getFilesList(),
        'language' => 'ru',
        'options' => ['multiple' => false, 'placeholder' => 'Выберите презентацию']
    ]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Сохранить' : 'Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
