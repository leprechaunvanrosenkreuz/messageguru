<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\site\PromoPres */

$this->title = 'Create Promo Pres';
$this->params['breadcrumbs'][] = ['label' => 'Promo Pres', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="promo-pres-create">

    <div class="box">
        <div class="box-body">

            <?= $this->render('_form', [
                'model' => $model,
            ]) ?>

        </div>
    </div>
</div>
