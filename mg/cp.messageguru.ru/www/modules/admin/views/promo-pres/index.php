<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use app\models\site\Files;

/* @var $this yii\web\View */
/* @var $searchModel app\models\site\search\PromoPresSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Промо презентаций';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="promo-pres-index">

    <div class="box">
        <div class="box-body">
            <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

            <?php Pjax::begin(); ?>
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'rowOptions' => function ($model, $key, $index, $grid) {
                    return ['data-sortable-id' => $model->id];
                },
                'columns' => [

                    [
                        'class' => \kotchuprik\sortable\grid\Column::className(),
                    ],
                    'id',
                    'order',
                    [
                        'label'=>'Презентация',
                        'attribute' => 'file_id',
                        'format' => 'raw',
                        'value'=>function ($model, $key, $index, $column) {
                            $file = Files::findOne($model->file_id);
                            return $file->title;
                        },
                    ],

                    [
                        'class' => 'yii\grid\ActionColumn',
                        'template' => '{view} {update}',
                    ],
                ],
                'options' => [
                    'data' => [
                        'sortable-widget' => 1,
                        'sortable-url' => \yii\helpers\Url::toRoute(['sorting']),
                    ]
                ],
            ]); ?>
            <?php Pjax::end(); ?>
        </div>
    </div>
</div>
