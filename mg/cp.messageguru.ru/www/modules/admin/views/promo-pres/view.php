<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\site\PromoPres */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Promo Pres', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="promo-pres-view">

    <div class="box">
        <div class="box-body">

            <p>
                <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            </p>

            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'id',
                    'order',
                    'file_id',
                    'created_at',
                    'updated_at',
                ],
            ]) ?>

        </div>
    </div>
</div>
