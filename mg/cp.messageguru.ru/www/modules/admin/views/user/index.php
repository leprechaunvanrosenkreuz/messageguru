<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use app\models\site\User;

/* @var $this yii\web\View */
/* @var $searchModel app\models\site\search\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Пользователи';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">
    <div class="box">
        <div class="box-body">
            <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

            <?php Pjax::begin(); ?>
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    'id',
                    //'type',
                    [
                        'header' => 'Статус',
                        'filter' => User::getStatusesArray(),
                        'attribute' => 'status',
                        'value' => 'StatusName',
                    ],
                    // 'password_hash',
                    // 'password_reset_token',
                    [
                        'attribute' => 'avatar',
                        'format' => 'html',
                        'value' => function ($data) {
                            return Html::img($data->avatar, ['width' => '70px']);
                        },
                    ],
                    'login',
                    'first_name',
                    'email:email',
                    'phone',
                    // 'organization',
                    // 'site',
                    // 'vk',
                    // 'fb',
                    // 'ok',
                    // 'youtube',
                    // 'lk',
                    // 'tw',
                    // 'profession',
                    // 'city_id',
                    // 'rating',
                    'created_at',
                    // 'updated_at',


                    ['class' => 'yii\grid\ActionColumn'],
                ],
            ]); ?>
            <?php Pjax::end(); ?>
        </div>
    </div>
</div>
