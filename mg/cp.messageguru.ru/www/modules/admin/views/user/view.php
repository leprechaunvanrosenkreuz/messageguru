<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\site\User */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Пользователи', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-view">

    <div class="box">
        <div class="box-body">

            <p>
                <?= Html::a('Отредактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => 'Are you sure you want to delete this item?',
                        'method' => 'post',
                    ],
                ]) ?>
            </p>

            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'id',
                    'type',
                    'status',
                    'auth_key',
                    'email_confirm_token:email',
                    'password_hash',
                    'password_reset_token',
                    'avatar',
                    'login',
                    'first_name',
                    'last_name',
                    'middle_name',
                    'email:email',
                    'phone',
                    'organization',
                    'site',
                    'vk',
                    'fb',
                    'ok',
                    'youtube',
                    'lk',
                    'tw',
                    'profession',
                    'city_id',
                    'rating',
                    'created_at',
                    'updated_at',
                    'desc',
                ],
            ]) ?>

        </div>
    </div>
</div>
