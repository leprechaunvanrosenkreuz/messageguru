$('.reset-password-request').on('click', function(){
    var email = $('#loginform-email').val();
    var url = $(this).find('a').attr('href');
    $.ajax({
        url: url,
        data: {'email': email},
        success: function (data) {
            $('#alert').removeClass();
            $('#alert').addClass('alert alert-'+data.status);
            $('#alert').html(data.message);
        }
    });
    return false;
})