<?php


namespace app\modules\user\controllers;

use app\modules\user\models\User;
use yii;
use yii\bootstrap\ActiveForm;

use yii\web\Controller;
use yii\web\Response;
use app\modules\user\models\forms\LoginForm;
use app\modules\user\models\forms\RegistrationForm;
use app\modules\user\models\forms\EmailConfirmForm;

use app\modules\user\models\forms\PasswordResetRequestForm;
use app\modules\user\models\forms\PasswordResetForm;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;
use yii\web\HttpException;
use app\modules\user\components\UserController;

class DoController extends UserController
{
    public $layoutPath = '@app/modules/user/views/layouts';
    public $adminLayoutPath = '@app/modules/admin/views/layouts';

    public function actionLogin()
    {

        /** @var LoginForm $model */
        $model = new LoginForm();

        $this->performAjaxValidation($model);

        if ($model->load(Yii::$app->getRequest()->post()) && $model->login()) {
            return $this->redirect(['/admin/dashboard/index']);
        }

        return $this->render('@app/modules/user/views/login', [
            'model' => $model,
            'layoutPath' => $this->layoutPath,
        ]);
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->redirect(['/user/do/login']);
    }


    protected function performAjaxValidation($model)
    {
        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }
    }

    public function actionResetPasswordRequest($email = null)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $email = Yii::$app->request->get('email');

        if ($email !== null AND strlen($email) > 0) {
            if (LoginForm::sendEmail($email)) {
                return ['status' => 'success', 'message' => 'Ваш запрос отправлен'];
            }
        } else {
            return ['status' => 'info', 'message' => 'Укажите email адрес'];
        }
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        if (\Yii::$app->user->can('createUser')) {
            $model = new RegistrationForm();

            if ($model->load(Yii::$app->request->post()) && $model->registration()) {
                return $this->redirect(['/admin/users']);
            }

            return $this->render('@app/modules/user/views/create', [
                'model' => $model,
                'adminLayoutPath' => $this->adminLayoutPath,
            ]);
        } else {
            throw new HttpException(403, 'У Вас нет прав на просмотр данной страницы');
        }

    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'list' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        if (\Yii::$app->user->can('updateUser') OR \Yii::$app->user->id == $id) {
            $model = $this->findModel($id);
            $model->setScenario('user_update');

            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                return $this->redirect(['/admin/users']);
            } else {
                return $this->render('@app/modules/user/views/update', [
                    'model' => $model,
                    'adminLayoutPath' => $this->adminLayoutPath,
                ]);
            }
        } else {
            throw new HttpException(403, 'У Вас нет прав на просмотр данной страницы');
        }
    }

    /**
     * Delete an existing User model.
     * If update is successful, the browser will be redirected to the 'list' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        if (\Yii::$app->user->can('deleteUser')) {
            $model = $this->findModel($id);

            $model->delete();
            return $this->redirect(['/admin/users']);
        } else {
            throw new HttpException(403, 'У Вас нет прав на просмотр данной страницы');
        }
    }


    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}