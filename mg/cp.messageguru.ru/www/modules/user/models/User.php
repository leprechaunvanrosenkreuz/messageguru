<?php

namespace app\modules\user\models;

use yii;
use yii\base\NotSupportedException;
use yii\web\IdentityInterface;
use app\components\validators\mobileValidator;
use yii\helpers\ArrayHelper;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;
use app\models\admin\AuthItem;

/**
 * This is the model class for table "{{%user}}".
 *
 * @property integer $id
 * @property string $type
 * @property string $fio
 * @property string $email
 * @property string $phone
 * @property string $password_hash
 * @property string $auth_key
 * @property string $access_token
 * @property string $vk_access_token
 * @property integer $confirmed_at
 * @property string $unconfirmed_email
 * @property integer $blocked_at
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $flags
 */
class User extends yii\db\ActiveRecord implements IdentityInterface
{

    //User types
    const TYPE_ADMIN = 1;
    const TYPE_MANAGER = 2;
    public $type_name;

    public $currentPassword;
    public $newPassword;
    public $newPasswordConfirm;

    public static $typesNames = [
        self::TYPE_ADMIN => 'Администратор',
        self::TYPE_MANAGER => 'Менеджер'
    ];
    public static $roleByType = [
        self::TYPE_ADMIN => 'admin',
        self::TYPE_MANAGER => 'manager'
    ];

    public static function getRolesList()
    {
        return AuthItem::getRolesList();
    }

    public function getRole()
    {
        return AuthItem::getRolesList()[$this->type];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user}}';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type', 'fio', 'email', 'phone', 'password_hash', 'auth_key', 'created_at', 'updated_at'], 'required'],
            [['type', 'email'], 'string', 'max' => 255],
            ['phone', mobileValidator::className(), 'message' => 'Номер мобильного телефона указан неверно'],

            [['password_hash'], 'string', 'max' => 60],
            [['auth_key'], 'string', 'max' => 32],
            [['email'], 'unique'],

            // password rules
            [['newPassword'], 'string', 'min' => 8, 'tooShort' => 'Пароль должен содержать минимум 8 символов'],
            ['newPassword', 'match', 'pattern' => '/([A-Z]|[А-Я]){1}/', 'message' => 'В пароле должны присутствовать строчные и прописные буквы и цифры'],
            [['newPassword'], 'filter', 'filter' => 'trim'],
            [['newPasswordConfirm'], 'compare', 'compareAttribute' => 'newPassword', 'message' => 'Пароль и его подтверждение не совпадают'],

//            [['currentPassword', 'newPasswordConfirm'], 'required', 'when' => function ($this) {
//                return $this->newPassword != '';
//            }, 'whenClient' => "function check_integer(attribute, value) {
//                return $('input[name=\"User[newPassword]\"]').val() != '';
//            }", 'message' => 'Пожалуйста, заполните поле'],

            // user update page
            //  [['currentPassword'], 'required', 'on' => ['user_update']],
            [['currentPassword'], 'validateCurrentPassword', 'on' => ['user_update']],
        ];
    }

    /**
     * Validate current password (account page)
     */
    public function validateCurrentPassword()
    {
        if (!$this->validatePassword($this->currentPassword)) {
            $this->addError("currentPassword", "Текущий пароль введен неверно");
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Тип',
            'type_name' => 'Роль',
            'fio' => 'ФИО',
            'email' => 'Email',
            'phone' => 'Телефон',
            'password_hash' => 'Password Hash',
            'auth_key' => 'Auth Key',
            'registration_ip' => 'Registration Ip',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'currentPassword' => 'Текущий пароль',
            'newPassword' => 'Новый пароль',
            'newPasswordConfirm' => 'Подтвердите новый пароль',
        ];
    }


    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id]);
    }

    /**
     * Finds an identity by the given token.
     *
     * @param string $token the token to be looked for
     * @return IdentityInterface|null the identity object that matches the given token.
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        //throw new NotSupportedException('findIdentityByAccessToken is not implemented.');
        return static::findOne(['access_token' => $token]);
    }


    /**
     * @inheritdoc
     */
    /*
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return static::findOne(['auth_key' => $token]);
        //throw new NotSupportedException('findIdentityByAccessToken is not implemented.');
    }
    */

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Finds user by email
     *
     * @param string $email
     * @return static|null
     */
    public static function findByEmail($email)
    {
        return static::findOne(['email' => $email]);
    }


    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }
        return static::findOne([
            'password_reset_token' => $token,
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return boolean
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }
        //$expire = Yii::$app->params['user.passwordResetTokenExpire'];
        $expire = 24 * 60 * 60;
        $parts = explode('_', $token);
        $timestamp = (int)end($parts);
        return $timestamp + $expire >= time();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }

    /**
     * @param string $email_confirm_token
     * @return static|null
     */
    public static function findByEmailConfirmToken($email_confirm_token)
    {
        return static::findOne(['email_confirm_token' => $email_confirm_token]);
    }

    /**
     * Generates email confirmation token
     */
    public function generateEmailConfirmToken()
    {
        $this->email_confirm_token = Yii::$app->security->generateRandomString();
    }

    /**
     * Removes email confirmation token
     */
    public function removeEmailConfirmToken()
    {
        $this->email_confirm_token = null;
    }

    public static function isAdmin($id)
    {
        $u = static::findOne(['id' => $id]);
        if ($u->type == User::TYPE_ADMIN)
            return true;
        else
            return false;
    }

    /**
     * Этот метод вызывается перед сохранением записи (после проверки, если таковые имеются).
     */
    public function beforeSave($insert)
    {
        $dirtyAttributes = $this->getDirtyAttributes();
        if (isset($dirtyAttributes["password"])) {
            $this->newPassword = $dirtyAttributes["password"];
        }

        // hash new password if set
        if ($this->newPassword) {
            $this->setPassword($this->newPassword);
        }


        // ensure fields are null so they won't get set as empty string
        $nullAttributes = ["email", "fio", "phone", "type"];
        foreach ($nullAttributes as $nullAttribute) {
            $this->$nullAttribute = $this->$nullAttribute ? $this->$nullAttribute : null;
        }

        if (parent::beforeSave($insert)) {
            $this->generateAuthKey();
            return true;
        } else {
            return false;
        }
    }


    public function afterSave($insert, $changedAttributes)
    {
        $auth = Yii::$app->authManager;
        $authorRole = $auth->getRole($this->type);

        $auth->revokeAll($this->getId());
        $auth->assign($authorRole, $this->getId());

        parent::afterSave($insert, $changedAttributes);
    }

    public function beforeDelete()
    {
        if (parent::beforeDelete()) {
            $auth = Yii::$app->authManager;
            $auth->revokeAll($this->getId());
            return true;
        } else {
            return false;
        }
    }

    /**
     * Возвращает названия ролей
     * @return array
     */
    public static function getTypesNames()
    {
        return self::$typesNames;
    }

    /**
     * Возврвщает название роли
     * @return mixed
     */
    public function getTypeName()
    {
        return ArrayHelper::getValue(self::getTypesNames(), $this->type);
    }

    /**
     * Возвращает имя текущего пользователя
     * @param $id
     * @return string
     */
    public static function getFio($id)
    {
        if (!empty($user = self::findOne(['id' => $id])))
            return $user->fio;
        else
            return '';
    }

    /**
     * Возвращает короткое имя текущего пользователя
     * @param $id
     * @return string
     */
    public static function getShortFio($id)
    {
        $user = self::findOne(['id' => $id]);
        if (!empty($user)) {
            $sh = explode(" ", $user->fio);
            return $sh[0];
        } else
            return '';
    }

    /**
     * Возвращает email текущего пользователя
     * @param $id
     * @return string
     */
    public static function getEmail($id)
    {
        $user = self::findOne(['id' => $id]);
        if (!empty($user)) {
            return $user->email;
        } else
            return '';
    }
}