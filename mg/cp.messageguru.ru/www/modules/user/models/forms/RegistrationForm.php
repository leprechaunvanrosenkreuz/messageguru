<?php

namespace app\modules\user\models\forms;

use app\modules\user\models\User;
use Yii;
use yii\base\Model;
use app\components\validators\mobileValidator;

/**
 * Registration Form
 */
class RegistrationForm extends Model
{
    public $password;
    public $password_confirm;
    public $phone;
    public $email;
    public $last_name;
    public $first_name;
    public $middle_name;
    public $fullname;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['password', 'required', 'message' => 'Пожалуйста, укажите пароль'],
            ['password', 'string', 'min' => 8, 'tooShort' => 'Пароль должен содержать минимум 8 символов'],
            ['password', 'match', 'pattern'=>'/([A-Z]|[А-Я]){1}/', 'message' => 'В пароле должны присутствовать строчные и прописные буквы и цифры'],
            ['password', 'match', 'pattern'=>'/\d+/', 'message' => 'В пароле должны присутствовать строчные и прописные буквы и цифры'],

            ['password_confirm', 'required', 'message' => 'Пожалуйста, повторите пароль'],
            ['password_confirm', 'string', 'min' => 6],

            ['phone', 'required', 'message' => 'Пожалуйста, укажите номер мобильного телефона'],
            ['phone', mobileValidator::className(), 'message' => 'Номер мобильного телефона указан неверно'],

            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required', 'message' => 'Пожалуйста, укажите электронный адрес'],
            ['email', 'email', 'message' => 'Пожалуйста, укажите корректный электронный адрес'],
            ['email', 'unique', 'targetClass' => User::className(), 'message' => 'Данный электронный адрес уже был использован.'],

            ['last_name', 'required', 'message' => 'Пожалуйста, укажите фамилию'],
            ['first_name', 'required', 'message' => 'Пожалуйста, укажите имя'],

            ['middle_name', 'safe'],

            ['fullname', 'required', 'message' => 'Пожалуйста, укажите фамилию имя отчество'],
            ['fullname', 'match', 'message' => 'Только русские буквы', 'pattern' => '/^[а-яА-ЯёЁ\s-]+$/u'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'password' => 'Пароль',
            'password_confirm' => 'Повторите пароль',
            'phone' => 'Номер телефона',
            'email' => 'Адрес электронной почты',
            'fullname' => 'ФИО',
        ];
    }

    /**
     * Registration user
     *
     * @return User|null the saved model or null if saving fails
     */
    public function registration($email = null)
    {
        if ($this->validate()) {
            $user = is_null($email) ? new User() : User::findByEmail($email);

            $user->fio = $this->fullname;
            $user->email = $this->email;
            $user->phone = $this->phone;
            $user->type = User::TYPE_ANALYST;

            $user->setPassword($this->password);
            $user->generateAuthKey();
            $user->generateEmailConfirmToken();
            $user->save(false);

            return $user;
        }
        return null;
    }


    public function beforeValidate()
    {
        if (!empty($this->fullname)) {
            $names = explode(' ', $this->fullname);
            $this->last_name = '';
            $this->first_name = '';
            $this->middle_name = '';

            ksort($names);

            foreach ($names as $key => $item) {
                if (empty($item))
                    unset($names[$key]);
                else
                    $bufer[] = $names[$key];
            }

            $names = $bufer;

            if (isset($names[0]))
                $this->last_name = $names[0];
            if (isset($names[1]))
                $this->first_name = $names[1];
            if (isset($names[2]))
                $this->middle_name = $names[2];
            if (isset($names[3]))
                $this->middle_name .= ' ' . $names[3];
        }

        return parent::beforeValidate();
    }

    public function afterValidate()
    {
        $last = $this->getErrors('last_name');
        if (!empty($last[0]))
            $this->addError("name", $last[0]);
        $first = $this->getErrors('first_name');
        if (!empty($first[0]))
            $this->addError("name", $first[0]);
        $middle = $this->getErrors('middle_name');
        if (!empty($middle[0]))
            $this->addError("name", $middle[0]);

        parent::afterValidate();
    }
}