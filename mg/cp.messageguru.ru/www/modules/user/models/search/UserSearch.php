<?php

namespace app\modules\user\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\user\models\User;


class UserSearch extends Model
{
    public $email;
    public $fio;
    public $phone;
    public $type_name;

    public function rules()
    {
        return [
            [['fio', 'email', 'phone', 'type_name'], 'safe'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'email' => 'Email',
            'fio' => '���',
            'phone' => '�������',
            'type_name' => '����'
        ];
    }

    public function search($params)
    {

        $query = User::find()->orderBy('fio ASC');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => false
        ]);

        $this->load($params);

        if (!$this->validate()) {
            $query->where('0=1');
            return $dataProvider;
        }


        $query
            ->andFilterWhere(['like', 'fio', $this->fio])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'type', $this->type_name])
            ->andFilterWhere(['like', 'phone', $this->phone]);

        return ['dataProvider' => $dataProvider];
    }
}