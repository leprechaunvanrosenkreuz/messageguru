<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\user\models\User */

$this->title = 'Создание пользователя';
$this->params['breadcrumbs'][] = ['label' => 'Пользователи', 'url' => ['/admin/users']];
$this->params['breadcrumbs'][] = 'Создание';
?>

<div class="user-create">
    <div class="box">
        <div class="box-body">

            <?php $form = ActiveForm::begin([
                'layout' => 'horizontal',
                'id' => 'registration-form',
                //'enableAjaxValidation' => true,
                'enableClientValidation' => false,
                'validateOnBlur' => false,
                'validateOnType' => false,
                'validateOnChange' => false,
            ]) ?>

            <?= $form->field($model, 'fullname') ?>
            <?= $form->field($model, 'email') ?>
            <?= $form->field($model, 'phone')->widget(\yii\widgets\MaskedInput::classname(), [
                'mask' => '+7 (999) 999-99-99',
            ]) ?>
            <?= $form->field($model, 'password')->passwordInput() ?>
            <?= $form->field($model, 'password_confirm')->passwordInput() ?>


            <div class="row">
                <div class="col-md-1 col-md-offset-2">
                    <?= Html::a('Отмена', ['/admin/users'], ['class' => 'btn btn-primary']) ?>
                </div>
                <div class="col-md-4">
                    <?= Html::submitButton('Сохранить', ['class' => 'btn  btn-success']) ?>
                </div>
            </div>
            <?php ActiveForm::end(); ?>
        </div>

    </div>

</div>

