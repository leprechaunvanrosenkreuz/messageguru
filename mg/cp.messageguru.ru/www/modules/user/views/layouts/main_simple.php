<?php
use app\assets\AppAsset;
use app\modules\user\assets\UserAsset;

AppAsset::register($this);
UserAsset::register($this);

$assetsUrl = AppAsset::getAssetsUrl();

?>

<?= $content ?>
