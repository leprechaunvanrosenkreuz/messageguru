<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use app\modules\user\models\User;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\user\models\search\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title = 'Пользователи';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="users-list-index">

    <div class="box-body">
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'layout' => "{items}\n{pager}",
            'tableOptions' => [
                'class' => 'table table-striped table-hover'
            ],
            'columns' => [
                [
                    'filter' => User::getTypesNames(),
                    'attribute' => 'type_name',
                    'value' => 'TypeName'
                ],
                'fio',
                'email',
                'phone',

                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{update}{delete}',
                    'headerOptions' => ['width' => '100'],
                    'buttons' => [
                        'update' => function ($url, $model) {
                            $url_update = Url::to(['/user/do/update', 'id' => $model->id]);
                            return \Yii::$app->user->can('updateUser') ? Html::a(
                                '<span class="glyphicon glyphicon-pencil"></span>',
                                $url_update) : '';
                        },
                        'delete' => function ($url, $model) {
                            $url_delete = Url::to(['/user/do/delete', 'id' => $model->id]);
                            return \Yii::$app->user->can('deleteUser') ? Html::a(
                                '<span class="glyphicon glyphicon-trash"></span>',
                                $url_delete, ['data-confirm' => 'Вы уверены, что хотите удалить этого пользователя?', 'data-method' => 'post']) : '';
                        },
                    ],
                    'contentOptions' => [
                        'class' => 'action-block',
                        'data-method' => 'post'
                    ]
                ],
            ],
        ]); ?>
    </div>
</div>
</div>


