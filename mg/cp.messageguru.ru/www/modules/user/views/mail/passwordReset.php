<?php

use yii\helpers\Html;
use yii\helpers\Url;
use app\modules\user\assets\UserAsset;


UserAsset::register($this);
?>


<table bgcolor="#f7f9ee"
       style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; width: 100%; margin: 0; padding: 0;">
    <tr style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;">
        <td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 16px; line-height: 1.6; margin: 0; padding: 20px; width: 100%; text-align: center">
            <h2 style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 18px; color: #00675d;">
                Запрос на восстановление доступа в систему администрирования аналитики ДДП
            </h2>
            <p>
                Email пользователя: <?= $email?>
            </p>


        </td>
    </tr>
</table>


