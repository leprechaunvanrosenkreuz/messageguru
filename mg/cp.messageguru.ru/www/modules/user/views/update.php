<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\user\models\User */

$this->title = 'Редактирование пользователя';
$this->params['breadcrumbs'][] = ['label' => 'Пользователи', 'url' => ['/admin/users']];
$this->params['breadcrumbs'][] = 'Редактирование';
?>

<div class="user-update">
    <div class="box">
        <div class="box-body">

            <?php $form = ActiveForm::begin([
                'layout' => 'horizontal',
                'id' => 'user-form',
                'options' => ['enctype' => 'multipart/form-data'],
            ]);
            ?>

            <?= $form->field($model, 'fio')->textInput(['maxlength' => true]) ?>
            <?php if (\Yii::$app->user->can('updateUser')) { ?>
                <?= $form->field($model, 'type')->radioList($model->getTypesNames()) ?>
            <?php } ?>
            <?= $form->field($model, 'phone')->widget(\yii\widgets\MaskedInput::classname(), [
                'mask' => '+7 (999) 999-99-99',
            ]) ?>

            <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'currentPassword')->passwordInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'newPassword')->passwordInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'newPasswordConfirm')->passwordInput(['maxlength' => true]) ?>

            <div class="row">
                <div class="col-md-1 col-md-offset-2">
                    <?= Html::a('Отмена', ['/admin/users'], ['class' => 'btn btn-primary']) ?>
                </div>
                <div class="col-md-4">
                    <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
                </div>
            </div>

            <?php ActiveForm::end(); ?>

        </div>

    </div>

</div>
