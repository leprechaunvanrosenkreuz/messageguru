<?php

$vendorDir = dirname(__DIR__);

return array (
  'yiisoft/yii2-swiftmailer' => 
  array (
    'name' => 'yiisoft/yii2-swiftmailer',
    'version' => '2.0.6.0',
    'alias' => 
    array (
      '@yii/swiftmailer' => $vendorDir . '/yiisoft/yii2-swiftmailer',
    ),
  ),
  'yiisoft/yii2-bootstrap' => 
  array (
    'name' => 'yiisoft/yii2-bootstrap',
    'version' => '2.0.6.0',
    'alias' => 
    array (
      '@yii/bootstrap' => $vendorDir . '/yiisoft/yii2-bootstrap',
    ),
  ),
  'yiisoft/yii2-debug' => 
  array (
    'name' => 'yiisoft/yii2-debug',
    'version' => '2.0.9.0',
    'alias' => 
    array (
      '@yii/debug' => $vendorDir . '/yiisoft/yii2-debug',
    ),
  ),
  'yiisoft/yii2-gii' => 
  array (
    'name' => 'yiisoft/yii2-gii',
    'version' => '2.0.5.0',
    'alias' => 
    array (
      '@yii/gii' => $vendorDir . '/yiisoft/yii2-gii',
    ),
  ),
  'yiisoft/yii2-faker' => 
  array (
    'name' => 'yiisoft/yii2-faker',
    'version' => '2.0.3.0',
    'alias' => 
    array (
      '@yii/faker' => $vendorDir . '/yiisoft/yii2-faker',
    ),
  ),
  'yiisoft/yii2-jui' => 
  array (
    'name' => 'yiisoft/yii2-jui',
    'version' => '2.0.6.0',
    'alias' => 
    array (
      '@yii/jui' => $vendorDir . '/yiisoft/yii2-jui',
    ),
  ),
  'execut/yii2-base' => 
  array (
    'name' => 'execut/yii2-base',
    'version' => '1.1.5.0',
    'alias' => 
    array (
      '@execut/yii' => $vendorDir . '/execut/yii2-base',
    ),
  ),
  'execut/yii2-widget-bootstraptreeview' => 
  array (
    'name' => 'execut/yii2-widget-bootstraptreeview',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@execut/widget' => $vendorDir . '/execut/yii2-widget-bootstraptreeview',
    ),
  ),
  'rmrevin/yii2-fontawesome' => 
  array (
    'name' => 'rmrevin/yii2-fontawesome',
    'version' => '2.17.1.0',
    'alias' => 
    array (
      '@rmrevin/yii/fontawesome' => $vendorDir . '/rmrevin/yii2-fontawesome',
    ),
  ),
  'cebe/yii2-gravatar' => 
  array (
    'name' => 'cebe/yii2-gravatar',
    'version' => '1.1.0.0',
    'alias' => 
    array (
      '@cebe/gravatar' => $vendorDir . '/cebe/yii2-gravatar/cebe/gravatar',
    ),
  ),
  'dmstr/yii2-adminlte-asset' => 
  array (
    'name' => 'dmstr/yii2-adminlte-asset',
    'version' => '2.3.4.0',
    'alias' => 
    array (
      '@dmstr' => $vendorDir . '/dmstr/yii2-adminlte-asset',
    ),
  ),
  'kotchuprik/yii2-sortable-widgets' => 
  array (
    'name' => 'kotchuprik/yii2-sortable-widgets',
    'version' => '2.0.2.0',
    'alias' => 
    array (
      '@kotchuprik/sortable' => $vendorDir . '/kotchuprik/yii2-sortable-widgets',
    ),
  ),
  'kartik-v/yii2-krajee-base' => 
  array (
    'name' => 'kartik-v/yii2-krajee-base',
    'version' => '1.8.8.0',
    'alias' => 
    array (
      '@kartik/base' => $vendorDir . '/kartik-v/yii2-krajee-base',
    ),
  ),
  'kartik-v/yii2-widget-select2' => 
  array (
    'name' => 'kartik-v/yii2-widget-select2',
    'version' => '2.0.8.0',
    'alias' => 
    array (
      '@kartik/select2' => $vendorDir . '/kartik-v/yii2-widget-select2',
    ),
  ),
  'kartik-v/yii2-widget-fileinput' => 
  array (
    'name' => 'kartik-v/yii2-widget-fileinput',
    'version' => '1.0.5.0',
    'alias' => 
    array (
      '@kartik/file' => $vendorDir . '/kartik-v/yii2-widget-fileinput',
    ),
  ),
  'yiisoft/yii2-imagine' => 
  array (
    'name' => 'yiisoft/yii2-imagine',
    'version' => '2.1.0.0',
    'alias' => 
    array (
      '@yii/imagine' => $vendorDir . '/yiisoft/yii2-imagine',
    ),
  ),
  'himiklab/yii2-easy-thumbnail-image-helper' => 
  array (
    'name' => 'himiklab/yii2-easy-thumbnail-image-helper',
    'version' => '1.0.3.0',
    'alias' => 
    array (
      '@himiklab/thumbnail' => $vendorDir . '/himiklab/yii2-easy-thumbnail-image-helper',
    ),
  ),
  'cornernote/yii2-linkall' => 
  array (
    'name' => 'cornernote/yii2-linkall',
    'version' => '1.0.0.0',
    'alias' => 
    array (
      '@cornernote/linkall' => $vendorDir . '/cornernote/yii2-linkall/src',
    ),
  ),
  'alexantr/yii2-elfinder' => 
  array (
    'name' => 'alexantr/yii2-elfinder',
    'version' => '1.1.2.0',
    'alias' => 
    array (
      '@alexantr/elfinder' => $vendorDir . '/alexantr/yii2-elfinder/src',
    ),
  ),
  '2amigos/yii2-tinymce-widget' => 
  array (
    'name' => '2amigos/yii2-tinymce-widget',
    'version' => '1.1.1.0',
    'alias' => 
    array (
      '@dosamigos/tinymce' => $vendorDir . '/2amigos/yii2-tinymce-widget/src',
    ),
  ),
);
