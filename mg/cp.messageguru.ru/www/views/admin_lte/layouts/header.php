<?php
use yii\helpers\Html;
use app\modules\user\models\User;
use yii\helpers\Url;
use app\modules\admin\assets\AdminAsset;

/* @var $this \yii\web\View */
/* @var $content string */

$fio_full = User::getFio(Yii::$app->user->id);
$fio_short = User::getShortFio(Yii::$app->user->id);

AdminAsset::register($this);
$assetsAdmUrl = AdminAsset::getAssetsUrl();
?>

<header class="main-header">

    <?= Html::a('<span class="logo-mini">MG</span><span class="logo-lg">MessageGuru</span>', Yii::$app->homeUrl, ['class' => 'logo']) ?>

    <nav class="navbar navbar-static-top" role="navigation">

        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Hide navigation</span>
        </a>

        <div class="navbar-custom-menu">

            <ul class="nav navbar-nav">

                <!-- User Account: style can be found in dropdown.less -->

                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <span class="hidden-xs"><?= $fio_short ?></span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header">
                            <p>
                                <?= $fio_full ?>
                            </p>
                        </li>
                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <div class="pull-left">
                                <a href="<?= Url::toRoute(['/user/do/update', 'id' => Yii::$app->user->id]) ?>"
                                   class="btn btn-default btn-flat">My account</a>
                            </div>
                            <div class="pull-right">
                                <?= Html::a(
                                    'Logout',
                                    ['/user/do/logout'],
                                    ['data-method' => 'post', 'class' => 'btn btn-default btn-flat']
                                ) ?>
                            </div>
                        </li>
                    </ul>
                </li>

                <!-- User Account: style can be found in dropdown.less -->

            </ul>
        </div>
    </nav>
</header>
