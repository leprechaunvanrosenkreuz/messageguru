<?php
use app\modules\user\models\User;
use cebe\gravatar\Gravatar;
use yii\helpers\Html;

?>
<aside class="main-sidebar">

    <section class="sidebar">

        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image" style="height: 40px">
                <?php echo Gravatar::widget(['email' => User::getEmail(Yii::$app->user->id), 'defaultImage' => 'identicon']); ?>
            </div>
            <div class="pull-left info">
                <p><?= User::getShortFio(Yii::$app->user->id) ?></p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>

        <?= dmstr\widgets\Menu::widget(
            [
                'encodeLabels' => false,
                'options' => ['class' => 'sidebar-menu'],
                'items' => [
                    [
                        'label' => 'Управление пользователями',
                        'icon' => 'fa fa-address-book',
                        'url' => ['/admin/user/index'],
                    ],
                    [
                        'label' => 'Управление файлами',
                        'icon' => 'fa fa-files-o',
                        'url' => ['/admin/files/index'],
                    ],
                    [
                        'label' => 'Управление промо',
                        'icon' => 'fa fa-money',
                        'url' => ['/admin/promo-pres/index'],
                    ]
                ]
            ]
        ) ?>

    </section>

</aside>
