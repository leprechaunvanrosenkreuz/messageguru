<?php
/**
 * Created by PhpStorm.
 * User: manchetten
 * Date: 18.07.16
 * Time: 10:12
 */

namespace app\modules\admin\assets;

use yii\web\AssetBundle;
use Yii;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AdminAsset extends AssetBundle
{
    /*
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/site.css',
    ];
    public $js = [
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
*/
    public $sourcePath = '@app/modules/admin/assets';

    public $css = [
        'https://fonts.googleapis.com/css?family=PT+Sans:400,400i,700&subset=cyrillic',
        'css/styles.css',
    ];
    public $js = [
        'js/speakingurl.js',
        'js/slugify.js',
        //'js/app.js'
//        'js/script.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];

    public $jsOptions = [
        'position' => \yii\web\View::POS_END
    ];

    public static function getAssetsUrl()
    {
        return Yii::$app->assetManager->getPublishedUrl((new self())->sourcePath);
    }
}