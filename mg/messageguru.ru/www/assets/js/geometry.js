/**
 * Created by it_somebody on 13.03.17.
 */

$( document ).ready(function() {

    function getsize() {
        //register page

        if ($(window).height()>$('.flex_container').height()) {
            $('.first_bg, .second_bg').height($(window).height()/2);
            $('.flex_container').css('top', ($(window).height()-$('.flex_container').height())/2);
        }
        else {
            $('.flex_container').css('top', '100px');
        //    $('body').css('height', ($('.flex_container').height()+400));
            $('.first_bg, .second_bg').height($('.flex_container').height()/2+100);
        }

        //user_inf fix
        if($("div").is(".user_menu")) {
            if ($('.user_menu').html().trim() === ''){
                $('.user_menu').hide();
            }
        }

        //Mobile menu
        
        $('.mobile_menu').click(function () {
            $('.mobile_menu_window, .mobile_menu_off').show();
            $('.mobile_menu').hide();
        })

        $('.mobile_menu_off').click(function () {
            $('.mobile_menu_window, .mobile_menu_off').hide();
            $('.mobile_menu').show();
        })
        $('.full_user_info').click(function () {
            $('.user_info, .user_menu').show();
            $('.full_user_info').hide();
        })

        //locked_screen

        $('.locked_screen .wrapper').css('margin-top', ($(window).height()-300)/2       );


        $('.video_view').height($('.video_view').width()*9/16);

    }


    $(window).resize(function(){
        getsize();
    });
    getsize();

});