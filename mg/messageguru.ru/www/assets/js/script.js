/**
 * Created by it_somebody on 12.03.17.
 */

$(document).ready(function () {
    //forms
    $('.form-control').after("<span class='underline_form'></span>");

    $('.form-control').bind('mouseover focus', function () {
        $(this).parent().find('.underline_form').animate({
            width: "100%",
            opacity: 1
        }, 300);
    });
    $('.form-control').bind('mouseleave focusout', function () {
        $(this).parent().find('.underline_form').animate({
            width: "0%",
            opacity: 0
        }, 200);
    })


//Register form

    $('.register_step1 .btn').click(function () {
        $('.register_step1, .register_cta').fadeOut(400);
        setTimeout(function () {
            $('.register_step2').fadeIn(500);
        }, 500);
        $('.register_bg').animate({
            'background-position-x': '-519px'
        }, 300, function () {
            $('.add_photo_contaner').fadeIn(500);
        });
    })


    //Search form
    $('.search input').mouseover(function () {
        $('.search_mask').css('opacity', '0.85');
    })
    $('.search input').focus(function () {
        $('.search_mask').css('opacity', '0.65');
    })
    $('.search').mouseleave(function () {
        $('.search_mask').css('opacity', '0.95');
    })
    $('.search_buttion').mousedown(function () {
        $(".search_buttion input").animate({top: "-3px"}, 100);
        $(".search_in").animate({top: "1px"}, 100);
    })
    $('.search_buttion').mouseup(function () {
        $(".search_buttion input").animate({top: "-4px"}, 100);
        $(".search_in").animate({top: "0px"}, 100);
    })


//Form for message type
    $('#files-type').change(function () {
        if ($(this).val() == '2') {
            $('.message_upload_box').hide();
            $('.message_upload_input').fadeIn();
        }
        else {
            $('.message_upload_input').hide();
            $('.message_upload_box').fadeIn();
        }

    })

    //like
    $(".like").on("click", function () {
        var obj = $(this);
        var id = obj.attr("rel");
        var curr_count = parseInt(obj.find("span").text());
        $.post(baseUrl + "/json-gate/add-like", {"obj_id": id})
            .success(function (data) {
                if (data.success) {
                    if (data.success > curr_count)
                        icon_class = 'fa-heart';
                    else icon_class = 'fa-heart-o';

                    obj.html('<i class="fa ' + icon_class + '" aria-hidden="true">  </i><span></span>');

                    obj.find("span").text(" " + data.success);
                }
                if (data.error)
                    $('.locked_screen').fadeIn(100);
            });
        return false;
    });

    //subscribe
    $(".subscribe").bind("click", function () {
        var obj = $(this);
        var id = obj.attr("rel");
        $.post(baseUrl + "/json-gate/add-subscribe", {"user_id": id})
            .success(function (data) {
                if (data.status == 0)
                    $('.locked_screen').fadeIn(100);
                if (data.status == 1)
                    obj.html('<i class="fa fa-plus" aria-hidden="true"></i> <span>Подписаться</span>');
                if (data.status == 2)
                    obj.html('<i class="fa fa-times" aria-hidden="true"></i> <span>Отписаться</span>');
                if (data.error)
                    $('.locked_screen').fadeIn(100);
            });
        return false;
    });


    //Locked screen

    $('.close_locked_screen').click(function () {
        $('.locked_screen').fadeOut(700);
    })


    $('#user-avatarfile').on("change", function () {
        $('.change_file').show();
    });


});