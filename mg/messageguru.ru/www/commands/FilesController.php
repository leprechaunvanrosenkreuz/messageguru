<?php
/**
 * Created by PhpStorm.
 * User: manchetten
 * Date: 21.05.17
 * Time: 16:09
 */

namespace app\commands;

use yii\console\Controller;
use app\models\FilesTemp;
use app\components\FileProcessing;

/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class FilesController extends Controller
{
    public function actionIndex()
    {
        $files = FilesTemp::find()
            ->where([
                'status' => FilesTemp::STATUS_NEW
            ])
            ->limit(20)
            ->all();

        if (!empty($files)) {
            /** @var FilesTemp $file */
            foreach ($files as $file) {

                $file->status = FilesTemp::STATUS_INWORK;
                $file->update(false);

                $p = new FileProcessing();

                //Чекаем, если это не пдф, то конвертим и отправляем в следующий цикл
                if ($file->type == FilesTemp::TYPE_PPT || $file->type == FilesTemp::TYPE_PPTX) {
                    $p->convertToPdf($file);
                    continue;
                }

                $p->convertToPng($file);

                $file->status = FilesTemp::STATUS_DONE;
                $file->update(false);
            }
        }
    }

    public function actionTest()
    {
        $files = FilesTemp::find()
            ->where([
                //'status' => FilesTemp::STATUS_NEW
                'id' => 7
            ])
            ->limit(20)
            ->all();

        if (!empty($files)) {
            /** @var FilesTemp $file */
            foreach ($files as $file) {
                $p = new FileProcessing();


                $p->convertToPng($file);
            }
        }
    }
}