<?php
/**
 * Created by PhpStorm.
 * User: manchetten
 * Date: 25.09.17
 * Time: 16:04
 */

namespace app\commands;

use app\models\Achievements;
use app\models\Files;
use app\models\Likes;
use app\models\UserAchievements;
use app\modules\user\models\User;
use yii\console\Controller;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Console;
use app\models\Subscribers;


class PqController extends Controller
{
    /**
     * pq=(кол-во презентаций Х кол-во просмотров) + (количество лайков Х кол-во подписок)
     */
    public function actionCalc()
    {
        /** @var User $user */
        foreach (User::find()->where(['user.status' => User::STATUS_ACTIVE])->joinWith('activeFiles')->each(100) as $user) {

            if (!empty($user->activeFiles)) {
                $allViews = $this->getAllViewsCount($user->activeFiles);
                $allLikes = $this->getAllLikesCount($user->activeFiles);
                $allSubscribes = $this->getSubscribersCount($user->id);

                //Console::output('User (' . $user->id . ')' . ' - allViews = ' . $allViews . PHP_EOL);
                //Console::output('User (' . $user->id . ')' . ' - allLikes = ' . $allLikes . PHP_EOL);
                //Console::output('User (' . $user->id . ')' . ' - allSubscribes = ' . $allSubscribes . PHP_EOL);

                //$pq = ceil((count($user->activeFiles) * $allLikes * $allSubscribes/($allViews != 0 ? $allViews : 1)) * 100);
                $pq = (count($user->activeFiles) * $allViews) + ($allSubscribes * $allLikes);

                $user->pq = $pq;
                $user->update(false);

                Console::output('User (' . $user->id . ')' . $user->login . ' - PQ = ' . $pq . PHP_EOL);
                Console::output(' ' . PHP_EOL);
                //echo $pq;
                //die();
            }
        }
    }

    /**
     * 7 грейд – самый высокий.
     *
     * a.     0.1% от общей базы зарегистрированных достигают 7го уровня, но с PQ не менее 125 000
     *
     * b.     1% 6го уровня но pq не менее 75 000 (75К)
     *
     * c.     3% 5го уровня, но pq не менее такого-то 35 000 К
     *
     * d.     5% 4го – pq 15 000
     *
     * e.     7% 3го  - pq 7 500
     *
     * f.      зарегистрированный и разместивший одну презентацию – 2-ой уровень
     *
     * g.     просто зарегистрированный без презентаций – 1-й уровень
     */
    public function actionAchieves()
    {
        Achievements::achieveGetProcents('pretender');
        /** @var User $user */
        foreach (User::find()->where(['user.status' => User::STATUS_ACTIVE])->joinWith('achievements')->each(100) as $user) {

            //Level 2
            if (count($user->activeFiles) == 1) {
                if (!Achievements::userHasAchieve($user->id, 'pretender')) {
                    Achievements::assignAchieve($user->id, 'pretender');

                    Console::output('User ' . $user->login . ' - add level pretender' . PHP_EOL);
                }
                continue;
            }

            //Level 3
            if ($user->pq >= 1500 && $user->pq <= 5000) {
                if (!Achievements::userHasAchieve($user->id, 'challenger')) {
                    Achievements::assignAchieve($user->id, 'challenger');

                    Console::output('User ' . $user->login . ' - add level challenger' . PHP_EOL);
                }
                continue;
            }

            //Level 4
            if ($user->pq >= 5000 && $user->pq <= 15000) {
                if (!Achievements::userHasAchieve($user->id, 'master')) {
                    Achievements::assignAchieve($user->id, 'master');

                    Console::output('User ' . $user->login . ' - add level master' . PHP_EOL);
                }
                continue;
            }

            //Level 5
            if ($user->pq >= 15000 && $user->pq <= 50000) {
                if (!Achievements::userHasAchieve($user->id, 'grandmaster')) {
                    Achievements::assignAchieve($user->id, 'grandmaster');

                    Console::output('User ' . $user->login . ' - add level grandmaster' . PHP_EOL);
                }
                continue;
            }

            //Level 6
            if ($user->pq >= 50000 && $user->pq <= 100000 && (Achievements::achieveGetProcents('guru') <= 20)) {
                if (!Achievements::userHasAchieve($user->id, 'guru')) {
                    Achievements::assignAchieve($user->id, 'guru');

                    Console::output('User ' . $user->login . ' - add level guru' . PHP_EOL);
                }
                continue;
            }

            //Level 7
            if ($user->pq >= 100000) {
                if (!Achievements::userHasAchieve($user->id, 'immortal')&& (Achievements::achieveGetProcents('immortal') <= 1)) {
                    Achievements::assignAchieve($user->id, 'immortal');

                    Console::output('User ' . $user->login . ' - add level immortal' . PHP_EOL);
                }
                continue;
            }
        }
    }

    /**
     * Возвращает суммарное кол-во просмотров по всем презам юзера
     * @param Files[] $files
     * @return number
     */
    public function getAllViewsCount($files, $returnZero = false)
    {
        $viewsSum = array_sum(ArrayHelper::getColumn($files, 'views'));
        if ($viewsSum == 0 && !$returnZero)
            $viewsSum = 1;
        return $viewsSum;
    }

    /**
     * Возвращает общее кол-во лайков по всем презам пользователя
     * @param Files[] $files
     * @return number
     */
    public function getAllLikesCount($files, $returnZero = false)
    {
        $filesIds = ArrayHelper::getColumn($files, 'id');

        $countLikes = Likes::find()->where(['in', 'obj_id', $filesIds])->count();
        if ($countLikes == 0 && !$returnZero)
            $countLikes = 1;
        return $countLikes;
    }

    public function getSubscribersCount($id, $returnZero = false)
    {
        $countSubscribers = Subscribers::find()
            ->joinWith('user')
            ->where(['subscribers.subscriber_id' => $id])
            ->orderBy('subscribers.id DESC')
            ->count();
        if ($countSubscribers == 0 && !$returnZero)
            $countSubscribers = 1;
        return $countSubscribers;
    }
}