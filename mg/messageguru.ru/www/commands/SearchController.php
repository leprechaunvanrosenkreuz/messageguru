<?php
/**
 * Created by PhpStorm.
 * User: manchetten
 * Date: 06.08.17
 * Time: 23:18
 */
namespace app\commands;

use yii\console\Controller;
use Yii;

class SearchController extends Controller
{
    public function actionIndex()
    {
        /** @var \himiklab\yii2\search\Search $search */
        $search = Yii::$app->search;
        $search->index();
    }
}