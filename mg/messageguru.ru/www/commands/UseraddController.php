<?php

namespace app\commands;

use app\modules\user\models\User;
use Yii;
use yii\console\Controller;


class UseraddController extends Controller
{

    public function actionInit() {
        $auth = Yii::$app->authManager;
        
        $auth->removeAll(); //На всякий случай удаляем старые данные из БД...
        
        // Создадим роли админа и редактора новостей
        $organiztaor = $auth->createRole('organiztaor');
        $speaker = $auth->createRole('speaker');
        
        // запишем их в БД
        $auth->add($organiztaor);
        $auth->add($speaker);
        
        // Создаем наше правило, которое позволит проверить автора новости
        //$authorRule = new \app\rbac\AuthorRule;
        
        // Запишем его в БД
        //$auth->add($authorRule);
        
        $viewAdminPage = $auth->createPermission('viewAdminPage');
        $viewAdminPage->description = 'Просмотр личного кабинета';
        
        $updatePost = $auth->createPermission('updatePost');
        $updatePost->description = 'Редактирование записей';
        
        $updateOwnPost = $auth->createPermission('updateOwnPost');
        $updateOwnPost->description = 'Редактирование собственной записи';
        
        //$updateOwnPost->ruleName = $authorRule->name;
        
        // Запишем все разрешения в БД
        $auth->add($viewAdminPage);
        $auth->add($updatePost);
        $auth->add($updateOwnPost);
        
        $auth->addChild($speaker,$updateOwnPost);
        $auth->addChild($organiztaor, $updatePost);        
        $auth->addChild($organiztaor, $viewAdminPage);
        $auth->assign($organiztaor, 1);
        $auth->assign($speaker, 2);
    }

    public function actionAddUser($fullname, $login, $mail, $password){
        $name = explode(" ", $fullname);

        $user = new User();
        $user->login = $login;
        $user->last_name = $name[0];
        $user->first_name = $name[1];
        $user->middle_name = $name[2];
        $user->email = $mail;
        $user->password_hash = Yii::$app->security->generatePasswordHash($password);
        $user->auth_key = Yii::$app->security->generateRandomString();
        $user->type = 1;
        $user->status = 1;
        $user->insert();
    }
}