<?php
namespace app\components;

/**
 * Created by PhpStorm.
 * User: manchetten
 * Date: 20.09.17
 * Time: 22:00
 */
class FacebookOAuth2Service extends \nodge\eauth\services\FacebookOAuth2Service
{

    const SCOPE_USER_FRIENDS = 'user_friends';

    protected $scopes = [
        self::SCOPE_USER_FRIENDS
    ];

    public function getFriendsList($soc_id)
    {

        $api_method = 'me/friends'; // ex. for Facebook this results to https://graph.facebook.com/me

        // get protected resource
        $response = $this->makeSignedRequest($api_method, [
            //'query' => ['uid' => $soc_id], // GET arguments
            //'data' => ['foo' => 'bar'], // POST arguments
            //'headers' => ['X-Foo' => 'bar'], // Extra HTTP headers
        ]);

        // you can get public resources with the same API:
        //$response = $this->makeRequest($api_method, []);

        // process $response
        //$data = process($response);

        // return results
        return $response;
    }
}