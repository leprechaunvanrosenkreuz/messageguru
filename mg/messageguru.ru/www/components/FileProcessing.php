<?php
/**
 * Created by PhpStorm.
 * User: manchetten
 * Date: 21.05.17
 * Time: 16:55
 */
namespace app\components;

use app\models\Files;
use app\models\FilesTemp;
use yii\base\Component;
use Yii;
use yii\base\Exception;

class FileProcessing extends Component
{
    public $path;

    public function init()
    {
        parent::init();
        $this->path = Yii::getAlias('@uploads/presentations/');
    }

    /**
     * Конвертация ppt файлов в pdf с заменой расширения в БД
     * @param FilesTemp $temp_file
     * @param bool $update_all
     * @return bool
     */
    public function convertToPdf($temp_file, $update_all = true)
    {
        if ($temp_file->type == FilesTemp::TYPE_PPT || $temp_file->type == FilesTemp::TYPE_PPTX) {
            $full_name = $temp_file->file_name . '.' . $temp_file->file_extension;
            exec("unoconv " . $this->path . $full_name . " " . $this->path . $temp_file->file_name . ".pdf", $out, $var);

            // exec is successful only if the $return_var was set to 0. !== means equal and identical, that is it is an integer and it also is zero.
            //if ($var == 0) {
            $temp_file->is_converted = 1;
            $temp_file->type = FilesTemp::TYPE_PDF;
            $temp_file->file_extension = 'pdf';
            $temp_file->status = FilesTemp::STATUS_NEW; //Возвращаем в статус новый - надо парсить пдф
            $temp_file->pages = $this->getPages($this->path . $temp_file->file_name . '.' . $temp_file->file_extension);

            $temp_file->update();

            if ($update_all) {
                /** @var Files $file */
                $file = Files::findOne($temp_file->file_id);
                $file->name = $temp_file->file_name . ".pdf";
                $file->update();
            }

            return true;
//            } else {
//                $temp_file->status = FilesTemp::STATUS_ERROR;
//                $temp_file->update();
//
//                return false;
//            }
        } else return false;
    }

    /**
     * Конвертирует загруженный пдф в серию картинок
     * @param FilesTemp $temp_file
     * @return bool
     */
    public function convertToPng($temp_file)
    {
        if ($temp_file->type == FilesTemp::TYPE_PDF) {
            $full_name = $temp_file->file_name . '.' . $temp_file->file_extension;
            $thumb_path = $this->path . $temp_file->file_name;

            if (!is_dir($thumb_path))
                mkdir($thumb_path, 0777, true);

            for ($i = 0; $i < $temp_file->pages; $i++) {
                $curr_file_page = $this->path . $full_name . '[' . $i . ']';
                $thumb = $thumb_path . '/' . $i . "_" . Files::getThumbName($full_name);

                exec("convert -background white -flatten -quality 85 -density 96 -alpha remove " . $curr_file_page . " " . $thumb, $out, $var);

                //echo $out . PHP_EOL;

                if ($var == 0) {
                    $temp_file->pages_loaded = $i + 1;
                    $temp_file->update();
                } else {
                    $temp_file->status = FilesTemp::STATUS_ERROR;
                    $temp_file->update();
                }
            }

            return true;
        } else return false;
    }

    /**
     * Возвращает кол-во страниц в пдф
     * @param $filename
     * @return int
     */
    private function getPages($filename)
    {
        $pdftext = file_get_contents($filename);
        $num = preg_match_all("/\/Page\W/", $pdftext, $dummy);
        return $num;
    }
}