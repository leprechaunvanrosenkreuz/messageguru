<?php
/**
 * Created by PhpStorm.
 * User: manchetten
 * Date: 23.06.16
 * Time: 14:42
 */

namespace app\components;

use app\models\Files;
use app\models\Subscribers;
use app\models\UserDevices;
use app\modules\user\models\User;
use Yii;
use yii\helpers\ArrayHelper;
use ApnsPHP_Message_Exception;

class PushService extends \yii\base\Component
{
    const TYPE_PRESENTATION = 1;
    const TYPE_NEW_SUBSCRIBE = 2;

    public function init()
    {
        parent::init();
    }

    /**
     * Отсылает пуш уведомление о новой презентации всем подписчикам этгго юзера
     * @param $user_id
     * @param $file_id
     */
    public function sendPushToSubscribers($user_id, $file_id)
    {
        //$subscribers = $this->getSubscribers();

        $subscribers = Subscribers::find()
            ->joinWith('subscriber')
            ->where(['subscribers.user_id' => $user_id])
            ->all();

        if (!empty($subscribers)) {
            $user = User::findOne($user_id);
            $pres = Files::findOne($file_id);

            $message = $user->first_name . ' ' . $user->last_name . ' разместил(ла) новый message.';

            /** @var Subscribers $subscriber */
            foreach ($subscribers as $subscriber) {
                //print_r($subscriber->subscriber);die();
                if (!empty($userToSend = $subscriber->subscriber) && !empty($userToSend->devices)) {
                    /** @var UserDevices $device */
                    foreach ($userToSend->devices as $device) {
                        if ($device->is_ios == 1) {
                            $apns = Yii::$app->apns;
                            $apns->send($device->device_id, $message, ['type' => 'post', 'id' => $pres->id], ['sound' => 'default', 'badge' => 1]);
                            //$this->sendIos($device->device_id, $message, $pres->id);
                        }

                        if ($device->is_android == 1) {
                            $gcm = Yii::$app->gcm;
                            $gcm->send($device->device_id, $message, ['type' => 'post', 'id' => $pres->id]);
                            //$this->sendAndroid($device->device_id, $message, $pres->id);
                        }
                    }
                }
            }
        }
    }

    /**
     * @param $user_id
     * @param $subscriber_id
     */
    public function sendPushNewSubscriber($user_id, $subscriber_id)
    {
        $user = User::findOne($user_id);
        $subscriber = User::findOne($subscriber_id);

        if(!empty($user) && !empty($subscriber_id) && !empty($user->devices)) {
            $message = $subscriber->first_name . ' ' . $subscriber->last_name . ' подписался(ась) на вас.';
            /** @var UserDevices $device */
            foreach ($user->devices as $device) {
                if ($device->is_ios == 1) {
                    $apns = Yii::$app->apns;
                    $apns->send($device->device_id, $message, ['type' => 'author_page', 'id' => $subscriber->id], ['sound' => 'default', 'badge' => 1]);
                    //$this->sendIos($device->device_id, $message, $pres->id);
                }

                if ($device->is_android == 1) {
                    $gcm = Yii::$app->gcm;
                    $gcm->send($device->device_id, $message, ['type' => 'author_page', 'id' => $subscriber->id]);
                    //$this->sendAndroid($device->device_id, $message, $pres->id);
                }
                //echo $device->device_id . PHP_EOL;
            }
        }
    }

    public function testPush($user_id, $file_id)
    {
        $user = User::findOne($user_id);

        $message = $user->first_name . ' ' . $user->last_name . ' разместил новый message';

        //print_r($user->devices); die();
        if (!empty($user->devices)) {
            /** @var UserDevices $device */
            foreach ($user->devices as $device) {
                if ($device->is_ios == 1) {
                    $this->sendIos($device->device_id, $message, $file_id);
                }

                if ($device->is_android == 1) {
                    $this->sendAndroid($device->device_id, $message, $file_id);
                }

                echo $device->device_id . PHP_EOL;
            }
        }
    }

    private function sendIos($token, $message, $file_id)
    {
        /* @var $apnsGcm \bryglen\apnsgcm\Apns */
        $apns = Yii::$app->apns;
        $apns->send($token, $message, ['type' => 'post', 'id' => $file_id], ['sound' => 'default', 'badge' => 1]);

        return true;
    }

    private function sendIosMulti($tokens, $message, $file_id)
    {
        /* @var $apnsGcm \bryglen\apnsgcm\Apns */
        $apns = Yii::$app->apns;
        try {
            $apns->sendMulti($tokens, $message, ['type' => 'post', 'id' => $file_id], ['sound' => 'default', 'badge' => 1]);
        } catch (ApnsPHP_Message_Exception $mes_ex) {
            Yii::info('Send exception: ' . $mes_ex->getMessage(), 'pushes');
        }
    }

    private function sendAndroid($token, $message, $file_id)
    {
        /* @var $apnsGcm \bryglen\apnsgcm\Gcm */
        $gcm = Yii::$app->gcm;
        $gcm->send($token, $message, ['type' => 'post', 'id' => $file_id]);
    }

    private function sendAndroidMulti($tokens, $message, $file_id)
    {
        /* @var $apnsGcm \bryglen\apnsgcm\Gcm */
        $gcm = Yii::$app->gcm;
        $gcm->sendMulti($tokens, $message, ['type' => 'post', 'id' => $file_id, 'priority' => 'high']);
    }
}