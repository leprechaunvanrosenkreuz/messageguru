<?php
/**
 * Created by PhpStorm.
 * User: manchetten
 * Date: 06.11.16
 * Time: 1:21
 */
namespace app\components;

use yii\base\Component;
use VK\VK;

class VKApi extends Component
{
    public $appId;
    public $apiSecret;
    public $accessToken = null;

    private $_vk;

    public function init() {
        $this->_vk = new \VK\VK($this->appId, $this->apiSecret, $this->accessToken);
        parent::init();
    }

    public function getAuthorizeURL($apiSettings, $callbackUrl) {
        return $this->_vk->getAuthorizeURL($apiSettings, $callbackUrl);
    }

    public function getAccessToken($code, $callback) {
        return $this->_vk->getAccessToken($code, $callback);
    }

    public function isAuth() {
        return $this->_vk->isAuth();
    }

    public function api($method, $params = []) {
        return $this->_vk->api($method, $params);
    }

    public function getFriends($user_id) {
        return $this->_vk->api('friends.get', ['user_id' => $user_id]);
    }

    public function getUserInfo($user_id) {
        return $this->_vk->api('users.get', [
            'user_ids' => $user_id,
            'fields' => [
                'photo_200_orig'
            ]
        ]);
    }
}
