<?php

/**
 * Валидатор мобильного
 * @param type $attribute
 * @param type $params
 */
namespace app\components\validators;

use yii;

class mobileValidator extends yii\validators\Validator
{
    public function validateAttribute($object, $attribute)
    {
        $value = $object->$attribute;

        if (!$this->validateValue($value))
            $this->addError($object, $attribute, $this->message ? $this->message : 'Номер мобильного телефона указан неверно');

        //if (!$this->validateOperator($value))
        //  $this->addError($object, $attribute, $this->message ? $this->message : 'Указан несуществующий номер мобильного телефона');
    }

    /**
     * Проверить телефон по маске
     */
    public function validateValue($value)
    {
        $ps          = '';
        $cut_symbols = array('+', '-', ' ', '(', ')', '.');
        $phone       = str_replace($cut_symbols, '', $value); // 79126406385 || 89126406385 || 9126406385

        switch (strlen($phone)) {
            case 11:
                $ps   = '/^[7|8](\d{10})$/';
                $code = substr($phone, 1, 3);
                break;
            case 10:
                $ps   = '/^(\d{10})$/';
                $code = substr($phone, 0, 3);
                break;
        }
        if ($ps == '' || !preg_match($ps, $phone))
            return false;
        else
            return true;
    }

    /**
     * Проверить оператора
     */
    public function validateOperator($value)
    {
        $cut   = array('+', '-', ' ', '(', ')', '.');
        $phone = str_replace($cut, '', $value); // 79126406385 || 89126406385 || 9126406385
        $code  = null;
        switch (strlen($phone)) {
            case 11:
                $code = substr($phone, 1, 3);
                break;
            case 10:
                $code = substr($phone, 0, 3);
                break;
        }
        if ($code === null)
            return false;

        $operator = ReferenceGsmOperators::model()->find('code=:code', array('code' => $code));
        if ($operator === null)
            return false;
        else
            return true;
    }
}