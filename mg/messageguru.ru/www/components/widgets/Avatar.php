<?php
/**
 * Created by PhpStorm.
 * User: marduk
 * Виджет для получения линка на аватар
 */

namespace app\components\widgets;

use app\models\Tags;
use yii\base\Widget;
use app\modules\user\models\User;
use Yii;

class Avatar extends Widget
{
    public $user_id;

    public function init()
    {

    }

    public function run()
    {
        if (!empty($this->user_id)) {
            $u = User::findOne($this->user_id);

            $image = $u->getAvatarUrl();
        } else
            if (!empty(Yii::$app->getUser()->id)) {
                $usr = User::find()
                    ->where(['id' => Yii::$app->getUser()->id])
                    ->orWhere(['social_id' => Yii::$app->getUser()->id])
                    ->one();

                $image = $usr->getAvatarUrl();
            }

        return $image;
    }
}
