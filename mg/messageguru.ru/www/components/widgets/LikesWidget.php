<?php
/**
 * Created by PhpStorm.
 * User: manchetten
 * Date: 23.07.17
 * Time: 0:45
 */
namespace app\components\widgets;

use app\models\Likes;
use yii\base\Widget;
use Yii;
use yii\db\Expression;

class LikesWidget extends Widget
{
    public $object_id;
    public $likes_object;

    public function init()
    {
        $this->likes_object = Likes::find()
            ->where([
                'obj_id' => $this->object_id
            ])
            ->all();
    }

    public function run()
    {
        return $this->render('likes', ['likes_object' => $this->likes_object]);
    }
}