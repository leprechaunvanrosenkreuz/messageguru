<?php

namespace app\components\widgets;

use app\models\Files;
use yii\base\Widget;
use app\modules\user\models\User;

class ProfileLeftBar extends Widget
{
    public $model_id;
    public $model;
    public $views = "ProfileLeftBar";

    public function init()
    { 
        if (!isset($this->model_id))
            $this->model_id = \Yii::$app->user->id;

        $this->model = User::find()
            ->joinWith('achievements')
            ->where([
                //'login' => $author_login,
                'user.id' => $this->model_id,
                //'user.status' => User::STATUS_ACTIVE
            ])
            ->one();
    }

    public function run()
    {
        return $this->render($this->views, ['model' => $this->model]);
    }
}
