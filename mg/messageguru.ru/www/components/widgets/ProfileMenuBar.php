<?php

namespace app\components\widgets;

use yii\base\Widget;
use app\modules\user\models\User;

class ProfileMenuBar extends Widget
{
    public $model;

    public function init()
    {

    }

    public function run()
    {
        return $this->render('ProfileMenuBar', ['model' => $this->model]);
    }
}
