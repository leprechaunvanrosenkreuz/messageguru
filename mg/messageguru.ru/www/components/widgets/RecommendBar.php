<?php

namespace app\components\widgets;

use app\models\Files;
use app\models\PromoPres;
use yii\base\Widget;
use Yii;
use yii\db\Expression;
use yii\helpers\ArrayHelper;

class RecommendBar extends Widget
{
    public $model;
    public $view = "RecommendBar";

    public function init()
    {

    }

    public function run()
    {
        $promo = PromoPres::find()->orderBy(['order' => SORT_ASC])->all();
        $ids = ArrayHelper::getColumn($promo, 'file_id');

        if(empty($ids))
            $model = Files::find()->where(['status' => Files::STATUS_ON])->orderBy(new Expression('rand()'))->limit(4)->all();
        else {
            $files = ArrayHelper::index(Files::find()->where(['in', 'id', $ids])->all(), 'id');
            $model = [];

            foreach($ids as $id){
                $model[$id] = ArrayHelper::getValue($files, $id);
            }
        }
        return $this->render($this->view, ['model' => $model]);
    }
}
