<?php
/**
 * Created by PhpStorm.
 * User: manchetten
 * Date: 29.09.17
 * Time: 11:06
*/

namespace app\components\widgets;

use app\models\Files;
use app\models\PromoPres;
use yii\base\Widget;
use Yii;
use yii\db\Expression;
use yii\helpers\ArrayHelper;

/**
* @property integer $pres_id
* @property Files $model
*/

class RenderFileItemWidget extends Widget
{
    public $pres_id;
    public $model;

    public function init()
    {
        if(!empty($this->pres_id))
            $this->model = Files::findOne($this->pres_id);
        elseif(!empty($this->model))
            $this->pres_id = $this->model->id;
    }

    public function run()
    {
        return $this->render('render_file_item', ['model' => $this->model]);
    }
}