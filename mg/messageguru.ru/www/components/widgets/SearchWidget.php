<?php
/**
 * User: marduk
 * Date: 22.06.17
 * Time: 21:46
 */

namespace app\components\widgets;

use Yii;
use nodge\eauth\Widget;


class SearchWidget extends Widget
{
    /**
     * Executes the widget.
     * This method is called by {@link CBaseController::endWidget}.
     */
    public function run()
    {
        //parent::run();
        echo $this->render('search_widget');
    }
}
