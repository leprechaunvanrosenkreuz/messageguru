<?php
/**
 * Created by PhpStorm.
 * User: manchetten
 * Date: 11.06.17
 * Time: 22:12
 */

namespace app\components\widgets;

use Yii;
use nodge\eauth\Widget;


class SocialAuthWidget extends Widget
{
    /**
     * @var string EAuth component name.
     */
    public $component = 'eauth';

    /**
     * @var array the services.
     * @see EAuth::getServices()
     */
    public $services = null;

    /**
     * @var boolean include the CSS file. Default is true.
     * If this is set false, you are responsible to explicitly include the necessary CSS file in your page.
     */
    public $assetBundle = 'nodge\\eauth\\assets\\WidgetAssetBundle';

    /**
     * Executes the widget.
     * This method is called by {@link CBaseController::endWidget}.
     */
    public function run()
    {
        //parent::run();
        echo $this->render('social_auth', [
            'id' => $this->getId(),
            'services' => $this->services,
            'action' => $this->action,
            'popup' => $this->popup,
            'assetBundle' => $this->assetBundle,
        ]);
    }
}
