<?php

namespace app\components\widgets;

use app\models\Tags;
use yii\base\Widget;
use app\modules\user\models\User;
use Yii;

class TagsBar extends Widget
{
    public $model;
    public $view = "TagsBar";

    public function init()
    {

    }

    public function run()
    {

        $connection = Yii::$app->getDb();
        $model = $connection
            ->createCommand('SELECT name, alias FROM tags GROUP BY name HAVING COUNT(*) >= ALL(SELECT COUNT(*) FROM tags GROUP BY name) LIMIT 10')
            ->queryAll();

        if(count($model) < 10){
            $model = Tags::find()->limit(10)->all();
        }

        return $this->render($this->view, ['model' => $model]);
    }
}
