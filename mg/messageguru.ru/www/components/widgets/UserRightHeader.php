<?php
/**
 * Created by PhpStorm.
 * User: manchetten
 * Date: 19.03.17
 * Time: 22:16
 */

namespace app\components\widgets;

use yii\base\Widget;
use app\modules\user\models\User;

class UserRightHeader extends Widget
{
    public $model;

    public function init()
    {

    }

    public function run()
    {
        $user = \Yii::$app->getUser();
        return $this->render('user_right_header', ['user' => $user]);
    }
}