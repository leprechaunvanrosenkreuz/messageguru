<?php
/**
 * Created by PhpStorm.
 * User: marduk
 * Date: 28.03.17
 * Time: 21:45
 */

/** @var User $model */

use yii\helpers\Html;
use yii\helpers\Url;
use app\modules\user\models\User;
use \app\models\Cities;
use \app\models\Files;
use app\components\widgets\Avatar;
use app\models\Achievements;
use \app\models\Subscribers;

?>
<div class="block user_block overflow">
    <div class="bg_primary noise_light user_head">
        <a href="<?= Url::to(['/author/' . $model->id]) ?>" style="color: #ffffff">
            <div class="add_photo">
                <div class="user_photo"
                     style="background-image: url('<?= Avatar::widget(['user_id' => $model->id]) ?>') "></div>
            </div>
            <h3><?= $model->getFullName($model->id) ?></h3>
        </a>
        <?php
        //TODO Переписать в виджет
        if (isset($model->achievements)) {
            //Берем последнее достижение по истории
            /** @var \app\models\Achievements $achieve */
            $achieve = end($model->achievements);
// print_r($achieve);
            if (isset($achieve) && $achieve->type == Achievements::TYPE_LEVEL) {
                echo '<span>- ' . $achieve->name . ' -</span>';
            }
        }
        ?>
    </div>
    <div class="user_counters">
        <div>
            <h3 class="color_primary"><?= $model->pq ?></h3><span>PQ</span>
        </div>

        <div>
            <h3 class="color_primary"><?= $model->getFilesCount(true) ?></h3><span>Messages</span>
        </div>

        <?php
        //TODO Переписать в виджет
        if (isset($model->achievements)) {
            //Берем последнее достижение по истории
            /** @var \app\models\Achievements $achieve */
            $achieve = end($model->achievements);

            if (isset($achieve) && $achieve->type == Achievements::TYPE_LEVEL) {
                echo '<div>';
                echo '<div class="rang rang_' . $achieve->alias . '"></div>';
                echo '<span>' . $achieve->name . '</span>';
                echo '</div>';
            }
        }
        ?>
    </div>


    <div class="full_user_info"><i class="fa fa-angle-double-down" aria-hidden="true"></i></div>

    <div class="user_info">
        <?php if (!empty(Cities::findOne($model->city_id)->name)) { ?>
            <div><i class="fa fa-globe color_primary"
                    aria-hidden="true"></i><span><?= Cities::findOne($model->city_id)->name ?></span></div>
        <?php } ?>
        <?php if (!empty($model->profession)) { ?>
            <div><i class="fa fa-briefcase color_primary" aria-hidden="true"></i><span><?= $model->profession ?></span>
            </div>
        <?php } ?>
        <?php if (!empty($model->desc)) { ?>
            <div><i class="fa fa-heart color_primary" aria-hidden="true"></i><span><?= $model->desc ?></span></div>
        <?php } ?>
    </div>
</div>

<?php if ((Yii::$app->getUser()->id != $model->id) && !Yii::$app->user->isGuest) {
    if (Subscribers::checkIfSubscriber(Yii::$app->user->id, $model->id))
        echo Html::a('<i class="fa fa-times" aria-hidden="true"></i> <span>Отписаться</span>', '#', ['class' => 'left btn bg_primary user_block subscribe', 'rel' => $model->id]);
    else
        echo Html::a('<i class="fa fa-plus" aria-hidden="true"></i> <span>Подписаться</span>', '#', ['class' => 'left btn bg_primary user_block subscribe', 'rel' => $model->id]);
}
?>
