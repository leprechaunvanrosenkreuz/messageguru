<?php
/**
 * Created by PhpStorm.
 * User: marduk
 * Date: 25.03.17
 * Time: 9:16
 */

/** @var User $model */

use yii\helpers\Html;
use yii\helpers\Url;
use app\modules\user\models\User;
use \app\models\Cities;
use \app\models\Files;
use \app\models\Subscribers;
use app\components\widgets\Avatar;
use app\models\Achievements;

?>

<div class="col left">
    <div class="block user_block">
        <div class="bg_primary noise_light user_head">
            <div class="add_photo">
                <div class="user_photo"
                     style="background-image: url('<?= Avatar::widget(["user_id" => $model->id]) ?>') "></div>
            </div>
            <h3><?= $model->getFullName($model->id) ?></h3>
            <?php
            //TODO Переписать в виджет
            if (isset($model->achievements)) {
                //Берем последнее достижение по истории
                /** @var \app\models\Achievements $achieve */
                $achieve = end($model->achievements);
//print_r($model);
                if (isset($achieve) && $achieve->type == Achievements::TYPE_LEVEL) {
                    echo '<span>- ' . $achieve->name . ' -</span>';
                }
            }
            ?>
        </div>
        <div class="user_counters">
            <div>
                <h3 class="color_primary"><?= $model->pq ?></h3><span>PQ</span>
            </div>

            <div>
                <h3 class="color_primary"><?= $model->getFilesCount(true) ?></h3><span>Messages</span>
            </div>

            <?php
            //TODO Переписать в виджет
            if (isset($model->achievements)) {
            //Берем последнее достижение по истории
            /** @var \app\models\Achievements $achieve */
            $achieve = end($model->achievements);

            if (isset($achieve) && $achieve->type == Achievements::TYPE_LEVEL) {
            echo '<div>';
            echo '<div class="rang rang_'.$achieve->alias.'"></div>';
            echo '<span>' .$achieve->name. '</span>';
            echo '</div>';
            }
            }
            ?>
        </div>


        <div class="full_user_info"><i class="fa fa-angle-double-down" aria-hidden="true"></i></div>
        <div class="user_info">
            <?php if (!empty(Cities::findOne($model->city_id)->name)){ ?>
            <div><i class="fa fa-globe color_primary"
                    aria-hidden="true"></i><span><?= Cities::findOne($model->city_id)->name ?></span></div>
            <?php } ?>
            <?php if (!empty($model->profession)){ ?>
            <div><i class="fa fa-briefcase color_primary" aria-hidden="true"></i><span><?= $model->profession ?></span>
            </div>
            <?php } ?>
            <?php if (!empty($model->desc)){ ?>
            <div><i class="fa fa-heart color_primary" aria-hidden="true"></i><span><?= $model->desc ?></span></div>
            <?php } ?>
        </div>
    </div>


    <div class="user_menu block  user_block">
        <?php if (!empty($model->site)){ ?>
        <?= Html::a('<i class="fa fa-external-link color_primary" aria-hidden="true"></i> ' . $model->site, Url::to($model->site, 'http'), ['class' => 'transition', 'target' => "_blank"]) ?>
        <?php } ?>
        <?php if (!empty($model->lk)){ ?>
        <?= Html::a('<i class="fa fa-linkedin color_primary" aria-hidden="true"></i> linkedin', Url::to($model->lk, 'http'), ['class' => 'transition', 'target' => "_blank"]) ?>
        <?php } ?>
        <?php if (!empty($model->vk)){ ?>
        <?= Html::a('<i class="fa fa-vk color_primary" aria-hidden="true"></i> VK', Url::to($model->vk, 'http'), ['class' => 'transition', 'target' => "_blank"]) ?>
        <?php } ?>
        <?php if (!empty($model->tw)){ ?>
        <?= Html::a('<i class="fa fa-twitter color_primary" aria-hidden="true"></i> Twitter', Url::to($model->tw, 'http'), ['class' => 'transition', 'target' => "_blank"]) ?>
        <?php } ?>
    </div>

    <?php if ((Yii::$app->getUser()->id != $model->id) && !Yii::$app->user->isGuest) {
        if(Subscribers::checkIfSubscriber(Yii::$app->user->id, $model->id))
            echo Html::a('<i class="fa fa-times" aria-hidden="true"></i> <span>Отписаться</span>', '#', ['class' => 'left btn bg_primary user_block subscribe', 'rel' => $model->id]);
        else
            echo Html::a('<i class="fa fa-plus" aria-hidden="true"></i> <span>Подписаться</span>', '#', ['class' => 'left btn bg_primary user_block subscribe', 'rel' => $model->id]);
    }
    ?>

    <div class="block fst_adv" style="visibility: hidden; display: none">
        Рекламный блок
    </div>
</div>