<?php
/**
 * Created by PhpStorm.
 * User: marduk
 * Date: 25.03.17
 * Time: 9:26
 */

use yii\helpers\Html;
use yii\helpers\Url;

$action = Yii::$app->controller->action->id;
?>
<div class="preheader">
    <div class="filters left">
        <?= Html::a((($action == "index") ? '<i class="fa fa-user color_primary" aria-hidden="true"></i>' : '') . ' Мой профиль', Url::to('@web/user/profile'), ['class' => 'filter' . (($action == "index") ? " active_filter" : '')]) ?>
        <?= Html::a((($action == "messages") ? '<i class="fa fa-comments-o color_primary" aria-hidden="true"></i>' : '') . ' Мои Messages', Url::to('@web/user/profile/messages'), ['class' => 'filter' . (($action == "messages") ? " active_filter" : '')]) ?>
        <?= Html::a((($action == "subscribers") ? '<i class="fa fa-user-plus color_primary" aria-hidden="true"></i>' : '') . ' Мои подписки', Url::to('@web/user/profile/subscribers'), ['class' => 'filter' . (($action == "subscribers") ? " active_filter" : '')]) ?>
        <?= Html::a((($action == "settings") ? '<i class="fa fa-pencil-square-o color_primary" aria-hidden="true"></i>' : '') . ' Настройки', Url::to('@web/user/profile/settings'), ['class' => 'filter' . (($action == "settings") ? " active_filter" : '')]) ?>
    </div>
</div>