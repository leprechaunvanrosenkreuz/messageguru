<?php
/**
 * Created by PhpStorm.
 * User: marduk
 * Date: 10.04.17
 * Time: 21:04
 */

use yii\helpers\Html;
use yii\helpers\Url;
use app\modules\user\models\User;

?>
<? foreach ($model AS $file){ ?>

    <div class="message">
    <a href="<?= Url::to("@web/pres/" . $file->id) ?>" class="message_view transition"
       style="background-image: url('<?= $file->getImageUrl() ?>')"></a>
    <h4 class="color_primary"><?= Html::a(User::getFullname($file->user_id), Url::to(['/author/' . $file->user_id]), ['class' => 'color_primary']); ?></h4>
    <p><?= $file->title ?></p>
    <div class="message_meta_inf ">
        <span><?= date("d.m.Y", strtotime($file->created_at)) ?></span>
        <span>
            <?= Yii::t('app', '{count, plural, =0{not просмотров} =1{# просмотр} few{# просмотра} many{# просмотров} other{# просмотр}}',
                ['count' => (!empty($file->views) ? $file->views : 0)]) ?>
        </span>
    </div>
</div>
<? } ?>
<?= Html::a("Показать еще!", ['/presentation'], ["class" => "btn btn_border color_primary transition"]) ?>
