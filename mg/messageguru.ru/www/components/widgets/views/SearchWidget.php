<?php

use yii\helpers\Html;
use yii\helpers\Url;
?>

<form action="<?= Url::to(["search/index"]) ?>" method="get">
    <div class="search_out ">
        <div class="search_in">
            <i class="fa fa-search"></i>
            <input value="<?= (!empty($_GET['search'])? $_GET['search'] : '')?>" type="text" <?= (!empty($_GET['filter'])? '': 'placeholder="Поиск сообщений..."')?> name="search" aria-required="true" aria-invalid="true">
            <input value="<?= (!empty($_GET['filter'])? $_GET['filter'] : '')?>" type="hidden" name="filter" class="search_filter">
            <div class="filter_label"></div>
        </div>
    </div>
    <div class="search_buttion">
        <input type="submit" value="Начать поиск!">
    </div>
    <div class="filters"> 
		<a class="filter" onclick="$('.search_in input').attr('placeholder', '').css('padding-left','90px');$('.search_filter').val(1); $('.filter_label').text('Все темы:');" href="#">Все темы <i class="fa fa-angle-down" aria-hidden="true"></i></a>
		<a class="filter" onclick="$('.search_in input').attr('placeholder', '').css('padding-left','90px');;$('.search_filter').val(2); $('.filter_label').text('Все авторы:');" href="#">Все авторы <i class="fa fa-angle-down" aria-hidden="true"></i></a>
		<a class="filter" onclick="$('.search_in input').attr('placeholder', 'Поиск сообщений...').css('padding-left','0px');;$('.search_filter').val(); $('.filter_label').text('');" href="#">За все время <i class="fa fa-angle-down" aria-hidden="true"></i></a>
	</div>
</form>