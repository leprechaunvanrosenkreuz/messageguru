<?php
/**
 * Created by PhpStorm.
 * User: manchetten
 * Date: 23.07.17
 * Time: 0:45
 */

/** @var \app\models\Likes $like_object */

$likes = !empty($likes_object) ? count($likes_object) : 0;
?>

<span class="like" rel="<?= $this->context->object_id ?>">
    <?php if (\app\models\Likes::isLikedByUser($this->context->object_id)) : ?>
        <i class="fa fa-heart" aria-hidden="true">  </i>
    <?php else: ?>
        <i class="fa fa-heart-o" aria-hidden="true">  </i>
    <?php endif; ?>
    <span> <?= $likes ?></span>
</span>