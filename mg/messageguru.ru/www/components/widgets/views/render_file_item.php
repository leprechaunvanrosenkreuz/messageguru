<?php
/**
 * Created by PhpStorm.
 * User: manchetten
 * Date: 29.09.17
 * Time: 11:08
 */

/* @var \app\models\Files $model */
use yii\helpers\Html;
use yii\helpers\Url;
use app\modules\user\models\User;
use app\components\widgets\LikesWidget;
?>

<div class="message">
    <?= Html::a('', Url::to(['/pres/' . $model->id]), ['style' => 'background-image: url(' . $model->getImageUrl() . ')', 'class' => 'message_view transition', 'title' => '', 'data-pjax' => 0]) ?>


    <h4 class="color_primary"><?= Html::a(User::getFullname($model->user_id), Url::to(['/author/' . $model->user_id]), ['class' => 'color_primary', 'data-pjax' => 0]); ?></h4>
    <p><?= $model->title ?></p>
    <div class="message_meta_inf ">
        <span><?= date("d.m.Y", strtotime($model->created_at)) ?></span>
        <span>
            <?= Yii::t('app', '{count, plural, =0{not просмотров} =1{# просмотр} few{# просмотра} many{# просмотров} other{# просмотр}}',
                ['count' => (!empty($model->views) ? $model->views : 0)]) ?>
        </span>
    </div>

    <div class="message_controls color_primary">
        <?= LikesWidget::widget(['object_id' => $model->id]); ?>

        <?php $download_url = Yii::getAlias('@uploads_http/presentations/') . $model->name; ?>
        <?php
        if ($model->type !== \app\models\Files::TYPE_YOUTUBE) {
            if (!empty(Yii::$app->getUser()->id))
                echo Html::a('<i class="fa fa-download color_primary" aria-hidden="true"></i>', $download_url, ['class' => 'download', 'data-pjax' => 0]);
            else
                echo Html::a('<i class="fa fa-download color_primary" aria-hidden="true"></i>', $download_url, ['class' => 'download', 'onclick' => 'lockedscreen();return false;']);
        }
        ?>
    </div>

</div>
