<?php

use yii\helpers\Html;
use yii\helpers\Url;

?>

<?= Html::beginForm(Url::to(['/search/index']), 'get', []) ?>
    <div class="search_out ">
        <div class="search_in">
            <i class="fa fa-search"></i>
            <?= Html::textInput('q', (!empty(Yii::$app->request->get('q')) ? Yii::$app->request->get('q') : ''), [
                'aria-required' => 'true',
                'aria-invalid' => 'true',
                'placeholder' => 'Поиск сообщений...'
            ]) ?>
            <div class="filter_label"></div>
        </div>
    </div>
    <div class="search_buttion">
        <input type="submit" value="Начать поиск!">
    </div>

<?= Html::endForm(); ?>