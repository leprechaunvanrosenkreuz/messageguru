<?php
/**
 * Created by PhpStorm.
 * User: manchetten
 * Date: 11.06.17
 * Time: 22:18
 */

use yii\helpers\Html;
use yii\web\View;

/** @var $this View */
/** @var $id string */
/** @var $services stdClass[] See EAuth::getServices() */
/** @var $action string */
/** @var $popup bool */
/** @var $assetBundle string Alias to AssetBundle */

Yii::createObject(['class' => $assetBundle])->register($this);

// Open the authorization dilalog in popup window.
if ($popup) {
    $options = [];
    foreach ($services as $name => $service) {
        $options[$service->id] = $service->jsArguments;
    }
    $this->registerJs('$("#' . $id . '").eauth(' . json_encode($options) . ');');
}

?>
<div class="eauth" id="<?php echo $id; ?>">

    <?php
    foreach ($services as $name => $service) {

        echo Html::a('<i class="fa fa-' . $service->title . '"></i>', [$action, 'service' => $name], [
            'class' => 'auth_' . $name . ' auth_soc transition',
            'data-eauth-service' => $service->id,
        ]);

    }
    ?>
</div>
