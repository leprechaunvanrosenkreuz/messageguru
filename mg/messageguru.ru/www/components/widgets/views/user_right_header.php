<?php
/**
 * Created by PhpStorm.
 * User: manchetten
 * Date: 19.03.17
 * Time: 22:18
 */

use yii\helpers\Html;
use yii\helpers\Url;
use app\components\widgets\Avatar;
?>

<?php if (Yii::$app->user->isGuest): ?>
            <div class="right auth_menu">
                <?= Html::a('Регистрация', Url::to(['/user/do/register']), ['class'=>'btn gen_btn signup btn_border color_primary transition'])  ?>
                <?= Html::a('Вход <i class="fa fa-sign-in" aria-hidden="true"></i>', Url::to(['/user/do/login']), ['class'=>'color_primary gen_btn signin transition'])   ?>
            </div>
<? else: ?>
    <div class="right user_header">
        <?= Html::a('<div class="user_photo" style="background-image: url('.Avatar::widget().')"></div>', Url::to(['/user/profile'])) ?>



        <div class="dropdown">
            <div class="open_sub_menu bg_primary transition" id="dLabel" data-toggle="dropdown"><i
                    class="fa fa-caret-down"></i></div>
            <ul class="dropdown-menu">
                <li><?= Html::a('Мой профиль', Url::to(['/user/profile'])) ?></li>
                <li><?= Html::a('Мои сообщения', Url::to(['/user/profile/messages'])) ?></li>
                <li><?= Html::a('Выход', Url::to(['/user/do/logout'])) ?></li>
            </ul>
        </div>
    </div>
    <div class="right">
        <?= Html::a('Добавить message!', Url::to(['/presentation/add']), ['class'=>'btn gen_btn btn_border color_primary transition']) ?>
    </div>
<?php endif; ?>
