<?php

$params = require(__DIR__ . '/params.php');
$db = require(__DIR__ . '/db.php');

$config = [
    'id' => 'basic-console',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'app\commands',
    'aliases' => [
        'uploads' => YII_ENV_PROD ? '/home/mg/files.messageguru.ru/www' : '/home/messageguru/files.messageguru.dev.new-tech.digital/www',
        'uploads_http' => YII_ENV_PROD ? 'http://files.messageguru.ru' : 'http://files.messageguru.dev.new-tech.digital',
    ],
    'components' => [
        'search' => [
            'class' => 'himiklab\yii2\search\Search',
            'models' => [
                'app\models\Files',
                'app\models\Tags',
                'app\modules\user\models\User',
            ],
        ],
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'log' => [
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => $db,
    ],
    'params' => $params,
    /*
    'controllerMap' => [
        'fixture' => [ // Fixture generation command line.
            'class' => 'yii\faker\FixtureController',
        ],
    ],
    */
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
    ];
}

return $config;
