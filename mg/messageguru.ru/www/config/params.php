<?php

return [
    'adminEmail' => 'admin@example.com',
    'mailFrom' => 'no-reply@messageguru.ru',
    'uploadsUrl' => YII_ENV_PROD ? 'http://files.messageguru.ru' : 'http://files.messageguru.dev.new-tech.digital',
];
