<?php

$params = require(__DIR__ . '/params.php');

$config = [
    'id' => 'basic',
    'language' => 'ru-RU',
    'sourceLanguage' => 'ru-RU',
    'name' => 'MessageGuru',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'aliases' => [
        'uploads' => YII_ENV_PROD ? '/home/mg/files.messageguru.ru/www' : '/home/messageguru/files.messageguru.dev.new-tech.digital/www',
        'uploads_http' => YII_ENV_PROD ? 'http://files.messageguru.ru' : 'http://files.messageguru.dev.new-tech.digital',
    ],
    'components' => [
//        'elasticsearch' => [
//            'class' => 'yii\elasticsearch\Connection',
//            'nodes' => [
//                ['http_address' => '127.0.0.1:9200'],
//        // configure more hosts if you have a cluster
//            ],
//        ],
        'search' => [
            'class' => 'himiklab\yii2\search\Search',
            'models' => [
                'app\models\Files',
                'app\models\Tags',
                'app\modules\user\models\User',
            ],
        ],
       /* 'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'viewPath' => '@app/modules/user/views/confirm',
            'transport' => [
                'class' => 'Swift_MailTransport',
            ],
        ],*/
        'eauth' => array(
            'class' => 'nodge\eauth\EAuth',
            'popup' => true, // Use the popup window instead of redirecting.
            'cache' => false, // Cache component name or false to disable cache. Defaults to 'cache' on production environments.
            'cacheExpire' => 0, // Cache lifetime. Defaults to 0 - means unlimited.
            'httpClient' => array(
                // uncomment this to use streams in safe_mode
                //'useStreamsFallback' => true,
            ),
            'services' => array( // You can change the providers and their classes.
                'google_oauth' => array(
                    // register your app here: https://code.google.com/apis/console/
                    'class' => 'nodge\eauth\services\GoogleOAuth2Service',
                    'clientId' => '642913282635-ubvigcgs6122os9ri1sgvnnq8ns790gr.apps.googleusercontent.com',
                    'clientSecret' => 'ZFIybIshXUOaTi34IKG9_TWC',
                    //'title' => 'google-plus'
                ),/*
                'google' => array(
                    'class' => 'nodge\eauth\services\GoogleOAuth2Service',
                    //'realm' => '*.example.org', // your domain, can be with wildcard to authenticate on subdomains.
                ),
                'yandex' => array(
                    'class' => 'nodge\eauth\services\YandexOAuth2Service',
                    //'realm' => '*.example.org', // your domain, can be with wildcard to authenticate on subdomains.
                ),
                'twitter' => array(
                    // register your app here: https://dev.twitter.com/apps/new
                    'class' => 'nodge\eauth\services\TwitterOAuth1Service',
                    'key' => '...',
                    'secret' => '...',
                ),
                
                'yandex_oauth' => array(
                    // register your app here: https://oauth.yandex.ru/client/my
                    'class' => 'nodge\eauth\services\YandexOAuth2Service',
                    'clientId' => '...',
                    'clientSecret' => '...',
                    'title' => 'Yandex (OAuth)',
                ),*/
                'facebook' => array(
                    // register your app here: https://developers.facebook.com/apps/
                    //'class' => 'nodge\eauth\services\FacebookOAuth2Service',
                    'class' => 'app\components\FacebookOAuth2Service',
                    'clientId' => '224200754735661',
                    'clientSecret' => 'ce6bdb347fa18a79ced945a5b11a7e22',
                ),
                /*'yahoo' => array(
                    'class' => 'nodge\eauth\services\YahooOpenIDService',
                    //'realm' => '*.example.org', // your domain, can be with wildcard to authenticate on subdomains.
                ),
                'linkedin' => array(
                    // register your app here: https://www.linkedin.com/secure/developer
                    'class' => 'nodge\eauth\services\LinkedinOAuth1Service',
                    'key' => '...',
                    'secret' => '...',
                    'title' => 'LinkedIn (OAuth1)',
                ),
                'linkedin_oauth2' => array(
                    // register your app here: https://www.linkedin.com/secure/developer
                    'class' => 'nodge\eauth\services\LinkedinOAuth2Service',
                    'clientId' => '...',
                    'clientSecret' => '...',
                    'title' => 'LinkedIn (OAuth2)',
                ),
                'github' => array(
                    // register your app here: https://github.com/settings/applications
                    'class' => 'nodge\eauth\services\GitHubOAuth2Service',
                    'clientId' => '...',
                    'clientSecret' => '...',
                ),
                'live' => array(
                    // register your app here: https://account.live.com/developers/applications/index
                    'class' => 'nodge\eauth\services\LiveOAuth2Service',
                    'clientId' => '...',
                    'clientSecret' => '...',
                ),
                'steam' => array(
                    'class' => 'nodge\eauth\services\SteamOpenIDService',
                    //'realm' => '*.example.org', // your domain, can be with wildcard to authenticate on subdomains.
                ),*/
                'vkontakte' => array(
                    // register your app here: https://vk.com/editapp?act=create&site=1
                    'class' => 'nodge\eauth\services\VKontakteOAuth2Service',
                    'clientId' => '6050368',
                    'clientSecret' => '38SbCrO18q0IVLIfKAVe',
                    'title' => 'vk'
                ),
                /*'mailru' => array(
                    // register your app here: http://api.mail.ru/sites/my/add
                    'class' => 'nodge\eauth\services\MailruOAuth2Service',
                    'clientId' => '...',
                    'clientSecret' => '...',
                ),
                'odnoklassniki' => array(
                    // register your app here: http://dev.odnoklassniki.ru/wiki/pages/viewpage.action?pageId=13992188
                    // ... or here: http://www.odnoklassniki.ru/dk?st.cmd=appsInfoMyDevList&st._aid=Apps_Info_MyDev
                    'class' => 'nodge\eauth\services\OdnoklassnikiOAuth2Service',
                    'clientId' => '...',
                    'clientSecret' => '...',
                    'clientPublic' => '...',
                    'title' => 'Odnoklas.',
                ),*/
            ),
        ),
        'vk' => [
            'class' => '\app\components\VKApi',
            'appId' => '6050368',
            'apiSecret' => '38SbCrO18q0IVLIfKAVe',
            'accessToken' => null,
        ],

        'apns' => [
            'class' => 'bryglen\apnsgcm\Apns',
            'environment' => YII_ENV_PROD ? \bryglen\apnsgcm\Apns::ENVIRONMENT_PRODUCTION : \bryglen\apnsgcm\Apns::ENVIRONMENT_SANDBOX,
            'pemFile' => YII_ENV_PROD ? dirname(__FILE__).'/certs/sona_prod_push.pem' : dirname(__FILE__).'/certs/dev_mg_push.pem',
            //'pemFile' => dirname(__FILE__).'/certs/SonaCert.pem',
            // 'retryTimes' => 3,
            'options' => [
                'sendRetryTimes' => 3
            ]
        ],
        'gcm' => [
            'class' => 'bryglen\apnsgcm\Gcm',
            //'apiKey' => YII_ENV_PROD ? 'AIzaSyCmxt-OnwRXvcXJ16OeseeDE9ie2A-Xr24': 'AIzaSyCmxt-OnwRXvcXJ16OeseeDE9ie2A-Xr24',
            //'apiKey' => YII_ENV_PROD ? 'AIzaSyALYBAkA6v2Nu6crI0cSgvVqQ7v3xn4c28': 'AIzaSyALYBAkA6v2Nu6crI0cSgvVqQ7v3xn4c28',
            'apiKey' => 'AAAAlbCeKks:APA91bEPQmqC69OyDOewNmoeh7EIy_lRj-nEuSMZDc9wspzobQ73lkHMgTQvp49U2hKyFdOl11bX86UAo6jNoH_c7qhW_ddYjd6_Z43RKIox5yX_oWCyJjNKJVzwl_zEz-pLVUuBZ9Fz',
        ],
        // using both gcm and apns, make sure you have 'gcm' and 'apns' in your component
        'apnsGcm' => [
            'class' => 'bryglen\apnsgcm\ApnsGcm',
            // custom name for the component, by default we will use 'gcm' and 'apns'
            //'gcm' => 'gcm',
            //'apns' => 'apns',
        ],

        'i18n' => [
            'translations' => [
                    'eauth' => [
                        'class' => 'yii\i18n\PhpMessageSource',
                        'basePath' => '@eauth/messages',
                    ],
                    'kvsocial' => [
                        'class' => 'yii\i18n\PhpMessageSource',
                        'basePath' => '@vendor/kartik-v/yii2-social/messages',
                        'sourceLanguage' => 'en'
                    ],

            ],
        ],
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'o0PmlDeQz7C091HEIPf-UR7DPSORyIgH',
            'parsers' => [
                'application/json' => 'yii\web\JsonParser',
            ]
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\modules\user\models\User',
            'enableAutoLogin' => true,
        ],
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
            //'defaultRoles' => ['guest'],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => false,
        ],

        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],

        'db' => require(__DIR__ . '/db.php'),
        'urlManager' => [
            'class' => 'yii\web\UrlManager',
            'enablePrettyUrl' => true,
            //'baseUrl' => '/site/fl/index',
            'showScriptName' => false,
            'rules' => [


               // 'login/<service:google|facebook|etc>' => 'site/login',
                'user/do/login/<service:google|facebook|etc>' => 'user/do/login',
                'gii' => 'gii',
                'gii/<controller:\w+>' => 'gii/<controller>',
                'gii/<controller:\w+>/<action:\w+>' => 'gii/<controller>/<action>',

                '<controller:(do)>/<action>' => 'user/do/<action>',

                'user' => 'user',
                'user/<controller:\w+>' => 'user/<controller>',
                'user/<controller:\w+>/<action:\w+>' => 'user/<controller>/<action>',
                'user/<controller:\w+>/<action:\w+>/<id:\d+>' => 'user/<controller>/edit-presentation',
                'user/<controller:\w+>/<action:\w+>/<id:\d+>' => 'user/<controller>/<action>',

                'user/do/login/<service:\w+>' => 'user/do/login',

                'authors' => 'authors/index',
                'author/<author:\w+>' => 'authors/view',
                'authors/<action>/<author:\w+>' => 'authors/<action>',

                'search' => 'search/index',
                'search/<search:\w+>' => 'search/index',

                'presentation' => 'presentation/index',
                'presentation/filter-rand' => 'presentation/filter-rand',
                'presentation/filter-all' => 'presentation/filter-all',
                'presentation/filter-month' => 'presentation/filter-month',
                'presentation/filter-day' => 'presentation/filter-day',
                //'author/<author:\w+>' => 'presentation/author',
                'presentation/view/<file:\w+>' => 'presentation/view',
                'presentation/tags/<tag:\w+>' => 'presentation/tags',
                'presentation/upload-step-two/<model_id:\w+>' => 'presentation/upload-step-two',
                'presentation/video-step-two/<model_id:\w+>' => 'presentation/video-step-two',
                'presentation/get-images/<model_id:\w+>' => 'presentation/get-images',

                'pres/<file:\d+>' => 'presentation/view',

                'about' => 'site/about',

                //API
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => 'api/presentations',
                    'except' => ['create', 'delete', 'update'],
                    'extraPatterns' => [
                        'GET search' => 'search',
                    ],
                ],
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => 'api/author',
                    'pluralize'=>false,
                    'except' => ['create', 'delete', 'update'],
                ],
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => 'api/auth',
                    'except' => ['create', 'delete', 'update'],
                    'extraPatterns' => [
                        'POST login' => 'login',
                    ],
                ],
            ],
        ],
    ],

    'modules' => [
        'user' => [
            'class' => 'app\modules\user\Module',
            'layoutPath' => '@app/views/layouts'
//            'class' => 'amnah\yii2\user\Module',
            // set custom module properties here ...
        ],
        'api' => [
            'class' => 'app\modules\api\Module',
        ],
        'social' => [
            // the module class
            'class' => 'kartik\social\Module',

            // the global settings for the disqus widget
           /* 'disqus' => [
                'settings' => ['shortname' => 'DISQUS_SHORTNAME'] // default settings
            ],*/

            // the global settings for the facebook plugins widget
            'facebook' => [
                'app_id' => '224200754735661',
                'appId' => '224200754735661',
                'app_secret' => 'ce6bdb347fa18a79ced945a5b11a7e22',
                'secret' => 'ce6bdb347fa18a79ced945a5b11a7e22'
            ],
            'vk' => [
                'app_id' => '6050368',
                'appId' => '6050368',
                'app_secret' => '38SbCrO18q0IVLIfKAVe',
                'secret' => '38SbCrO18q0IVLIfKAVe'
            ],

            // the global settings for the google plugins widget
            /*'google' => [
                'clientId' => 'GOOGLE_API_CLIENT_ID',
                'pageId' => 'GOOGLE_PLUS_PAGE_ID',
                'profileId' => 'GOOGLE_PLUS_PROFILE_ID',
            ],

            // the global settings for the google analytic plugin widget
            'googleAnalytics' => [
                'id' => 'TRACKING_ID',
                'domain' => 'TRACKING_DOMAIN',
            ],

            // the global settings for the twitter plugins widget
            'twitter' => [
                'screenName' => 'TWITTER_SCREEN_NAME'
            ],*/
        ],
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        'allowedIPs' => ['127.0.0.1', '82.193.155.241', '217.118.91.*', '*'],
    ];

    $config['components']['assetManager']['forceCopy'] = true;
}

return $config;
