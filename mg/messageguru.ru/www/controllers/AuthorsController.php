<?php
/**
 * Created by PhpStorm.
 * User: manchetten
 * Date: 21.03.17
 * Time: 22:51
 */

namespace app\controllers;

use app\modules\user\models\User;
use SebastianBergmann\CodeCoverage\Report\Html\File;
use Yii;
use yii\filters\AccessControl;
use app\components\BaseController;
use yii\web\NotFoundHttpException;
use yii\data\ActiveDataProvider;
use yii\data\SqlDataProvider;
use app\models\Files;
use app\models\Achievements;

/**
 * Контроллер для отображения авторов
 * Class AuthorsController
 * @package app\controllers
 */
class AuthorsController extends BaseController
{
    public $modelAuthor;

    public function renderAuthorProvider($condition = "")
    {
        $author_id = Yii::$app->request->get('author', '');
        //Есть автор - рендерим страницу автора
        if (!empty($author_id)) {
            $user = User::find()->joinWith('achievements')
                ->where([
                    //'login' => $author_login,
                    'user.id' => $author_id,
                    'user.status' => User::STATUS_ACTIVE
                ])
                ->one();
            if (!empty($user)) {

                $query = Files::find()->where(['user_id' => $user->id])->andWhere(["status" => 1]);
                if (!empty($condition)) {
                    $query->andWhere($condition);
                }

                $filesProvider = new ActiveDataProvider([
                    'query' => $query,
                    'pagination' => [
                        'pageSize' => 6,
                    ],
                ]);

                return ['author' => $user, 'filesProvider' => $filesProvider];
            } else throw new NotFoundHttpException();
        }
    }

    public function renderUserProvider($sort = "rating")
    {
        //Иначе - список всех авторов
        $usersProvider = new ActiveDataProvider([
            'query' => User::find()->joinWith('achievements')->where(['user.status' => User::STATUS_ACTIVE])->orderBy([$sort => SORT_DESC]),
            'pagination' => [
                'pageSize' => 6,
            ],
        ]);

        if (!empty($usersProvider)) {
            return ['usersProvider' => $usersProvider];
        } else throw new NotFoundHttpException();
    }

    public function actionIndex()
    {
        $usersProvider = new ActiveDataProvider([
            'query' => User::find()
                ->joinWith('achievements')
                ->where(['user.status' => User::STATUS_ACTIVE]),
            'pagination' => [
                'pageSize' => 7,
            ],
            'sort' => [
                'defaultOrder' => [
                    'created_at' => SORT_DESC,
                ],
                'attributes' => ['created_at']
            ]
        ]);

        if (!empty($usersProvider)) {
            return $this->render('authors_list', ['usersProvider' => $usersProvider, 'curr' => 1]);
        } else throw new NotFoundHttpException();

    }

    public function actionView($author)
    {
        $user = User::find()
            ->joinWith('achievements')
            ->where([
                //'login' => $author_login,
                'user.id' => $author,
                'user.status' => User::STATUS_ACTIVE
            ])
            ->one();

        if (!empty($user)) {
            $query = Files::find()
                ->where(['user_id' => $user->id])
                ->andWhere(['status' => Files::STATUS_ON]);

            $filesProvider = new ActiveDataProvider([
                'query' => $query,
                'pagination' => [
                    'pageSize' => 6,
                ],
                'sort' => [
                    'defaultOrder' => [
                        'created_at' => SORT_DESC,
                    ],
                    'attributes' => ['created_at']
                ]
            ]);

            return $this->render('author_page', ['author' => $user, 'filesProvider' => $filesProvider, 'curr' => 3]);
        } else throw new NotFoundHttpException();
    }

    /**
     * Фильтр по дате создания
     * @return string
     */
    public function actionFilterNew()
    {
        $query = User::find()
            ->where(['status' => User::STATUS_ACTIVE])
            ->orderBy(['created_at' => SORT_DESC]);

        $userProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 7,
            ],
            'sort' => [
                'defaultOrder' => [
                    'created_at' => SORT_DESC,
                ],
                'attributes' => ['created_at']
            ]
        ]);
        return $this->render('authors_list', ['usersProvider' => $userProvider, 'curr' => 1]);
    }

    /**
     * Фильтр продуктивность (PQ)
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionFilterProductivity()
    {
        $query = User::find()
            //->joinWith('files')
            ->where(['user.status' => User::STATUS_ACTIVE]);
            //->orderBy(['pq' => SORT_DESC]);

        $userProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 6,
            ],
            'sort' => [
                'defaultOrder' => [
                    'pq' => SORT_DESC,
                ],
                'attributes' => ['pq']
            ]
        ]);
        return $this->render('authors_list', ['usersProvider' => $userProvider, 'curr' => 2]);
    }

    /**
     * Фильтр по рейтингу (Achievements)
     * @return string
     */
    public function actionFilterRating()
    {
        $query = User::find()
            ->joinWith('achievements')
            ->where([
                'user.status' => User::STATUS_ACTIVE,
                'achievements.type' => Achievements::TYPE_LEVEL
            ])
            ->orderBy(['achievements.id' => SORT_DESC]); //TODO надо ввести веса или порядок

        //print_r($query->all()); die();

        $userProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 6,
            ],
            'sort' => [
                'defaultOrder' => [
                    'achievements.id' => SORT_DESC,
                ],
                'attributes' => ['achievements.id']
            ]
        ]);

        return $this->render('authors_list', ['usersProvider' => $userProvider, 'curr' => 3]);
    }

    public function actionFilterAuthorToday()
    {
        $authorProvider = $this->renderAuthorProvider('DATE(`created_at`) = CURDATE()');

        if (!empty($authorProvider['filesProvider'])) {
            return $this->render('author_page', ['author' => $authorProvider['author'], 'filesProvider' => $authorProvider['filesProvider'], 'curr' => 1]);
        } else {
            return $this->render('author_page', ['author' => $authorProvider['author'], 'filesProvider' => [], 'curr' => 1]);
        }
    }

    public function actionFilterAuthorMonth()
    {
        $authorProvider = $this->renderAuthorProvider('DATE(`created_at`) > DATE_SUB(CURRENT_DATE, INTERVAL 1 MONTH)');

        if (!empty($authorProvider['filesProvider'])) {
            return $this->render('author_page', ['author' => $authorProvider['author'], 'filesProvider' => $authorProvider['filesProvider'], 'curr' => 2]);
        } else {
            return $this->render('author_page', ['author' => $authorProvider['author'], 'filesProvider' => [], 'curr' => 2]);
        }
    }

    public function actionFilterAuthorAll()
    {
        $authorProvider = $this->renderAuthorProvider();

        if (!empty($authorProvider['filesProvider'])) {
            return $this->render('author_page', ['author' => $authorProvider['author'], 'filesProvider' => $authorProvider['filesProvider'], 'curr' => 3]);
        } else {
            return $this->render('author_page', ['author' => $authorProvider['author'], 'filesProvider' => [], 'curr' => 3]);
        }
    }
}