<?php
/**
 * Created by PhpStorm.
 * User: marduk
 * Date: 09.04.17
 * Time: 19:22
 */

namespace app\controllers;

use app\models\Files;
use app\models\FilesTemp;
use Yii;
use app\models\Likes;
use app\models\Subscribers;
use app\components\BaseController;
use app\models\CityOffices;
use \yii\web\Response;
use app\components\PushService;

class JsonGateController extends BaseController
{


    /**
     * Основной метод обработки запросов
     */
    public function actionIndex()
    {
        if (!empty($params['format']))
            switch ($params['format']) {
                case 'json':
                    Yii::$app->response->format = Response::FORMAT_JSON;
                    break;
                case 'xml':
                    Yii::$app->response->format = Response::FORMAT_XML;
                    break;
                case 'jsonp':
                    Yii::$app->response->format = Response::FORMAT_JSONP;
                    break;
                default:
                    Yii::$app->response->format = Response::FORMAT_JSON;
            }
        else
            Yii::$app->response->format = Response::FORMAT_JSON;
    }

    /**
     * функция добавления лайков
     * @return array
     */
    public function actionAddLike()
    {
        header('Content-Type: application/json');
        Yii::$app->response->format = Response::FORMAT_JSON;
        $obj_id = Yii::$app->request->post('obj_id', '');

        $file = Files::findOne($obj_id);

        if (empty($obj_id) || empty($file)) {
            return ["error" => "empty params obj_id"];
        }

        if (!empty(Yii::$app->getUser()->id)) {
            // Проверка, на установку лайков самому себе
            if($file->user_id == Yii::$app->getUser()->id) {
                $likes = Likes::find()->where(["obj_id" => $obj_id])->count();
                return ["success" => $likes];
            }

            /** @var Likes $model */
            $model = Likes::find()->where([
                'obj_id' => $obj_id,
                'user_by' => Yii::$app->getUser()->id
            ])->one();

            if (!empty($model->id)) {
                //уже был поставлен лайк - дизлайкаем
                $model->delete();
                $likes = Likes::find()->where(["obj_id" => $model->obj_id])->count();
                return ["success" => $likes];
            } else {
                $model = new Likes();
                $model->obj_id = $obj_id;
                $model->count = 1;
                $model->user_by = Yii::$app->getUser()->id;
                if ($model->save()) {
                    $likes = Likes::find()->where(["obj_id" => $model->obj_id])->count();
                    return ["success" => $likes];
                }
            }
        } else {
            $likes = Likes::find()->where(["obj_id" => $obj_id])->count();
            return ["error" => $likes];
        }
    }

    /**
     * Возвращает актуальную статистику по кол-ву обработанных слайдов презы
     * @param $id
     * @return array
     */
    function actionGetCount($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        /** @var FilesTemp $file_temp */
        $file_temp = FilesTemp::find()
            ->where([
                'file_id' => $id,
                //'status' => FilesTemp::STATUS_INWORK
            ])->one();

        if (!empty($file_temp)) {
            return [
                'status' => 'success',
                'data' => [
                    'pages_loaded' => is_null($file_temp->pages_loaded) ? 0 : $file_temp->pages_loaded,
                    'pages_total' => is_null($file_temp->pages) ? 0 : $file_temp->pages,
                    'need_conversion' => $file_temp->type != FilesTemp::TYPE_PDF ? true : false,
                    'is_converted' => is_null($file_temp->is_converted) ? 0 : $file_temp->is_converted
                ]
            ];
        } else {
            return [
                'status' => 'error',
            ];
        }
    }

    //функция запускает разбор презинтации в фоновом режиме
    public function actionFileParser()
    {
        header('Content-Type: application/json');
        Yii::$app->response->format = Response::FORMAT_JSON;

        $id = Yii::$app->request->get('id');

        $model = Files::findOne($id);
        $model->saveThumb($model->name);

        return true;

    }

    /**
     * функция возвращает содержимое папки
     * @param $id
     * @return array
     */
    public function actionGetImages($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $path = Yii::getAlias('@uploads/presentations/');
        $url = Yii::getAlias('@uploads_http/presentations/');

        /** @var FilesTemp $file_temp */
        $file_temp = FilesTemp::find()->where(['file_id' => $id])->one();

        if (!empty($file_temp)) {
            $pictures = [];

            $files = scandir($path . $file_temp->file_name);

            foreach ($files as $file) {
                $parse_int = explode("_", $file)[0];

                if ($file != '.' && $file != '..') {
                    $pictures[$parse_int] = $url . $file_temp->file_name . "/" . $file;
                }
            }

            return [
                'status' => 'success',
                'data' => $pictures
            ];
        } else {
            return [
                'status' => 'error',
            ];
        }
    }

    //функция подписки/отписки
    /*
    * return status
    * status 0 - не залогинен
    * status 1 - не подписан
    * status 2 - подписан
    */
    public function actionAddSubscribe()
    {
        header('Content-Type: application/json');
        Yii::$app->response->format = Response::FORMAT_JSON;
        $responce = Yii::$app->request->post();

        if (empty($responce["user_id"])) {
            return ["error" => "empty params user_id"];
        }

        if (!empty(Yii::$app->getUser()->id)) {
            $model = Subscribers::find()->where([
                "subscriber_id" => Yii::$app->getUser()->id,
                "user_id" => $responce["user_id"]
            ])->one();

            if (!empty($model->id)) {
                //уже был подписан - отписываемся
                $model->delete();
                
                return ["status" => 1];
            } else {
                $model = new Subscribers();
                $model->subscriber_id = Yii::$app->getUser()->id;
                $model->user_id = $responce["user_id"];

                if ($model->save()) {
                    // Отправляем пуши подписчикам
                    $push = new PushService();
                    $push->sendPushNewSubscriber($model->user_id, $model->subscriber_id);

                    return ["status" => 2];
                }
            }
        } else {
            return ["status" => 0];
        }
    }
}