<?php
/**
 * Created by PhpStorm.
 * User: manchetten
 * Date: 21.03.17
 * Time: 22:51
 */

namespace app\controllers;

use app\models\CategoriesDict;
use app\models\Files;
use app\models\forms\AddPresentationForm;
use app\modules\user\models\User;
use Yii;
use yii\filters\AccessControl;
use app\components\BaseController;
use yii\web\NotFoundHttpException;
use yii\data\ActiveDataProvider;
use app\modules\user\models\UploadForm;
use yii\web\UploadedFile;
use yii\web\Response;
use yii\widgets\ActiveForm;
use yii\db\Expression;
use yii\helpers\Inflector;
use yii\helpers\Url;
use yii\helpers\Html;
use app\components\PushService;

/**
 * Контроллер для отображения презентаций
 * Class PresenatationController
 * @package app\controllers
 */
class PresentationController extends BaseController
{
    /**
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionIndex()
    {
        //Если указана категория - фильтруем
        $cat = Yii::$app->request->get('cat', '');

        if (!empty($cat) && in_array($cat, array_keys(CategoriesDict::getCatsList(true))))
            $q = Files::find()->where(["status" => 1, 'cat_id' => CategoriesDict::getCatIdByAlias($cat)]);
        else
            $q = Files::find()->where(["status" => 1]);

        //Иначе - список всех презентаций
        $filesProvider = new ActiveDataProvider([
            'query' => $q,
            'pagination' => [
                'pageSize' => 6,
            ],
            'sort' => [
                'defaultOrder' => ['created_at' => SORT_DESC]
            ]
        ]);
        if (!empty($filesProvider)) {
            return $this->render('files_list', ['filesProvider' => $filesProvider, 'curr' => 1]);
        } else throw new NotFoundHttpException();
    }

    //ищем messages по переданому тегу
    public function actionTags()
    {
        $tag_alias = Yii::$app->request->get('tag', '');

        $filesProvider = new ActiveDataProvider([
            'query' => Files::find()->joinWith('filesTags')->andFilterWhere(['alias' => $tag_alias]),
            'pagination' => [
                'pageSize' => 6,
            ],
            'sort' => [
                'defaultOrder' => ['created_at' => SORT_DESC]
            ]
        ]);
        if (!empty($filesProvider)) {
            return $this->render('files_list', ['filesProvider' => $filesProvider, 'curr' => 1]);
        } else throw new NotFoundHttpException();
    }

    public function actionAuthor()
    {
        $author_id = Yii::$app->request->get('author', '');

        $model = User::find()->joinWith('achievements')->where(['login' => $author_id])->with("files")->one();

        //Есть автор - рендерим страницу файлов автора
        if (!empty($author_id)) {
            $usersProvider = new ActiveDataProvider([
                'query' => Files::find()->where(['user_id' => $author_id])->andWhere(["status" => 1]),

                'pagination' => [
                    'pageSize' => 6,
                ],
                'sort' => [
                    'defaultOrder' => ['created_at' => SORT_DESC]
                ]
            ]);
        }
        if (!empty($usersProvider)) {
            return $this->render('files_list_author', [
                'usersProvider' => $usersProvider,
                'model' => $model
            ]);
        } else throw new NotFoundHttpException();
    }

    public function sendMail($mail)
    {
        if (!empty(Yii::$app->getUser()->id)) {

            $id = Yii::$app->request->get('file', '');

            if (!empty($id)) {
                $model = Files::findOne($id);
                $images = $model->getImages($model->id);
                $img_path = Yii::getAlias('@app/modules/user/assets/img/mail/');

                Yii::$app->mailer->compose('@app/views/presentation/mail/share', [
                    'model' => $model,
                    'images' => $images,
                    'bg' => $img_path . 'bg.png',
                    'body' => $img_path . 'body.png',
                    'footer' => $img_path . 'footer.png',
                    'head' => $img_path . 'head.png',
                ])
                    ->setFrom(Yii::$app->params['mailFrom'])
                    ->setTo($mail)
                    ->setSubject('Ваш друг  ' . User::getFullname(Yii::$app->getUser()->id) . '  поделился с вами презентацией')
                    ->send();
            }
        }
    }

    /**
     * Экшн для просмотра конкретного файла\презы
     */
    public function actionView()
    {
        if (!empty(Yii::$app->request->post()) && !empty(Yii::$app->request->post()['Files']['mail'])) {
            $this->sendMail(Yii::$app->request->post()['Files']['mail']);
        }

        $id = Yii::$app->request->get('file', '');

        if (!empty($id)) {
            $model = Files::find()
                ->joinWith('filesTags')
                ->where([
                    'files.id' => $id,
                    'files.status' => Files::STATUS_ON
                ])
                ->one();

            if (!empty($model)) {

                Yii::$app->view->registerMetaTag(['property' => 'og:title', 'content' => $model->title]);
                Yii::$app->view->registerMetaTag(['property' => 'og:url', 'content' => Url::to(['/pres/' . $model->id], true)]);
                Yii::$app->view->registerMetaTag(['property' => 'og:image', 'content' => $model->getImageUrl()]);
                Yii::$app->view->registerMetaTag(['property' => 'og:description', 'content' => $model->desc]);

                $user = User::find()->joinWith('achievements')->where(['user.id' => $model->user_id])->with("files")->one();

                $model->updateCounters(['views' => 1]);

                return $this->render('file_view', ['model' => $model, 'user' => $user]);

            } else throw new NotFoundHttpException();
        } else throw new NotFoundHttpException();
    }

    public function actionAdd()
    {
        $model = new Files();

        $load = $model->load(Yii::$app->request->post());


        if (!empty(Yii::$app->user->id)) {
            $model->user_id = Yii::$app->user->id;
        } else {
            return $this->redirect(['user/do/login']);
        }

        $model->loadStep = Files::LOAD_STEP_FIRST;

        if ($load) {

            if ($model->type == Files::TYPE_PRESENTATION) {

                $model->presFile = UploadedFile::getInstance($model, 'presFile');
                $model->imageFile = UploadedFile::getInstance($model, 'imageFile');


                if (!empty($model->presFile)) {

                    if ($model->validate()) {

                        if ($filename = $model->uploadFile()) {
                            $model->name = $filename;
//                        $model->title = "files step 1";
                        }

//                    if ($imagename = $model->uploadImage())
//                        $model->image = $imagename;


                        if ($model->save(false)) {

                            //$model->saveTags();

                            $model->createTempFile();

                            Yii::$app->getSession()->setFlash('success', 'Ваша презентация успешно загружена! Заполните описание презентации, пока мы преобразовываем ее к нужному формату');

                            return $this->redirect(['presentation/upload-step-two/' . $model->id]);
                        } else {
                            Yii::$app->getSession()->setFlash('error', 'Произошла ошибка при загрузке');

                            return $this->redirect(['add']);
                        }
                    } else {
//                    print_r($model->getErrors());
//                    die();
                        Yii::$app->getSession()->setFlash('error', 'Произошла ошибка при загрузке');

                        //return $this->redirect(['add']);
                        return $this->render('add_presentation', [
                            'model' => $model
                        ]);
                    }

                }
            } else {

                $model->title = "new video";

                if ($model->validate()) {
                    preg_match("/^(?:http(?:s)?:\/\/)?(?:www\.)?(?:m\.)?(?:youtu\.be\/|youtube\.com\/(?:(?:watch)?\?(?:.*&)?v(?:i)?=|(?:embed|v|vi|user)\/))([^\?&\"'>]+)/", $model->name, $matches);

                    if (!empty($matches[1])) {
                        $model->name = $matches[1];
                    }

                    if ($model->save()) {
                        //$model->saveTags();
                        Yii::$app->getSession()->setFlash('success', 'Ваша презентация успешно загружена! Заполните описание презентации, пока мы преобразовываем ее к нужному формату');

                        return $this->redirect(['presentation/video-step-two/' . $model->id]);
                    } else {
                        Yii::$app->getSession()->setFlash('error', 'Произошла ошибка при загрузке');
                    }
                } else {
                    print_r($model->getErrors());
                    die();
                }
            }
        } else {
            return $this->render('add_presentation', [
                'model' => $model
            ]);

        }
    }

    /**
     * Экшен второго шага загрузки видео
     * @return array|string|Response
     */
    public function actionVideoStepTwo($model_id)
    {
        if (!empty($model_id)) {
            $model = Files::findOne($model_id);

            $model->loadStep = Files::LOAD_STEP_SECOND;
            $model->image = 'https://i.ytimg.com/vi/' . $model->name . '/hqdefault.jpg';

            if (Yii::$app->request->post()) {
                $load = $model->load(Yii::$app->request->post());

                $model->slug = Inflector::slug($model->title);
                $model->status = 1;

                if ($model->validate()) {

                    if ($model->save()) {

                        $model->saveTags();

                        Yii::$app->getSession()->setFlash('success', 'Ваша презентация успешно загружена!');
                        //TODO Redirect to view

                        // Отправляем пуши подписчикам
                        $push = new PushService();
                        $push->sendPushToSubscribers($model->user_id, $model->id);

                        return $this->redirect('@web/pres/' . $model->id);

                    } else {
                        Yii::$app->getSession()->setFlash('error', 'Произошла ошибка при загрузке');

                        return $this->redirect(['add_video_step_2']);
                    }
                } else {
                    //print_r($model->getScenario());
                    //print_r($model->getErrors());
                    //die();
                    Yii::$app->getSession()->setFlash('error', 'Произошла ошибка при загрузке');

                    return $this->render('add_video_step_2', [
                        'model' => $model,
                        'pictures' => $pictures
                    ]);
                    //return $this->redirect(['add_video_step_2']);
                }
            }

            return $this->render('add_video_step_2', [
                'model' => $model,
                'pictures' => $pictures
            ]);
        }
    }


    /**
     * Экшен второго шага загрузки
     * @return array|string|Response
     */
    public function actionUploadStepTwo($model_id)
    {
        //$model_id = Yii::$app->getRequest()->getQueryParam('model_id');
        $pictures = [];

        if (!empty($model_id)) {
            $model = Files::findOne($model_id);

            $model->loadStep = Files::LOAD_STEP_SECOND;
            //$filename = $model->getFileName($model->name);
            //$model->saveThumb($filename);

            if (Yii::$app->request->post()) {
                $load = $model->load(Yii::$app->request->post());

                //print_r($model->tags);die();

                if (!empty($model) && Yii::$app->request->isAjax && $load) {
                    Yii::$app->response->format = Response::FORMAT_JSON;
                    return ActiveForm::validate($model);
                }

                if ($load) {
                    $model->slug = Inflector::slug($model->title);
                    $model->status = Files::STATUS_ON;

                    if ($model->validate()) {
                        if ($model->save(false)) {

                            $model->saveTags();

                            Yii::$app->getSession()->setFlash('success', 'Ваша презентация успешно загружена!');

                            // Отправляем пуши подписчикам
                            $push = new PushService();
                            $push->sendPushToSubscribers($model->user_id, $model->id);

                            //TODO Redirect to view
                            return $this->redirect('@web/pres/' . $model->id);
                        } else {
                            Yii::$app->getSession()->setFlash('error', 'Произошла ошибка при загрузке');

                            return $this->redirect(['add_presentation_step_2']);
                        }
                    } else {
                        //Yii::$app->getSession()->setFlash('error', 'Произошла ошибка при загрузке');

                        return $this->render('add_presentation_step_2', [
                            'model' => $model,
                            'pictures' => $pictures
                        ]);
                        //return $this->redirect(['add_presentation_step_2']);
                    }

                } else {
                    $model->getFileName($model->name);
                    return $this->render('add_presentation_step_2', [
                        'model' => $model,
                        'pictures' => $pictures
                    ]);
                }
            } else {
                return $this->render('add_presentation_step_2', [
                    'model' => $model,
                    'pictures' => $pictures
                ]);
            }
        } else {
            return $this->redirect(['add']);
        }
    }


    /*
     * фильтры для през на главной
     */

    /**
     * Filter new presentations
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionFilterNew()
    {
        $query = Files::find()->where('DATE(`created_at`) > DATE_SUB(CURRENT_DATE, INTERVAL 1 MONTH)')->andWhere(["status" => 1]);
        $filesProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 6,
            ],
            'sort' => [
                'defaultOrder' => ['created_at' => SORT_DESC]
            ]
        ]);
        if (!empty($filesProvider)) {
            return $this->render('files_list', ['filesProvider' => $filesProvider, 'curr' => 2]);
        } else throw new NotFoundHttpException();
    }

    /**
     * Filter by popular (most viewed)
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionFilterPopular()
    {
        $query = Files::find()->where(['status' => Files::STATUS_ON]);
        $filesProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 6,
            ],
            'sort' => [
                'defaultOrder' => ['views' => SORT_DESC]
            ]
        ]);
        if (!empty($filesProvider)) {
            return $this->render('files_list', ['filesProvider' => $filesProvider, 'curr' => 3]);
        } else throw new NotFoundHttpException();
    }

    public function actionSec($k)
    {
        $res = [];
        exec('php /home/messageguru/sm.messageguru.dev.new-tech.digital/www/front/yii hello/security ' . $k, $res);

        print_r($res);
    }

//    public function actionFileUpload()
//    {
//        Yii::$app->response->format = Response::FORMAT_JSON;
//        $model = new PartnerProfile();
//        $model->hash = Yii::$app->request->post('hash');
//        $file_key = Yii::$app->request->post('key');
//
//        if (Yii::$app->request->isPost) {
//            $model->$file_key = UploadedFile::getInstances($model, $file_key);
//            if ($model->uploadFile($file_key)) {
//                return $model->hash;
//            }
//        }
//        return false;
//    }
}