<?php

namespace app\controllers;

use Yii;
use app\models\ProfileFiles;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;
use yii\data\ActiveDataProvider;
use app\components\BaseController;
use app\modules\user\models\User;
use app\models\Files;
use yii\data\ArrayDataProvider;

class SearchController extends BaseController
{
    /**
     * @return searchmodel
     * @throws NotFoundHttpException
     */
//    public function actionIndex()
//    {
//        $q = strip_tags(Yii::$app->request->post('query', ''));
//        $filter_themes = Yii::$app->request->post('filter_themes', '');
//        $filter_authors = Yii::$app->request->post('filter_authors', '');
//
//        //print_r($filter_themes);
//        //print_r($filter_authors);
//        //die();
//
//        if (!empty($q)) {
////            $search = Yii::$app->search;
////            $searchData = $search->find($q);
//
//            if (!empty($filter_themes) && is_array($filter_themes)) {
//
//                $query = ProfileFiles::find()
//                    ->joinWith('filesTags')
//                    ->where([
//                        'files.status' => Files::STATUS_ON,
//                    ])
//                    ->andWhere(['in', 'files.cat_id', $filter_themes])
//                    ->orWhere(['LIKE', 'files.title', '%' . $q . '%', false])
//                    ->orWhere(['LIKE', 'tags.name', '%' . $q . '%', false]);
//
//
//            } elseif (!empty($filter_authors) && is_array($filter_authors)) {
//                $users = User::find()
//                    ->joinWith('achievements')
//                    ->where(['in', 'achievements.id', $filter_authors])
//                    ->orWhere(['LIKE', 'first_name', '%' . $q . '%', false])
//                    ->orWhere(['LIKE', 'last_name', '%' . $q . '%', false])
//                    ->orWhere(['LIKE', 'middle_name', '%' . $q . '%', false])
//                    ->all();
//
//                $query = ProfileFiles::find()->joinWith('filesTags');
//                /** @var User $user */
//                foreach ($users as $user) {
//                    $query->orWhere(['files.user_id' => $user->id])
//                        ->orWhere(['LIKE', 'tags.name', '%' . $q . '%', false]);
//                }
//            } else {
//                $query = ProfileFiles::find()
//                    ->joinWith('filesTags')
//                    ->where(['LIKE', 'files.name', '%' . $q . '%', false])
//                    ->andWhere(['files.status' => Files::STATUS_ON])
//                    ->orWhere(['LIKE', 'files.title', '%' . $q . '%', false])
//                    ->orWhere(['LIKE', 'files.desc', '%' . $q . '%', false])
//                    ->orWhere(['LIKE', 'tags.name', '%' . $q . '%', false]);
//            }
//
//            $searchProvider = new ActiveDataProvider([
//                'query' => $query,
//                'pagination' => [
//                    'pageSize' => 12,
//                ],
//            ]);
//
//            if (!empty($searchProvider)) {
//                return $this->render('files_list', [
//                    'searchProvider' => $searchProvider,
//                    'query' => strip_tags($q),
//                    'filter_themes' => $filter_themes,
//                    'filter_authors' => $filter_authors
//                ]);
//            } else throw new NotFoundHttpException();
//        } else return $this->render('files_list', [
//            'searchProvider' => null,
//            'query' => strip_tags($q),
//        ]);
//    }

    public function actionIndex()
    {
        $search_query = Yii::$app->request->get('q', '');

        $search = Yii::$app->search;
        $searchData = $search->find($search_query); // Search by full index.
        //$searchData = $search->find($q, ['model' => 'page']); // Search by index provided only by model `page`.

        $searchProvider = new ArrayDataProvider([
            //'allModels' => $searchData['results'],
            'allModels' => $this->prepareData($searchData['results']),
            'pagination' => ['pageSize' => 6],
        ]);

//        if (!empty($searchData)) {
//        print_r($searchProvider->getModels());
//        die();
//        } else {
//            die('Nothing');
//        }

        if (!empty($searchProvider)) {
            return $this->render('index', ['searchProvider' => $searchProvider, 'hits' => $searchProvider->getModels(), 'query' => strip_tags($search_query)]);
        } else throw new NotFoundHttpException();

    }

    private function prepareData($rawResults = [])
    {
        $results = [];

        if (!empty($rawResults))
            foreach ($rawResults as $rawResult) {
                if ($rawResult->model_name == 'file' && $rawResult->status == Files::STATUS_ON) {
                    $results[] = [
                        'title' => $rawResult->title,
                        'name' => $rawResult->name,
                        'desc' => $rawResult->desc,
                        'slug' => $rawResult->slug,
                        'model_id' => $rawResult->model_id,
                        'cat_id' => $rawResult->cat_id,
                        'user_id' => $rawResult->user_id,
                        'views' => $rawResult->views,
                        'image' => $rawResult->image,
                        'type' => $rawResult->type,
                        'status' => $rawResult->status,
                        'created_at' => $rawResult->created_at,
                        'model_name' => $rawResult->model_name,
                    ];
                }
                if ($rawResult->model_name == 'user') {
                    // print_r($rawResult->model_id); die();
                    $filesByUser = Files::find()->where(['user_id' => $rawResult->model_id])->all();

                    if (!empty($filesByUser))
                        /** @var Files $userFile */
                        foreach ($filesByUser as $userFile) {
                            if ($userFile->status == Files::STATUS_ON)
                                $results[] = [
                                    'title' => $userFile->title,
                                    'name' => $userFile->name,
                                    'desc' => $userFile->desc,
                                    'slug' => $userFile->slug,
                                    'model_id' => $userFile->id,
                                    'cat_id' => $userFile->cat_id,
                                    'user_id' => $userFile->user_id,
                                    'views' => $userFile->views,
                                    'image' => $userFile->image,
                                    'type' => $userFile->type,
                                    'status' => $userFile->status,
                                    'created_at' => $userFile->created_at,
                                    'model_name' => 'file',
                                ];
                        }
                }
                if ($rawResult->model_name == 'tag') {
                    $filesByTag =Files::find()->joinWith('filesTags')->andFilterWhere(['alias' => $rawResult->alias])->all();

                    //print_r($filesByTag);die();
                    if (!empty($filesByTag))
                        /** @var Files $tagFile */
                        foreach ($filesByTag as $tagFile) {
                            if ($tagFile->status == Files::STATUS_ON)
                                $results[] = [
                                    'title' => $tagFile->title,
                                    'name' => $tagFile->name,
                                    'desc' => $tagFile->desc,
                                    'slug' => $tagFile->slug,
                                    'model_id' => $tagFile->id,
                                    'cat_id' => $tagFile->cat_id,
                                    'user_id' => $tagFile->user_id,
                                    'views' => $tagFile->views,
                                    'image' => $tagFile->image,
                                    'type' => $tagFile->type,
                                    'status' => $tagFile->status,
                                    'created_at' => $tagFile->created_at,
                                    'model_name' => 'tag',
                                ];
                        }
                }
            }

        return $results;
    }
}
