<?php

namespace app\controllers;

use app\components\PushService;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\ProfileFiles;
use app\modules\user\models\User;
use yii\db\Expression;
use app\models\Files;

class SiteController extends Controller
{
    public $model;
    public $modelAuthor;
    public $curr;
    public $curr_author;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
//        $push = new PushService();
//        $push->sendPushNewSubscriber(Yii::$app->user->id, 5);
//        die();
//        //$push->testPush(5, 174);
//        die('dgsdfgfdsg');


        $this->model = ProfileFiles::find()->where(["status" => 1])->limit(6)->orderBy(['created_at' => SORT_DESC])->all();
        
        $this->modelAuthor = User::find()->andWhere(["status" => User::STATUS_ACTIVE])->orderBy('rating')->limit(6)->all();
        return $this->render('index', [
            'messages' => $this->model,
            'modelAuthor' => $this->modelAuthor,
            'curr' => 3,
            'curr_author' => 3
        ]);
    }

    /*
     * фильтры для през на главной
     */

    public function actionFilterToday()
    {
        $this->model = ProfileFiles::find()->where('DATE(`created_at`) = CURDATE()')->andWhere(["status" => 1])->limit(6)->orderBy(['created_at' => SORT_DESC])->all();
        return $this->render('index', ['modelAuthor' => [], 'messages' => $this->model, 'curr' => 1]);
    }

    public function actionFilterMonth()
    {
        $this->model = ProfileFiles::find()->where('DATE(`created_at`) > DATE_SUB(CURRENT_DATE, INTERVAL 1 MONTH)')->andWhere(["status" => 1])->limit(6)->orderBy(['created_at' => SORT_DESC])->all();
        return $this->render('index', ['modelAuthor' => [], 'messages' => $this->model, 'curr' => 2]);
    }

    public function actionFilterAll()
    {
        $this->model = ProfileFiles::find()->where(["status" => 1])->limit(6)->orderBy(['created_at' => SORT_DESC])->all();
        return $this->render('index', ['modelAuthor' => [], 'messages' => $this->model, 'curr' => 3]);
    }

    public function actionFilterRand()
    {
        //$this->model = ProfileFiles::find()->where(["status" => 1])->orderBy(new Expression('rand()'))->limit(6)->orderBy(['created_at' => SORT_DESC])->all();

        $this->model = Files::find()
            ->where(["status" => 1])
            ->orderBy(new Expression('rand()'))
            ->limit(6)
            ->all();

        return $this->render('index', ['modelAuthor' => [], 'messages' => $this->model, 'curr' => 3]);
    }

    /*
 * фильтры авторов на главной
 */

    public function actionFilterAuthorToday()
    {
        $this->modelAuthor = User::find()->where('DATE(`created_at`) = CURDATE()')->orderBy('rating')->andWhere(["status" => User::STATUS_ACTIVE])->limit(6)->all();
        return $this->render('index', ['messages' => [], 'modelAuthor' => $this->modelAuthor, 'curr_author' => 1]);
    }

    public function actionFilterAuthorMonth()
    {
        $this->modelAuthor = User::find()->where('DATE(`created_at`) = DATE_SUB(CURRENT_DATE, INTERVAL 1 MONTH)')->andWhere(["status" => User::STATUS_ACTIVE])->orderBy('rating')->limit(6)->all();
        return $this->render('index', ['messages' => [], 'modelAuthor' => $this->modelAuthor, 'curr_author' => 2]);
    }

    public function actionFilterAuthorAll()
    {
        $this->modelAuthor = User::find()->limit(6)->orderBy('rating')->andWhere(["status" => User::STATUS_ACTIVE])->all();
        return $this->render('index', ['messages' => [], 'modelAuthor' => $this->modelAuthor, 'curr_author' => 3]);
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    /*
     * статичные страницы
     */
    public function actionEvents(){
        return $this->render('events');
    }

    public function actionEducation(){
        return $this->render('education');
    }

    public function actionBusiness(){
        return $this->render('business');
    }
}
