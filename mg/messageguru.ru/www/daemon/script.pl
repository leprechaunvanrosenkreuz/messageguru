use strict;
use warnings;
use Proc::Daemon;

Proc::Daemon::Init;

my $continue = 1;
$SIG{TERM} = sub { $continue = 0 };

while ($continue) {
     #do stuff
     system('php /home/messageguru/demo.messageguru.dev.new-tech.digital/www/f/yii files');
    sleep(5);
}
