<?php

use yii\db\Migration;

class m170225_121241_begin extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }


        $this->createTable('{{%cities}}', [
            'id' => $this->primaryKey(11),
            'name' => $this->string(255)->notNull(),
            'alias' => $this->string(255)->notNull(),
            'code' => $this->string(255),
            'status' => $this->integer(11)->notNull(),
        ], $tableOptions);

        $this->createIndex('idx-cities-code', 'cities', 'code');
        $this->createIndex('idx-cities-status', 'cities', 'status');

        $this->createTable('{{%user}}', [
            'id' => $this->primaryKey(11),
            'type' => $this->integer(4),
            'status' => $this->integer(4),
            'auth_key' => $this->string(32),
            'email_confirm_token' => $this->string(),
            'password_hash' => $this->string()->notNull(),
            'password_reset_token' => $this->string(),
            'avatar' => $this->string(255),
            'login' => $this->string(255)->notNull(),
            'first_name' => $this->string(255)->notNull(),
            'last_name' => $this->string(255)->notNull(),
            'middle_name' => $this->string(255)->notNull(),
            'email' => $this->string(255)->notNull(),
            'phone' => $this->string(255),
            'site' => $this->string(255),
            'vk' => $this->string(255),
            'fb' => $this->string(255),
            'ok' => $this->string(255),
            'city_id' => $this->integer(11),
            'rating' => $this->integer(11),

            'created_at' => $this->timestamp()->defaultValue('0000-00-00 00:00:00')->notNull(),
            'updated_at' => $this->timestamp()->defaultValue('0000-00-00 00:00:00')->notNull()

        ], $tableOptions);

        $this->createIndex('idx-user-email', 'user', 'email');
        $this->createIndex('idx-user-role', 'user', 'type');
        $this->createIndex('idx-user-login', 'user', 'login');
        $this->createIndex('idx-user-status', 'user', 'status');

        $this->createTable('{{%events}}', [
            'id' => $this->primaryKey(11),
            'name' => $this->string(255)->notNull(),
            'alias' => $this->string(255)->notNull(),
            'desc' => $this->text(),
            'place' => $this->string(255),
            'place_geo' => $this->string(255),
            'status' => $this->integer(4)->notNull(),

            'created_at' => $this->timestamp()->defaultValue('0000-00-00 00:00:00')->notNull(),
            'updated_at' => $this->timestamp()->defaultValue('0000-00-00 00:00:00')->notNull(),
            'created_by' => $this->integer(11)->notNull(),
            'updated_by' => $this->integer(11)->notNull(),
        ], $tableOptions);
          
        $this->createIndex('idx-events-alias', 'events', 'alias');
        $this->createIndex('idx-events-status', 'events', 'status');


        $this->createTable('{{%user_events}}', [
            'id' => $this->primaryKey(11),
            'user_id' => $this->integer(11),
            'events_id' => $this->integer(11)
        ], $tableOptions);

        $this->createIndex('idx-user_events-user_id', 'user_events', 'user_id');
        $this->createIndex('idx-user_events-events_id', 'user_events', 'events_id');

        $this->addForeignKey(
            'user_events-user_id', 
            '{{user_events}}', 
            'user_id', 
            '{{user}}', 
            'id', 
            'RESTRICT', 
            'CASCADE'
        );

        $this->addForeignKey(
            'user_events-events_id', 
            '{{user_events}}', 
            'events_id', 
            '{{events}}', 
            'id', 
            'RESTRICT', 
            'CASCADE'
        );

        $this->createTable('{{%categories_dict}}', [
            'id' => $this->primaryKey(11),
            'name' => $this->string(255)->notNull(),
            'alias' => $this->string(255)->notNull(),
            'weight' => $this->integer(11),

            'created_at' => $this->timestamp()->defaultValue('0000-00-00 00:00:00')->notNull(),
            'updated_at' => $this->timestamp()->defaultValue('0000-00-00 00:00:00')->notNull(),
            'created_by' => $this->integer(11)->notNull(),
            'updated_by' => $this->integer(11)->notNull(),
        ], $tableOptions);
          
        $this->createIndex('idx-categories_dict-alias', 'categories_dict', 'alias');
        $this->createIndex('idx-categories_dict-weight', 'categories_dict', 'weight');

        $this->createTable('{{%likes}}', [
            'id' => $this->primaryKey(11),
            'obj_id' => $this->integer(11)->notNull(),
            'count' => $this->integer(11),
            'user_by' => $this->integer(11)->notNull(),

            'created_at' => $this->timestamp()->defaultValue('0000-00-00 00:00:00')->notNull(),
            'updated_at' => $this->timestamp()->defaultValue('0000-00-00 00:00:00')->notNull(),
        ], $tableOptions);
          
        $this->createIndex('idx-likes-obj_id', 'likes', 'obj_id');
        $this->createIndex('idx-likes-count', 'likes', 'count');
        $this->createIndex('idx-likes-user_by', 'likes', 'user_by');


        $this->createTable('{{%shares}}', [
            'id' => $this->primaryKey(11),
            'obj_id' => $this->integer(11),
            'views_count' => $this->integer(11),
            'share_by' => $this->integer(11),

            'created_at' => $this->timestamp()->defaultValue('0000-00-00 00:00:00')->notNull(),
            'updated_at' => $this->timestamp()->defaultValue('0000-00-00 00:00:00')->notNull(),
        ], $tableOptions);
          
        $this->createIndex('idx-shares-obj_id', 'shares', 'obj_id');
        $this->createIndex('idx-shares-share_by', 'shares', 'share_by');

        $this->createTable('{{%files}}', [
            'id' => $this->primaryKey(11),
            'user_id' => $this->integer(11),
            'cat_id' => $this->integer(11),
            'type' => $this->integer(4),
            'name' => $this->string(255)->notNull(),
            'desc' => $this->text(),
            'path' => $this->string(255)->notNull(),

            'created_at' => $this->timestamp()->defaultValue('0000-00-00 00:00:00')->notNull(),
            'updated_at' => $this->timestamp()->defaultValue('0000-00-00 00:00:00')->notNull(),
        ], $tableOptions);
 
        $this->createIndex('idx-files-user_id', 'files', 'user_id');
        $this->createIndex('idx-files-cat_id', 'files', 'cat_id');
        $this->createIndex('idx-files-type', 'files', 'type');

        
        $this->createTable('{{%tags}}', [
            'id' => $this->primaryKey(11),
            'alias' => $this->string(255)->notNull(),
            'name' => $this->string(255)->notNull(),

            'created_at' => $this->timestamp()->defaultValue('0000-00-00 00:00:00')->notNull(),
            'updated_at' => $this->timestamp()->defaultValue('0000-00-00 00:00:00')->notNull(),
        ], $tableOptions);
 
        $this->createIndex('idx-tags-alias', 'tags', 'alias');


        $this->createTable('{{%files_tags}}', [
            'id' => $this->primaryKey(11),
            'files_id' =>  $this->integer(),
            'tags_id' =>  $this->integer()

        ], $tableOptions);
 
        $this->createIndex('idx-files_tags-files_id', 'files_tags', 'files_id');

        $this->createIndex('idx-files_tags-tags_id', 'files_tags', 'tags_id');

        $this->addForeignKey(
            'files_tags-files_id', 
            '{{files_tags}}', 
            'files_id', 
            '{{files}}', 
            'id', 
            'RESTRICT', 
            'CASCADE'
        );

        $this->addForeignKey(
            'files_tags-tags_id', 
            '{{files_tags}}', 
            'tags_id', 
            '{{tags}}', 
            'id', 
            'RESTRICT', 
            'CASCADE'
        );

        $this->createTable('{{%achievements}}', [
            'id' => $this->primaryKey(11),
            'alias' => $this->string(255)->notNull(),
            'name' => $this->string(255)->notNull(),
            'desc' => $this->text(),
            'image' => $this->string(255),
            'status' => $this->integer(4)->notNull(),

            'created_at' => $this->timestamp()->defaultValue('0000-00-00 00:00:00')->notNull(),
            'updated_at' => $this->timestamp()->defaultValue('0000-00-00 00:00:00')->notNull(),
        ], $tableOptions);
 
        $this->createIndex('idx-achievements-alias', 'achievements', 'alias');
        $this->createIndex('idx-achievements-status', 'achievements', 'status');


        $this->createTable('{{%user_achievements}}', [
            'id' => $this->primaryKey(11),
            'user_id' =>  $this->integer(11),
            'ach_id' =>  $this->integer(11)

        ], $tableOptions);
 
        $this->createIndex('idx-user_achievements-user_id', 'user_achievements', 'user_id');

        $this->createIndex('idx-user_achievements-ach_id', 'user_achievements', 'ach_id');

        $this->addForeignKey(
            'user_achievements-user_id',
            '{{user_achievements}}',
            'user_id', 
            '{{user}}', 
            'id', 
            'RESTRICT', 
            'CASCADE'
        );

        $this->addForeignKey(
            'user_achievements-tags_id',
            '{{user_achievements}}',
            'ach_id',
            '{{achievements}}',
            'id', 
            'RESTRICT', 
            'CASCADE'
        );
    
    }

    public function down()
    {
        $this->dropTable('{{%cities}}');
        $this->dropTable('{{%user}}');
        $this->dropTable('{{%events}}');
        $this->dropTable('{{%user_events}}');
        $this->dropTable('{{%categories_dict}}');
        $this->dropTable('{{%likes}}');
        $this->dropTable('{{%shares}}');
        $this->dropTable('{{%files}}');
        $this->dropTable('{{%tags}}');
        $this->dropTable('{{%files_tags}}');
        $this->dropTable('{{%achievements}}');
        $this->dropTable('{{%user_achievements}}');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
