<?php

use yii\db\Migration;

class m170302_173901_update_user_table extends Migration
{
    public function up()
    {
        return true;
//        $tableOptions = null;
//        if ($this->db->driverName === 'mysql') {
//            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
//        }
//
//        $this->createTable('{{%user}}', [
//            'id' => $this->primaryKey(11),
//            'type' => $this->integer(4),
//            'status' => $this->integer(4),
//            'auth_key' => $this->string(32),
//            'email_confirm_token' => $this->string(),
//            'password_hash' => $this->string()->notNull(),
//            'password_reset_token' => $this->string(),
//            'avatar' => $this->string(255),
//            'login' => $this->string(255)->notNull(),
//            'first_name' => $this->string(255)->notNull(),
//            'last_name' => $this->string(255)->notNull(),
//            'middle_name' => $this->string(255)->notNull(),
//            'email' => $this->string(255)->notNull(),
//            'phone' => $this->string(255),
//            'site' => $this->string(255),
//            'vk' => $this->string(255),
//            'fb' => $this->string(255),
//            'ok' => $this->string(255),
//            'city_id' => $this->integer(11),
//            'rating' => $this->integer(11),
//
//            'created_at' => $this->timestamp()->defaultValue('0000-00-00 00:00:00')->notNull(),
//            'updated_at' => $this->timestamp()->defaultValue('0000-00-00 00:00:00')->notNull()
//
//        ], $tableOptions);
//
//        $this->createIndex('idx-user-email', 'user', 'email');
//        $this->createIndex('idx-user-role', 'user', 'role');
//        $this->createIndex('idx-user-login', 'user', 'login');
//        $this->createIndex('idx-user-status', 'user', 'status');
    }

    public function down()
    {
        $this->dropTable('{{%user}}');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
