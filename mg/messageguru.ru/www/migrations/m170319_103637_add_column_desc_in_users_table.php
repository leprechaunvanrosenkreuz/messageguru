<?php

use yii\db\Migration;

class m170319_103637_add_column_desc_in_users_table extends Migration
{
    public function up()
    {
        $this->addColumn('user', 'desc', 'VARCHAR(255)');
        $this->renameColumn('user', 'rating', 'raiting');
    }

    public function down()
    {
        echo "m170319_103637_add_column_desc_in_users_table cannot be reverted.\n";

        return true;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
