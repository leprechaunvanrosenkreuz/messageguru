<?php

use yii\db\Migration;

class m170323_164955_add_columns_in extends Migration
{
    public function up()
    {
        $this->addColumn('files', 'views', 'integer');
        $this->addColumn('files', 'image', 'VARCHAR(255)');
    }

    public function down()
    {
        echo "m170323_164955_add_columns_in cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
