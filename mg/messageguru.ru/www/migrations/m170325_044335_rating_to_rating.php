<?php

use yii\db\Migration;

class m170325_044335_rating_to_rating extends Migration
{
    public function up()
    {
        $this->renameColumn('user', 'raiting', 'rating');
        $this->addColumn('user', 'profession', 'VARCHAR(255) AFTER ok');
        $this->addColumn('user', 'tw', 'VARCHAR(255) AFTER ok');
        $this->addColumn('user', 'lk', 'VARCHAR(255) AFTER ok');
        $this->addColumn('user', 'youtube', 'VARCHAR(255) AFTER ok');
    }

    public function down()
    {
        echo "m170325_044335_rating_to_rating cannot be reverted.\n";

        return true;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
