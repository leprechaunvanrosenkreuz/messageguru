<?php

use yii\db\Migration;

class m170330_151320_add_columns_in_files extends Migration
{
    public function up()
    {
        $this->addColumn('files', 'title', 'VARCHAR(255) AFTER name');
    }

    public function down()
    {
        echo "m170330_151320_add_columns_in_files cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
