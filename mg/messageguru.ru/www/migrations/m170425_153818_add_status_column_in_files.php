<?php

use yii\db\Migration;

class m170425_153818_add_status_column_in_files extends Migration
{
    public function up()
    {
        $this->addColumn('files', 'status', 'integer');
    }

    public function down()
    {
        echo "m170425_153818_add_status_column_in_files cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
