<?php

use yii\db\Migration;

class m170427_192041_add_column_slug_in_files extends Migration
{
    public function up()
    {
        //$this->addColumn('files', 'slug', 'VARCHAR(255) AFTER name');
        $this->addColumn('user', 'profession', 'VARCHAR(255) AFTER tw');
        $this->addColumn('user', 'city_id', 'integer AFTER tw');
    }


    public function down()
    {
        echo "m170427_192041_add_column_slug_in_files cannot be reverted.\n";

        return true;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
