<?php

use yii\db\Migration;

/**
 * Handles the creation of table `files_temp`.
 */
class m170521_112054_create_files_temp_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('files_temp', [
            'id' => $this->primaryKey(11),
            'file_name' => $this->string()->notNull(),
            'type' => $this->string()->notNull(),
            'pages' => $this->integer(4),
            'pages_loaded' => $this->integer(4),
            'is_converted' => $this->integer(4),
            'status' => $this->integer(4),

            'created_at' => $this->timestamp()->defaultValue('0000-00-00 00:00:00')->notNull(),
            'updated_at' => $this->timestamp()->defaultValue('0000-00-00 00:00:00')->notNull()
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('files_temp');
    }
}
