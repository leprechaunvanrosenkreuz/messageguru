<?php

use yii\db\Migration;

class m170524_154913_add_user_workplace_column extends Migration
{
    public function up()
    {
        $this->renameColumn('user', 'organization', 'place_work');
        $this->addColumn('user', 'position', 'string AFTER `place_work`');
    }

    public function down()
    {
        echo "m170524_154913_add_user_workplace_column cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
