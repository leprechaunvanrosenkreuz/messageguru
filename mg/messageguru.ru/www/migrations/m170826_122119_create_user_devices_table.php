<?php

use yii\db\Migration;

/**
 * Handles the creation of table `user_devices`.
 */
class m170826_122119_create_user_devices_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
return true;
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('user_devices');
    }
}
