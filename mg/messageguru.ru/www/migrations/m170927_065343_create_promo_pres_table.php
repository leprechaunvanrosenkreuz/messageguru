<?php

use yii\db\Migration;

/**
 * Handles the creation of table `promo_pres`.
 */
class m170927_065343_create_promo_pres_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }


        $this->createTable('{{%promo_pres}}', [
            'id' => $this->primaryKey(11),
            'order' => $this->integer(11),
            'file_id' => $this->integer(11)->notNull(),

            'created_at' => $this->timestamp()->defaultValue('0000-00-00 00:00:00'),
            'updated_at' => $this->timestamp()->defaultValue('0000-00-00 00:00:00')
        ], $tableOptions);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('promo_pres');
    }
}
