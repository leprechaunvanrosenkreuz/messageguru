<?php

namespace app\models;

use app\modules\user\models\User;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%achievements}}".
 *
 * @property integer $id
 * @property integer $type
 * @property string $alias
 * @property string $name
 * @property string $desc
 * @property string $image
 * @property integer $status
 * @property string $created_at
 * @property string $updated_at
 *
 * @property UserAchievements[] $userAchievements
 */
class Achievements extends \yii\db\ActiveRecord
{
    const TYPE_LEVEL = 1;

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%achievements}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type', 'alias', 'name', 'status'], 'required'],
            [['type', 'status'], 'integer'],
            [['desc'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['alias', 'name', 'image'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Type',
            'alias' => 'Alias',
            'name' => 'Name',
            'desc' => 'Desc',
            'image' => 'Image',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserAchievements()
    {
        return $this->hasMany(UserAchievements::className(), ['ach_id' => 'id']);
    }

    public static function getIdByAlias($alias)
    {
        $c = self::find()->where(['alias' => $alias])->one();

        return !empty($c) ? $c->id : '';
    }

    /**
     * Возвращает список всех ачивок
     * @return array
     */
    public static function getAchivevesList($key_attr = 'id')
    {
        $q = self::find()->all();
        return ArrayHelper::map($q, $key_attr, 'name');
    }

    /**
     * @param $userId
     * @param string $achieveAlias
     * @return bool
     */
    public static function assignAchieve($userId, $achieveAlias = 'stranger')
    {
        $ach = new UserAchievements();
        $ach->user_id = $userId;
        $ach->ach_id = self::getIdByAlias($achieveAlias);
        return $ach->save();
    }

    public static function userHasAchieve($userId, $achieveAlias)
    {
        if (!empty(UserAchievements::find()->where(['user_id' => $userId, 'ach_id' => self::getIdByAlias($achieveAlias)])->one()))
            return true;
        else return false;
    }

    public static function achieveGetProcents($achieveAlias)
    {
        $usersCount = User::find()->where(['status' => User::STATUS_ACTIVE])->count();
        $achieveCount = UserAchievements::find()->where(['ach_id' => self::getIdByAlias($achieveAlias)])->groupBy('user_id')->count();

        return ceil(($achieveCount / $usersCount) * 100);
    }
}