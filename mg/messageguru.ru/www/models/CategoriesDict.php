<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\behaviors\BlameableBehavior;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%categories_dict}}".
 *
 * @property integer $id
 * @property string $name
 * @property string $alias
 * @property integer $weight
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 */
class CategoriesDict extends \yii\db\ActiveRecord
{
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
                'value' => new Expression('NOW()'),
            ],
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'user_id',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%categories_dict}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'alias', 'created_by', 'updated_by'], 'required'],
            [['weight', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['name', 'alias'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'alias' => 'Alias',
            'weight' => 'Weight',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }

    /**
     * Возвращает список категорий
     * TODO добавить кэш
     * @return array
     */
    public static function getCatsList($named = false, $with_all = false)
    {
        if ($named) {
            if (!$with_all)
                return ArrayHelper::map(self::find()->orderBy('weight')->all(), 'alias', 'name');
            else
                return ArrayHelper::merge(ArrayHelper::map(self::find()->orderBy('weight')->all(), 'alias', 'name'), ['all' => 'Все темы']);
        } else
            return ArrayHelper::map(self::find()->orderBy('weight')->all(), 'id', 'name');
    }

    public static function getCatIdByAlias($alias)
    {
        return ArrayHelper::getValue(self::find()->where(['alias' => $alias])->one(), 'id');
    }

    public static function getAliasByCatId($id)
    {
        return ArrayHelper::getValue(self::find()->where(['id' => $id])->one(), 'alias');
    }
}