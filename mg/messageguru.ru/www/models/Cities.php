<?php
/**
 * Created by PhpStorm.
 * User: marduk
 * Date: 25.03.17
 * Time: 8:27
 */

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "cities".
 *
 * @property integer $id
 * @property string $name
 * @property string $alias
 * @property string $code
 * @property integer $status
 */
class Cities extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cities';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'alias', 'status'], 'required'],
            [['status'], 'integer'],
            [['name', 'alias', 'code'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'alias' => 'Alias',
            'code' => 'Code',
            'status' => 'Status',
        ];
    }

    /**
     * Возвращает список всех городов
     * @return array
     */
    public static function getCitiesList($key_attr = 'id')
    {
        $q = self::find()->all();
        return ArrayHelper::map($q, $key_attr, 'name');
    }
}