<?php
/**
 * Created by PhpStorm.
 * User: marduk
 * Date: 12.04.17
 * Time: 20:46
 */

namespace app\models;

use Yii;

class Elastic extends \yii\elasticsearch\ActiveRecord
{

    public function attributes()
    {

        return['name', 'email'];

    }

}
