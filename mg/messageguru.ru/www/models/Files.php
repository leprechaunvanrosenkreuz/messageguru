<?php

namespace app\models;

use app\components\MgHelpers;
use Yii;
use app\modules\user\models\User;
use app\models\Likes;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\db\Command;
use himiklab\yii2\search\behaviors\SearchBehavior;
use himiklab\thumbnail\EasyThumbnailImage;


/**
 * This is the model class for table "files".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $cat_id
 * @property string $name
 * @property string $desc
 * @property integer $type
 * @property integer $views
 * @property integer $image
 * @property string $title
 * @property string $created_at
 * @property string $updated_at
 * @property integer $status
 * @property string $slug
 * @property string $mail
 *
 * @property FilesTags[] $filesTags
 */
class Files extends \yii\db\ActiveRecord
{
    public $presFile;
    public $imageFile;
    public $tags;

    public $loadStep;

    public $mail;

    const TYPE_PRESENTATION = 1;
    const TYPE_YOUTUBE = 2;
    const TYPE_ARTICLE = 3;

    const STATUS_OFF = 0;
    const STATUS_ON = 1;

    const LOAD_STEP_FIRST = 1;
    const LOAD_STEP_SECOND = 2;

    public static function getTypesList()
    {
        return [
            self::TYPE_PRESENTATION => 'Презентация',
            //self::TYPE_YOUTUBE => 'Youtube',
            //self::TYPE_ARTICLE => 'Статья',
        ];
    }

    public static function getStatusList()
    {
        return [
            self::STATUS_OFF => 0,
            self::STATUS_ON => 1
        ];
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
                'value' => new Expression('NOW()'),
            ],
//            'search' => [
//                'class' => SearchBehavior::className(),
//                'searchScope' => function ($model) {
//                    /** @var \yii\db\ActiveQuery $model */
//                    $model->select(['title', 'desc']);
//                    //$model->andWhere(['indexed' => true]);
//                },
//                'searchFields' => function ($model) {
//                    /** @var self $model */
//                    return [
//                        ['name' => 'title', 'value' => $model->title, 'type' => SearchBehavior::FIELD_KEYWORD],
//                        ['name' => 'desc', 'value' => strip_tags($model->desc), 'type' => SearchBehavior::FIELD_TEXT],
////                        ['name' => 'alias', 'value' => $model->alias],
////                        ['name' => 'cat_id', 'value' => $model->cat_id],
////                        ['name' => 'city', 'value' => $model->city],
////                        ['name' => 'date', 'value' => $model->date],
////                        ['name' => 'image', 'value' => $model->image],
////                        ['name' => 'type', 'value' => 'event'],
//                        // ['name' => 'model', 'value' => 'page', 'type' => SearchBehavior::FIELD_UNSTORED],
//                    ];
//                }
//            ],
            'search' => [
                'class' => SearchBehavior::className(),
                'searchScope' => function ($model) {
                    /** @var \yii\db\ActiveQuery $model */
                    $model->select([
                        'id',
                        'title',
                        'desc',
                        'name',
                        'slug',
                        'cat_id',
                        'user_id',
                        'views',
                        'image',
                        'status',
                        //'type',
                        'created_at'
                    ]);
                    //$model->andWhere(['indexed' => true]);
                },
                'searchFields' => function ($model) {
                    /** @var self $model */
                    return [
                        ['name' => 'title', 'value' => $model->title, 'type' => SearchBehavior::FIELD_TEXT],
                        ['name' => 'name', 'value' => $model->name],
                        ['name' => 'desc', 'value' => $model->desc, 'type' => SearchBehavior::FIELD_TEXT],
                        ['name' => 'slug', 'value' => $model->slug],
                        ['name' => 'model_id', 'value' => $model->id, 'type' => SearchBehavior::FIELD_KEYWORD],
                        ['name' => 'cat_id', 'value' => $model->cat_id],
                        ['name' => 'user_id', 'value' => $model->user_id],
                        ['name' => 'views', 'value' => $model->views],
                        ['name' => 'image', 'value' => $model->image],
                        ['name' => 'type', 'value' => $model->type],
                        ['name' => 'created_at', 'value' => $model->created_at],
                        ['name' => 'status', 'value' => $model->status],
                        ['name' => 'model_name', 'value' => 'file'],
                        // ['name' => 'model', 'value' => 'page', 'type' => SearchBehavior::FIELD_UNSTORED],
                    ];
                }
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%files}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'cat_id', 'type', 'views', 'status'], 'integer'],
            [['desc', 'title', 'created_at', 'updated_at', 'slug'], 'string'],
            ['tags', 'each', 'rule' => ['string'], 'on' => 'step2'],
            [['name', 'image', 'title'], 'string', 'max' => 255],
            [['user_id'], 'required', 'message' => 'Заполните поле'],
            [['image'], 'required', 'message' => 'Выберите главное изображение презентации из списка слева', 'on' => 'step2'],
            [['name', 'title'], 'required', 'message' => 'Заполните поле', 'on' => 'step2'],
            ['desc', 'string', 'max' => 1000, 'message' => 'Заполните описание от 500 до 1000 знаков', 'on' => 'step2'],
            [['presFile'], 'file', 'extensions' => ['pdf', 'pptx', 'ppt'],
                'checkExtensionByMimeType' => true,
                'mimeTypes' => [
                    'application/pdf',
                    'application/vnd.oasis.opendocument.presentation',
                    'application/vnd.ms-powerpoint',
                    'application/mspowerpoint',
                    'application/powerpoint',
                    'application/x-mspowerpoint',
                    'application/vnd.openxmlformats-officedocument.presentationml.presentation'
                ],
                'skipOnEmpty' => false
            ],
            [['presFile', 'tags'], 'safe', 'on' => 'step2'],
            [['presFile'], 'safe', 'on' => 'step1video'],
            [['tags', 'loadStep'], 'safe']
        ];
    }

    public function scenarios()
    {
        $scenarios = parent::scenarios();

        $scenarios['step1video'] = [
            'name',
            'type',
            'user_id',
            'loadStep'
        ];

        $scenarios['step2'] = [
            'cat_id',
            'name',
            'desc',
            'type',
            'user_id',
            'image',
            'title',
            'tags',
            'loadStep'
        ];

        return $scenarios;
    }

    public function beforeValidate()
    {

        if ($this->loadStep == Files::LOAD_STEP_SECOND)
            $this->scenario = 'step2';
        else {
            if ($this->type == Files::TYPE_PRESENTATION)
                $this->scenario = self::SCENARIO_DEFAULT;
            else $this->scenario = 'step1video';
        }

        return parent::beforeValidate();
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'cat_id' => 'Cat ID',
            'name' => 'Name',
            'desc' => 'Desc',
            'title' => 'Title',
            'status' => 'Status',
            'type' => 'Type'
        ];
    }

    public function getImageUrl()
    {
        $path_image = Yii::getAlias('@uploads/presentations/thumbs/');

        if ($this->type == Files::TYPE_YOUTUBE) {
            return $image = 'https://i.ytimg.com/vi/' . $this->name . '/hqdefault.jpg';
        } else {
            if (!empty($this->image)) {
                $image = $this->image;
            } else {
                $image = $this->getImages($this->id)[0];

                if (empty($image))
                    $image = Yii::$app->params['uploadsUrl'] . '/profile/no_ava.png';
            }
        }

        //return EasyThumbnailImage::thumbnailFileUrl($image, 215, 153, EasyThumbnailImage::THUMBNAIL_OUTBOUND, 95);

        return $image;
    }

    public function saveTags()
    {
        if (!empty($this->tags)) {
            foreach ($this->tags AS $val) {
                if (!is_numeric($val)) {
                    $stripTag = MgHelpers::removeHashtags($val);

                    //Добавляем новый тег
                    $modelTags = new Tags();
                    $modelTags->alias = $stripTag;
                    $modelTags->name = $stripTag;
                    $modelTags->save();

                    $this->link('filesTags', $modelTags); //и линкуем
                } else {
                    $tags_db = Tags::findOne($val);

                    if (!empty($tags_db)) {
                        $this->link('filesTags', $tags_db); //линкуем с каждым найденным тегом
                    }
                }
            }
            return true;
        } else {
            return false;
        }
    }

    public static function removeFiles($file_id)
    {
        $model = self::findOne($file_id);
        $model->status = self::STATUS_OFF;

        return $model->update(false);
    }

    public static function getThumbName($filename)
    {
        if (!is_file($filename)) {
            return self::getFileName($filename) . "_thumb.png";
        } else {
            //todo сюда картинку по дефолту поставить
            return "";
        }
    }


    public static function getFileName($filename)
    {
        if (!is_file($filename)) {
            return explode(".", $filename)[0];
        } else {
            return "";
        }
    }

    public function getPages($filename)
    {
        $pdftext = file_get_contents($filename);
        $num = preg_match_all("/\/Page\W/", $pdftext, $dummy);
        return $num;
    }

    public function saveThumb($filename)
    {
        if (!empty($filename)) {
            //$model = Files::find()->where(['name' => 'filename'])->one();
            $path = Yii::getAlias('@uploads/presentations/');

            if (!is_dir($path))
                mkdir($path, 0777, true);

            $dir = explode(".", $filename)[0] . "/";
            if (!is_dir($path . $dir))
                mkdir($path . $dir, 0777, true);

            $i = 0;
            $arr = array();

            $typeFile = end(explode(".", $filename));
            if (!is_file($path . self::getFileName($filename) . ".pdf")) {
                if ($typeFile == "ppt" || $typeFile == "pptx") {
                    exec("unoconv " . $path . $filename . " " . $path . self::getFileName($filename) . ".pdf", $out, $var);
                    $filename = self::getFileName($filename) . ".pdf";
                }
                return false;
            } else {
                $filename = self::getFileName($filename) . ".pdf";
                $this->parseFile($path, $filename);
                return true;
            }
        }
        return false;
    }

    public function parseFile($path, $filename)
    {
        $cPages = $this->getPages($path . $filename);
        $dir = explode(".", $filename)[0] . "/";

        for ($i = 0; $i < $cPages - 1; $i++) {
            exec("convert -background white -quality 100 -trim -density 300 -alpha remove " . $path . $filename . "[" . $i . "] " . $path . $dir . $i . "_" . self::getThumbName($filename), $arr);
            //print_r($arr);
            echo $i;
        }
        return $i;
    }

    public function uploadFile()
    {
        if (!empty($this->presFile)) {
            $path = Yii::getAlias('@uploads/presentations/');

            if (!is_dir($path))
                mkdir($path, 0777, true);

            $filename = 'pres_' . uniqid(time()) . '.' . $this->presFile->extension;

            if ($this->presFile->saveAs($path . $filename)) {
                return $filename;
            } else
                return false;

        } else
            return false;

        //$uploaded_files = array_diff(scandir($path . $hash), ['.', '..']);
        //$count_uploaded_files = count($uploaded_files) ? count($uploaded_files) : 0;

    }

    /**
     * Создает временную запись для файла в БД, для парсинга на втором шаге
     * @return bool
     */
    public function createTempFile()
    {
        $t_file = new FilesTemp();
        $path = Yii::getAlias('@uploads/presentations/');
        $prep_file = explode(".", $this->name);

        $t_file->file_name = $prep_file[0];
        $t_file->file_extension = end($prep_file);
        $t_file->file_id = $this->id;
        $t_file->type = end($prep_file);
        if (end($prep_file) == FilesTemp::TYPE_PDF)
            $t_file->pages = $this->getPages($path . $this->name);
        $t_file->status = FilesTemp::STATUS_NEW;

        return $t_file->save();
    }

    public function uploadImage()
    {
        if (!empty($this->imageFile)) {
            $path = Yii::getAlias('@uploads/presentations/thumbs/');

            if (!is_dir($path))
                mkdir($path, 0777, true);

            $filename = 'thpres_' . uniqid(time()) . '.' . $this->imageFile->extension;

            if ($this->imageFile->saveAs($path . $filename)) {
                return $filename;
            } else
                return false;

        } else
            return false;

        //$uploaded_files = array_diff(scandir($path . $hash), ['.', '..']);
        //$count_uploaded_files = count($uploaded_files) ? count($uploaded_files) : 0;

    }

    /**
     * функция возвращает содержимое папки
     * @param $model_id
     * @return array
     */
    public function getImages($model_id)
    {
        $path = Yii::getAlias('@uploads/presentations/');
        $url = Yii::getAlias('@uploads_http/presentations/');

        if (!empty($model_id)) {
            $model = self::findOne($model_id);
            $album = $model->getFileName($model->name);

            $i = 0;
            $pictures = array();

            if (is_dir($path . $album)) {
                $files = scandir($path . $album);
            } else {
                $files = array();
            }

            foreach ($files as $file) {
                $parse_int = explode("_", $file)[0];
                if ($file != '.' && $file != '..') {
                    $pictures[$parse_int] = $url . $album . "/" . $file;
                }
            }

            ksort($pictures);

            return $pictures;
        } else {
            return [];
        }
    }

    //рейтинг - количество файлов с +10 лайками
    public static function getPQ($user_id)
    {
        $sql = 'SELECT * FROM `files` as f
                     WHERE  f.user_id = ' . $user_id . ' and
                          (SELECT count(*) FROM `likes` as l WHERE l.obj_id =f.id) >= 3';
        $connection = Yii::$app->db;
        $command = $connection->createCommand($sql);
        return $command->execute();  // execute the non-query SQL
        //  $dataReader=$command->query(); // execute a query SQL
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFilesTags()
    {
        return $this
            ->hasMany(Tags::className(), ['id' => 'tags_id'])
            ->viaTable(FilesTags::tableName(), ['files_id' => 'id']);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this
            ->hasMany(User::className(), ['id' => 'user_id']);
    }

    /*    public function getFilesTags()
        {
            return $this
                ->hasMany(Tags::className(), ['files_id' => 'id' ])
                ->viaTable(FilesTags::tableName(), ['files_tags' => 'id']);
        }*/
}