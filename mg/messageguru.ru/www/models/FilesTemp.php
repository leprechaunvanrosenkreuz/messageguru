<?php
/**
 * Created by PhpStorm.
 * User: manchetten
 * Date: 21.05.17
 * Time: 16:27
 */

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;

/**
 * This is the model class for table "{{%files_temp}}".
 *
 * @property integer $id
 * @property integer $file_id
 * @property string $file_name
 * @property string $file_extension
 * @property string $type
 * @property integer $pages
 * @property integer $pages_loaded
 * @property integer $is_converted
 * @property integer $status
 * @property string $created_at
 * @property string $updated_at
 */
class FilesTemp extends \yii\db\ActiveRecord
{
    const TYPE_PPT = 'ppt';
    const TYPE_PPTX = 'pptx';
    const TYPE_PDF = 'pdf';

    const STATUS_NEW = 1;
    const STATUS_INWORK = 2;
    const STATUS_DONE = 3;
    const STATUS_ERROR = 4;

    public static function getStatusesList()
    {
        return [
            self::STATUS_NEW => 'Новая',
            self::STATUS_INWORK => 'В работе',
            self::STATUS_DONE => 'Завершено',
            self::STATUS_ERROR => 'Ошибка',
        ];
    }


    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
                'value' => new Expression('NOW()'),
            ],
        ];
    }


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%files_temp}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['file_name', 'type'], 'required'],
            [['file_id', 'pages', 'pages_loaded', 'is_converted', 'status'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['file_name', 'file_extension', 'type'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'file_name' => 'File Name',
            'type' => 'Type',
            'pages' => 'Pages',
            'pages_loaded' => 'Pages Loaded',
            'is_converted' => 'Is Converted',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}