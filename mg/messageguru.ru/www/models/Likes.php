<?php

namespace app\models;

use SebastianBergmann\CodeCoverage\Report\Html\File;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;

/**
 * This is the model class for table "likes".
 *
 * @property integer $id
 * @property integer $obj_id
 * @property integer $count
 * @property integer $user_by
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Files[] $presentation
 */
class Likes extends \yii\db\ActiveRecord
{
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'likes';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['obj_id', 'user_by'], 'required'],
            [['obj_id', 'count', 'user_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'obj_id' => 'Obj ID',
            'count' => 'Count',
            'user_by' => 'User By',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPresentation()
    {
        return $this->hasOne(Files::className(), ['obj_id' => 'id']);
    }

    public static function getCountLike($file_id)
    {
        $model = self::find()->where(["obj_id" => $file_id])->all();
        return count($model);
    }

    public static function isLikedByUser($obj_id, $user_id = null)
    {
        if (Yii::$app->user->isGuest)
            return false;

        if (empty($user_id))
            $user_id = Yii::$app->user->getId();

        $obj = self::find()->where([
            'obj_id' => $obj_id,
            'user_by' => $user_id,
        ])->one();

        return !empty($obj) ? true : false;
    }

    public static function addlike($responce)
    {

        if (empty($responce["obj_id"])) {
            return ["error" => "empty params obj_id"];
        }

        if (!empty(Yii::$app->getUser()->id)) {
            $model = self::find()->where([
                "obj_id" => $responce["obj_id"],
                "user_by" => Yii::$app->getUser()->id
            ])->one();

            if (!empty($model->id)) {
                //уже был поставлен лайк - дизлайкаем
                $model->delete();
                $likes = self::find()->where(["obj_id" => $model->obj_id])->all();
                return ["success" => count($likes)];
            } else {
                $model = new Likes();
                $model->obj_id = $responce["obj_id"];
                $model->count = 1;
                $model->user_by = Yii::$app->getUser()->id;

                if ($model->save()) {
                    $likes = Likes::find()->where(["obj_id" => $model->obj_id])->all();

                    return ["success" => count($likes)];
                }
            }
        } else {
            return ["error" => "only register user"];
        }
    }
}
