<?php
/**
 * Created by PhpStorm.
 * User: marduk
 * Date: 09.03.17
 * Time: 17:33
 */

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "tags".
 *
 * @property integer $id
 * @property string $alias
 * @property string $name
 * @property string $created_at
 * @property string $updated_at
 *
 * @property FilesTags[] $filesTags
 */
class ProfileFiles extends Files
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'files';
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFilesTags()
    {
        return $this
            ->hasMany(Tags::className(), ['id' => 'tags_id' ])
            ->viaTable(FilesTags::tableName(), ['files_id' => 'id']);
    }

}
