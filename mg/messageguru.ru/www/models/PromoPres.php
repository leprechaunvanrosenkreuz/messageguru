<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "promo_pres".
 *
 * @property integer $id
 * @property integer $order
 * @property integer $file_id
 * @property string $created_at
 * @property string $updated_at
 */
class PromoPres extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'promo_pres';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order', 'file_id'], 'integer'],
            [['file_id'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'order' => 'Order',
            'file_id' => 'File ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}