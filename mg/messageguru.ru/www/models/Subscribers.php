<?php

namespace app\models;

use Yii;
use app\modules\user\models\User;

/**
 * This is the model class for table "subscribers".
 *
 * @property integer $user_id
 * @property integer $subscriber_id
 *
 * @property User $user
 * @property User $subscriber
 */
class Subscribers extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'subscribers';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'subscriber_id'], 'integer'],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
            [['subscriber_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['subscriber_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'subscriber_id' => 'Subscriber ID',
            'user_id' => 'User ID',
        ];
    }

    /**
     * @param $user_id
     * @param $check_user_id
     * @return bool
     */
    public static function checkIfSubscriber($user_id, $check_user_id){
        $model = self::find()->where(['user_id' => $check_user_id, 'subscriber_id' => $user_id])->one();

        if(empty($model))
            return false;
        else return true;
    }

    public static function getStatus($user_id){
        $model = self::find()->where(["user_id" => $user_id])->all();
        return count($model);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubscriber()
    {
        return $this->hasOne(User::className(), ['id' => 'subscriber_id']);
    }
}
