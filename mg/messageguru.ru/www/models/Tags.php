<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
use himiklab\yii2\search\behaviors\SearchBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;

/**
 * This is the model class for table "tags".
 *
 * @property integer $id
 * @property string $alias
 * @property string $name
 * @property string $created_at
 * @property string $updated_at
 *
 * @property FilesTags[] $filesTags
 */
class Tags extends \yii\db\ActiveRecord
{
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
                'value' => new Expression('NOW()'),
            ],
            'search' => [
                'class' => SearchBehavior::className(),
                'searchScope' => function ($model) {
                    /** @var \yii\db\ActiveQuery $model */
                    $model->select([
                        'id',
                        'alias',
                        'name',
                        'created_at'
                    ]);
                    //$model->andWhere(['indexed' => true]);
                },
                'searchFields' => function ($model) {
                    /** @var self $model */
                    return [
                        ['name' => 'alias', 'value' => $model->alias, 'type' => SearchBehavior::FIELD_TEXT],
                        ['name' => 'name', 'value' => $model->name, 'type' => SearchBehavior::FIELD_TEXT],
                        ['name' => 'model_id', 'value' => $model->id, 'type' => SearchBehavior::FIELD_KEYWORD],
                        ['name' => 'created_at', 'value' => $model->created_at],
                        ['name' => 'model_name', 'value' => 'tag'],
                    ];
                }
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tags';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['alias', 'name'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['alias', 'name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'alias' => 'Alias',
            'name' => 'Name',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFilesTags()
    {
        return $this->hasMany(FilesTags::className(), ['tags_id' => 'id']);
    }

    /**
     * Возвращает список всех страниц
     * @return array
     */
    public static function getTagsList($key_attr = 'id')
    {
        $q = self::find()->all();
        return ArrayHelper::map($q, $key_attr, 'name');
    }

}
