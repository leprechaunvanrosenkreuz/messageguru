<?php

namespace app\models;

use Yii;
use app\modules\user\models\User;

/**
 * This is the model class for table "{{%user_achievements}}".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $ach_id
 *
 * @property Achievements $ach
 * @property User $user
 */
class UserAchievements extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user_achievements}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'ach_id'], 'integer'],
            [['ach_id'], 'exist', 'skipOnError' => true, 'targetClass' => Achievements::className(), 'targetAttribute' => ['ach_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'ach_id' => 'Ach ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAch()
    {
        return $this->hasOne(Achievements::className(), ['id' => 'ach_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}