<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;

/**
 * This is the model class for table "{{%user_devices}}".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $device_id
 * @property integer $is_android
 * @property integer $is_ios
 * @property integer $confirm_pushes
 * @property string $created_at
 * @property string $updated_at
 */
class UserDevices extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user_devices}}';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'device_id'], 'required'],
            [['user_id', 'is_android', 'is_ios', 'confirm_pushes'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['device_id'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'device_id' => 'Device ID',
            'is_android' => 'Is Android',
            'is_ios' => 'Is Ios',
            'confirm_pushes' => 'Confrim Pushes',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}