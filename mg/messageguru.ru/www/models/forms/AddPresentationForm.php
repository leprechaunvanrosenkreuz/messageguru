<?php
/**
 * Created by PhpStorm.
 * User: manchetten
 * Date: 06.04.17
 * Time: 19:04
 */


namespace app\models\forms;

use Yii;
use yii\base\Model;
use yii\helpers\StringHelper;

class AddPresentationForm extends Model
{
    public $name;
    public $desc;
    public $type;

    public $presentation;
    public $photos;
    public $files_path;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'desc', 'type'], 'required', 'message' => 'Заполните поле'],

            ['desc', 'string', 'max' => 1000, 'message' => 'Заполните описание от 500 до 1000 знаков'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id'          => 'ID',
            'user_id'     => 'User ID',
            'name'        => 'Название',
            'site'        => 'Сайт',
            'addr'        => 'Основной адрес',
            'phones'      => 'Контактные телефоны',
            'email'       => 'Электронная почта',
            'description' => 'Описание компании',
            'active'      => 'Active',
            'moderated'   => 'Moderated',
            'has_account' => 'Has account',
            'created_at'  => 'Created At',
            'updated_at'  => 'Updated At',
            'logo'        => 'Логотип',
            'photos'      => 'Имиджевые фото',
        ];
    }

    public function uploadFile($file_key)
    {

        if (!empty($filemodel->$fileAttr)) {

            $filemodel->$fileAttr->saveAs($this->getBasePath() . '/web/uploads/' . $this->getUserId() . '/' . $filemodel->$fileAttr->baseName . '.' . $filemodel->$fileAttr->extension);

            return $filemodel->$fileAttr->baseName . '.' . $filemodel->$fileAttr->extension;
        }

        $parent_object_name = StringHelper::basename(get_class(new PartnerProfile()));
        $path = Yii::getAlias('@app/runtime/uploads/' . $parent_object_name . '/');
        $hash = $this->hash.'/'.$file_key.'/';

        if (!is_dir($path . $hash))
            mkdir($path . $hash, 0777, true);

        $uploaded_files = array_diff(scandir($path . $hash), ['.', '..']);
        $count_uploaded_files = count($uploaded_files) ? count($uploaded_files) : 0;

        foreach ($this->$file_key as $file)
        {
            $index = $count_uploaded_files + 1;
            $filename = $file_key . '_' . $index . '.' . $file->extension;
            $file->saveAs($path . $hash . '/' . $filename);
            $count_uploaded_files++;
        }

        return $hash;
    }

    public function save(){

    }
}