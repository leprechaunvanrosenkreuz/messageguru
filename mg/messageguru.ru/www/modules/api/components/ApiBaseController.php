<?php
/**
 * Created by PhpStorm.
 * User: manchetten
 * Date: 08.05.17
 * Time: 14:00
 */

namespace app\modules\api\components;

use yii\rest\ActiveController;
use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBasicAuth;
use app\filters\auth\HttpBearerAuth;
use yii\filters\auth\QueryParamAuth;
use app\modules\user\models\User;
use yii\filters\AccessControl;
use yii\filters\ContentNegotiator;
use yii\web\Response;
use yii\db\Expression;
use yii\filters\Cors;
use yii\filters\VerbFilter;


class ApiBaseController extends ActiveController
{
//    public function behaviors()
//    {
//        $behaviors = parent::behaviors();
//        $behaviors['corsFilter'] = [
//            'class' => Cors::className(),
//        ];
//        $behaviors['contentNegotiator'] = [
//            'class' => ContentNegotiator::className(),
//            'formats' => [
//                'application/json' => Response::FORMAT_JSON,
//            ],
//        ];
//        $behaviors['access'] = [
//            'class' => AccessControl::className(),
//            'only' => ['create', 'update', 'delete'],
//            'rules' => [
//                [
//                    'actions' => ['create', 'update', 'delete'],
//                    'allow' => false,
//                    'roles' => ['@'],
//                ],
//                [
//                    'actions' => ['index', 'view'],
//                    'allow' => true,
//                    'roles' => ['@'],
//                ],
//            ],
//        ];
//        $behaviors['authenticator'] = [
//            'class' => HttpBearerAuth::className()
//        ];
//        $behaviors['verbs'] = [
//            'class' => VerbFilter::className(),
//            'actions' => [
//                'index' => ['get'],
//                'view' => ['get'],
//                //'set-claim-status' => ['post']
//            ]
//        ];
//
//        return $behaviors;
//    }

    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $behaviors['authenticator'] = [
            'class' => CompositeAuth::className(),
            'authMethods' => [
                HttpBasicAuth::className(),
                HttpBearerAuth::className(),
                QueryParamAuth::className(),
            ],
        ];

        $behaviors['contentNegotiator'] = [
            'class' => 'yii\filters\ContentNegotiator',
            'formats' => [
                'text/html' => Response::FORMAT_JSON,
                'application/json' => Response::FORMAT_JSON,
                'application/xml' => Response::FORMAT_XML,
            ],
        ];

        $behaviors['corsFilter'] = [
            'class' => \yii\filters\Cors::className(),
            'cors' => [
                'Origin' => ['*'],
                'Access-Control-Request-Method' => ['GET', 'POST', 'PUT', 'PATCH', 'DELETE', 'HEAD', 'OPTIONS'],
                'Access-Control-Request-Headers' => '*',
                'Access-Control-Allow-Headers' => '*',
                'Access-Control-Max-Age' => 86400,
            ],
        ];

        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'rules' => [
                [
                    'allow' => true,
                    'roles' => ['@'],
                ],
            ],
        ];

        return $behaviors;
    }

}