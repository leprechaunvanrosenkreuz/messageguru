<?php

namespace app\modules\api\controllers;

use app\models\UserDevices;
use app\modules\api\components\ApiBaseController;
use Yii;
use yii\helpers\ArrayHelper;
use \yii\web\Response;
use app\modules\user\models\User;
use app\modules\user\models\forms\LoginForm;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\AccessControl;
use app\modules\user\models\forms\PasswordResetRequestForm;

/**
 * Class PresentationsController
 * @package app\modules\api\controllers
 */
class AuthController extends ApiBaseController
{
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        unset($behaviors['authenticator']);
        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'rules' => [
                [
                    'allow' => true,
                    'roles' => ['?'],
                ],
            ],
        ];

        return $behaviors;
    }


    public $modelClass = 'app\models\Files';

    /**
     * Переопределение дефолтных экшенов
     * @return array
     */
    public function actions()
    {
        $actions = parent::actions();
        unset($actions['index']);
        unset($actions['view']);
        return $actions;
    }

    /**
     * Выполняет логин юзера
     * @return bool|string
     */
    public function actionLogin()
    {
//        $email = Yii::$app->request->post('email', '');
//        $password = Yii::$app->request->post('password', '');

        $res = file_get_contents('php://input');
        //
        //$raw_data = Yii::$app->getRequest()->getBodyParams() ? key(Yii::$app->getRequest()->getBodyParams()) : '';
        $arr_data = json_decode($res, true);

        $email = ArrayHelper::getValue($arr_data, 'email');
        $password = ArrayHelper::getValue($arr_data, 'password');

        $model = new LoginForm();

        $model->email = $email;
        $model->password = $password;

        if ($model->login()) {
            $user_id = Yii::$app->getUser()->id;
            /** @var User $user */
            $user = User::findOne($user_id);

            if (!empty($user->access_token))
                return $user->access_token;
            else {
                $user->generateAccessToken();
                $user->update(false);

                return $user->access_token;
            }
        } else {
            //print_r($model);
            return $model;
        }
    }

    /**
     * Выполняет логаут юзера
     * @return bool|string
     */
    public function actionLogout()
    {
        $token = Yii::$app->request->get('access-token', '');
        /** @var User $user */
        $user = User::findIdentityByAccessToken($token);

        if (!empty($user) && $user->id == Yii::$app->user->id)
            return Yii::$app->user->logout();
        else
            return false;
    }

    /**
     * Регистрирует устройство пользователя и сохраняет его в БД
     * @return bool
     */
    public function actionRegisterDevice()
    {
        //$raw_data = Yii::$app->getRequest()->getBodyParams() ? key(Yii::$app->getRequest()->getBodyParams()) : '';
        //$arr_data = json_decode($raw_data, true);

        $res = file_get_contents('php://input');
        $arr_data = json_decode($res, true);

        $accessToken = Yii::$app->request->get('access-token', '');
        $deviceId = ArrayHelper::getValue($arr_data, 'device_id');

        /** @var User $user */
        $user = User::findOne(['access_token' => $accessToken]);

        if (!empty($user)) {
            $device = UserDevices::find()->where(['device_id' => $deviceId])->one();
            if (empty($device)) {
                $device = new UserDevices();
            }

            $device->user_id = $user->id;
            $device->device_id = $deviceId;
            $device->is_android = ArrayHelper::getValue($arr_data, 'is_android', '');
            $device->is_ios = ArrayHelper::getValue($arr_data, 'is_ios', '');
            $device->confirm_pushes = 1;

            $device->save(false);

            return true;
        } else {
            return false;
        }
    }

    /**
     * Регистрирует устройство пользователя и сохраняет его в БД
     *
     */
    public function actionGetUser()
    {
//        $res = file_get_contents('php://input');
//        $arr_data = json_decode($res, true);
//
//        $accessToken = ArrayHelper::getValue($arr_data, 'access-token');
        $accessToken = Yii::$app->request->get('access-token', '');

        /** @var User $user */
        $user = User::findOne(['access_token' => $accessToken]);

        if (!empty($user)) {
            return [
                'id' => $user->id,
                'avatar' => $user->getAvatarUrl(),
                'first_name' => $user->first_name,
                'last_name' => $user->last_name,
                'middle_name' => $user->middle_name,
                'desc' => $user->desc,
                'status' => $user->status,
                'pq' => $user->pq,
            ];
        }

    }

    public function actionPasswordReset()
    {
        $model = new PasswordResetRequestForm();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->getSession()->setFlash('success', 'Спасибо! На ваш Email было отправлено письмо со ссылкой на восстановление пароля.');

                return $this->goHome();
            } else {
                Yii::$app->getSession()->setFlash('error', 'Извините. У нас возникли проблемы с отправкой.');
            }
        }
    }
}
