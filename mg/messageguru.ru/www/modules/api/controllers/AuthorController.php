<?php

namespace app\modules\api\controllers;

use app\models\Files;
use app\models\Cities;
use app\modules\user\models\User;
use app\modules\api\components\ApiBaseController;
use Yii;
use app\models\Likes;
use \yii\web\Response;
use app\models\Achievements;
use yii\filters\auth\HttpBasicAuth;

/**
 * Class PresentationsController
 * @package app\modules\api\controllers
 */
class AuthorController extends ApiBaseController
{
    public $modelClass = 'app\modules\user\models\User';

    /**
     * Переопределение дефолтных экшенов
     * @return array
     */
    public function actions()
    {
        $actions = parent::actions();
        unset($actions['index']);
        unset($actions['view']);
        return $actions;
    }

    /**
     * Вытаскивает порцию презентаций (по 6 штук) + функционал одной большой (7 штук)
     * @param null $id
     * @return array
     */
    public function actionIndex()
    {
        Yii::$app->cache->flush();
        $offset = (int)Yii::$app->request->get('offset', 0);
        $filter = Yii::$app->request->get('filter');
        $id = Yii::$app->request->get('id');

        $cache_name = 'authors_all_' . $offset;

        $q = User::find()
            ->distinct()
            ->orderBy(['id' => SORT_DESC])
            ->offset($offset)
            ->limit(6);

        if (Yii::$app->cache->get($cache_name) === false) {
            $res = $q->all();
            $process_res = [];

            if (!empty($res)) {
                /** @var User $user */
                foreach ($res as $user) {
                    $tags = [];
                    if (!empty($user->tags))
                        foreach ($user->tags as $tag)
                            $tags[] = $tag->tag;

                    //TODO Сделать метод подготовки изображений
                    $process_res[] = [
                        'id' => $user->id,
                        'avatar' => $user->getAvatarUrl(),
                        'first_name' => $user->first_name,
                        'last_name' => $user->last_name,
                        'middle_name' => $user->middle_name,
                        'desc' => $user->desc,
                        'status' => $user->status,
                        'cnt' => Files::find()->where([
                            'user_id' => $user->id,
                            'status' => Files::STATUS_ON
                        ])->count(),
                        'pq' => $user->pq,
                        'achieve' => end($user->achievements)->name
                    ];
                }
            }
            Yii::$app->cache->set($cache_name, $process_res, 3600);
        } else $process_res = Yii::$app->cache->get($cache_name);

        return $process_res;
    }


    /**
     * Возвращает конкретную презу
     * @param $id
     * @return array|mixed
     */
    public function actionView($id)
    {
        Yii::$app->cache->flush();
        if (Yii::$app->cache->get('user_' . $id) === false) {
            /** @var User $user */
            $user = User::find()
                ->joinWith('files')
                ->joinWith('achievements')
                ->where(['user.id' => $id])
                ->one();

            $process_res = [];

            if (!empty($user)) {
                $presentations = [];

                if (!empty($user->files)) {
                    /** @var Files $file */
                    foreach ($user->files as $file) {
                        $tags = [];
                        if (!empty($file->tags))
                            foreach ($file->tags as $tag)
                                $tags[] = $tag->tag;

                        $likes = Likes::find()->where(['obj_id' => $file->id])->count();

                        if ($file->status == Files::STATUS_ON)
                            $presentations[] = [
                                'id' => $file->id,
                                'user_id' => $file->user_id,
                                'cat_id' => $file->cat_id,
                                'type' => $file->type,
                                'title' => $file->title,
                                'description' => $file->desc,
                                'status' => $file->status,
                                'views_count' => $file->views,
                                'tags' => $tags,
                                'likes' => $likes,
                                'image' => $file->getImageUrl(),
                                'images' => $file->getImages($file->id),
                                'created_at' => strtotime($file->created_at . ' -5 hours'),
                                'updated_at' => strtotime($file->updated_at . ' -5 hours'),
                                'fullname' => User::getFullname($file->user_id)
                            ];
                    }
                }

                //TODO Расширить в будущем для других типов наград
                $achievements = [];
                if (!empty($user->achievements)) {
                    //print_r($user->achievements);die();
                    //Берем последнее достижение по истории
                    /** @var \app\models\Achievements $achieve */
                    $achieve = end($user->achievements);

                    if (isset($achieve) && $achieve->type == Achievements::TYPE_LEVEL) {
                        $achievements = [
                            'alias' => $achieve->alias,
                            'name' => $achieve->name,
                            'image' => $achieve->image
                        ];
                    }
                }

                $process_res = [
                    'id' => $user->id,
                    'avatar' => $user->getAvatarUrl(),
                    'first_name' => $user->first_name,
                    'last_name' => $user->last_name,
                    'middle_name' => $user->middle_name,
                    'desc' => $user->desc,
                    'status' => $user->status,
                    'cnt' => count($presentations),
                    'pq' => $user->pq,
                    'level' => $achievements,
                    'presentations' => $presentations
                    //'achieve' => end($user->achievements)->desc
                ];
            }

            Yii::$app->cache->set('user_' . $id, $process_res, 3600);

            return $process_res;
        } else
            return Yii::$app->cache->get('user_' . $id);
    }


    public function actionAddlike()
    {
        $likes = Likes::addlike(Yii::$app->request->get());
        return $likes;
    }
}
