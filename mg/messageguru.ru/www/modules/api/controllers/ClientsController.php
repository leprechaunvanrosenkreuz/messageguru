<?php

namespace app\modules\api\controllers;

use yii;
use app\modules\admin\models\Categories;
use app\modules\admin\models\Clients;
use yii\rest\ActiveController;
use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\QueryParamAuth;
use app\modules\user\models\User;
use yii\filters\AccessControl;
use yii\filters\ContentNegotiator;
use yii\web\Response;
use yii\db\Expression;
use yii\helpers\Url;
use yii\web\ServerErrorHttpException;


/**
 * Default controller for the `api` module
 */
class ClientsController extends ActiveController
{
    public $modelClass = 'app\modules\admin\models\Clients';

    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $behaviors['authenticator'] = [
            'class' => CompositeAuth::className(),
            'authMethods' => [
                HttpBasicAuth::className(),
                HttpBearerAuth::className(),
                QueryParamAuth::className(),
            ],
        ];

        $behaviors['contentNegotiator'] = [
            'class' => 'yii\filters\ContentNegotiator',
            'formats' => [
                'text/html' => Response::FORMAT_JSON,
                'application/json' => Response::FORMAT_JSON,
                'application/xml' => Response::FORMAT_XML,
            ],
        ];

        $behaviors['corsFilter'] = [
            'class' => \yii\filters\Cors::className(),
            'cors' => [
                'Origin' => ['*', 'http://localhost:8100'],
                'Access-Control-Request-Method' => ['GET', 'POST', 'PUT', 'PATCH', 'DELETE', 'HEAD', 'OPTIONS'],
                'Access-Control-Request-Headers' => '*',
                'Access-Control-Allow-Headers' => '*',
                'Access-Control-Allow-Origin' => ['*'],
                'Access-Control-Max-Age' => 86400,
            ],
        ];

        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'rules' => [
                [
                    'allow' => true,
                    'roles' => ['@'],
                    //'verbs' => ['GET', 'PUT', 'POST']
                ],
                [
                    'allow' => true,
                    'roles' => ['@'],
                    'actions' => ['create'],
                    //'verbs' => ['GET', 'PUT', 'POST']
                ],
            ],
        ];

        $behaviors['verbs'] = [
            'class' => \yii\filters\VerbFilter::className(),
            'actions' => [
                'index' => [''],
                'view' => ['get'],
                'create' => ['get', 'post'],
                'update' => ['get', 'put', 'post'],
                'delete' => ['post', 'delete'],
                'register' => ['get', 'post']
            ],
        ];

        return $behaviors;
    }

    public function actionRegister()
    {
        header("Access-Control-Allow-Origin: *");

        $raw_data =Yii::$app->getRequest()->getBodyParams() ? key(Yii::$app->getRequest()->getBodyParams()): '';

        Yii::info('=== Get new register request ===', 'clients');
        Yii::info($raw_data, 'clients');

        $arr_data = json_decode($raw_data, true);

        $client = Clients::find()->where(['device_id' => $arr_data['device_id']])->one();
        if (empty($model)) {
            $client = new Clients();
        }

        $client->load($arr_data, '');
        $client->confirm_pushes = 1;

        if ($client->save()) {
            $response = Yii::$app->getResponse();
            $response->setStatusCode(201);
            $id = implode(',', array_values($client->getPrimaryKey(true)));
            //$response->getHeaders()->set('Location', Url::toRoute([$this->viewAction, 'id' => $id], true));
        } elseif (!$client->hasErrors()) {
            throw new ServerErrorHttpException('Failed to create the object for unknown reason.');
        }

        Yii::info(print_r($client, true), 'clients');
        Yii::info('=== End register ===', 'clients');

        return $client;
        //print_r($client); die();

    }
}