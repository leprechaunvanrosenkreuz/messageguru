<?php

namespace app\modules\api\controllers;

use app\models\CategoriesDict;
use app\modules\api\components\ApiBaseController;
use Yii;
use app\models\Files;
use yii\helpers\ArrayHelper;
use app\models\Likes;
use \yii\web\Response;
use app\modules\user\models\User;
use yii\filters\AccessControl;
use yii\helpers\Inflector;

/**
 * Class PresentationsController
 * @package app\modules\api\controllers
 */
class PresentationsController extends ApiBaseController
{
    public $modelClass = 'app\models\Files';

    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'rules' => [
                [
                    'allow' => true,
                    'roles' => ['@'],
                ],
                [
                    'allow' => true,
                    'roles' => ['@'],
                    'actions' => ['search'],
                    'verbs' => ['GET']
                ],
            ],
        ];

        return $behaviors;
    }

    /**
     * Переопределение дефолтных экшенов
     * @return array
     */
    public function actions()
    {
        $actions = parent::actions();
        unset($actions['index']);
        unset($actions['view']);
        return $actions;
    }

    /**
     * Вытаскивает порцию презентаций (по 6 штук) + функционал одной большой (7 штук)
     * @param null $cat_id
     * @return array
     */
    public function actionIndex($cat_id = null)
    {
        Yii::$app->cache->flush();
        if (!empty($cat_id)) {
            $offset = (int)Yii::$app->request->get('offset', 0);

            $is_first = Yii::$app->request->get('has_first', false);

            $cache_name = $is_first ? 'first_pres_' . $cat_id . '_' . $offset : 'cat_pres_' . $cat_id . '_' . $offset;

            $q = Files::find()
                ->distinct()
                ->joinWith('filesTags')
                //->where(['files.status' => Files::TYPE_PRESENTATION]) TODO Добавить статус
                ->andWhere(['files.cat_id' => $cat_id])
                ->orderBy(['created_at' => SORT_DESC])
                ->offset($offset)
                ->limit(6);

            if (Yii::$app->cache->get($cache_name) === false) {

                $res = $q->all();
                $process_res = [];

                //print_r(count($res));
                if (!empty($res)) {

                    /** @var Files $pres */
                    foreach ($res as $pres) {
                        $tags = [];
                        if (!empty($pres->tags))
                            foreach ($pres->tags as $tag)
                                $tags[] = $tag->tag;


                        $likes = Likes::find()->where(["obj_id" => $pres->id])->count();
                        //TODO Сделать метод подготовки изображений
                        $process_res[] = [
                            'id' => $pres->id,
                            'user_id' => $pres->user_id,
                            'cat_id' => $pres->cat_id,
                            'type' => $pres->type,
                            'title' => $pres->title,
                            'description' => $pres->desc,
                            'status' => $pres->status,
                            'views_count' => $pres->views,
                            'tags' => $tags,
                            'likes' => $likes,
                            'image' => $pres->getImageUrl(),
                            'created_at' => strtotime($pres->created_at . ' -5 hours'),
                            'updated_at' => strtotime($pres->updated_at . ' -5 hours'),
                            'fullname' => User::getFullname($pres->user_id)

                        ];
                    }
                }
                Yii::$app->cache->set($cache_name, $process_res, 3600);
            } else
                $process_res = Yii::$app->cache->get($cache_name);
        } else {
            $offset = (int)Yii::$app->request->get('offset', 0);

            $is_first = Yii::$app->request->get('has_first', false);

            $cache_name = $is_first ? 'first_pres_all_' . $offset : 'cat_pres_all_' . $offset;

            $q = Files::find()
                ->distinct()
                ->joinWith('filesTags')
                ->where(['files.status' => Files::STATUS_ON])
                ->orderBy(['created_at' => SORT_DESC])
                ->offset($offset)
                ->limit(6);

            if (Yii::$app->cache->get($cache_name) === false) {
                $res = $q->all();
                $process_res = [];

                if (!empty($res)) {
                    /** @var Files $pres */
                    foreach ($res as $pres) {
                        $tags = [];
                        if (!empty($pres->tags))
                            foreach ($pres->tags as $tag)
                                $tags[] = $tag->tag;

                        $likes = Likes::find()->where(["obj_id" => $pres->id])->count();
                        //TODO Сделать метод подготовки изображений
                        $process_res[] = [
                            'id' => $pres->id,
                            'user_id' => $pres->user_id,
                            'cat_id' => $pres->cat_id,
                            'type' => $pres->type,
                            'title' => $pres->title,
                            'file_name' => $pres->name,
                            'description' => $pres->desc,
                            'image' => $pres->getImageUrl(),
                            'status' => $pres->status,
                            'views_count' => $pres->views,
                            'tags' => $tags,
                            'likes' => $likes,
                            'created_at' => strtotime($pres->created_at . ' -5 hours'),
                            'updated_at' => strtotime($pres->updated_at . ' -5 hours'),
                            'fullname' => User::getFullname($pres->user_id)
                        ];
                    }
                }
                Yii::$app->cache->set($cache_name, $process_res, 3600);
            } else $process_res = Yii::$app->cache->get($cache_name);
        }

        return $process_res;
    }


    /**
     * Возвращает конкретную презу
     * @param $id
     * @return array|mixed
     */
    public function actionView($id)
    {
        Yii::$app->cache->flush();
        if (Yii::$app->cache->get('post_' . $id) === false) {
            /** @var Files $pres */
            $pres = Files::find()
                ->joinWith('filesTags')
                //TODO Добавить статус
                ->where([
                    'files.id' => $id,
                    'files.status' => Files::STATUS_ON
                ])
                ->one();

            $process_res = [];

            if (!empty($pres)) {
                $tags = [];
                if (!empty($pres->tags))
                    foreach ($pres->tags as $tag)
                        $tags[] = $tag->tag;

                $likes = Likes::find()->where(["obj_id" => $pres->id])->count();
                //TODO Сделать метод подготовки изображений
                $process_res = [
                    'id' => $pres->id,
                    'user_id' => $pres->user_id,
                    'cat_id' => $pres->cat_id,
                    'type' => $pres->type,
                    'title' => $pres->title,
                    'file_name' => $pres->name,
                    'description' => $pres->desc,
                    'status' => $pres->status,
                    'views_count' => $pres->views,
                    'tags' => $tags,
                    'likes' => $likes,
                    'images' => $pres->getImages($pres->id),
                    'created_at' => strtotime($pres->created_at . ' -5 hours'),
                    'updated_at' => strtotime($pres->updated_at . ' -5 hours'),
                    'fullname' => User::getFullname($pres->user_id)
                ];
            }

            Yii::$app->cache->set('pres_' . $id, $process_res, 3600);

            return $process_res;
        } else
            return Yii::$app->cache->get('pres_' . $id);
    }

    public function actionAddlike()
    {
        $responce = Yii::$app->request->post();

        if (empty($responce["obj_id"])) {
            return ["error" => "empty params obj_id"];
        }

        if (!empty(Yii::$app->getUser()->id)) {
            $model = Likes::find()->where([
                "obj_id" => $responce["obj_id"],
                "user_by" => Yii::$app->getUser()->id
            ])->one();

            if (!empty($model->id)) {
                //уже был поставлен лайк - дизлайкаем
                $model->delete();
                $likes = Likes::find()->where(["obj_id" => $model->obj_id])->all();
                return ["success" => count($likes)];
            } else {
                $model = new Likes();
                $model->obj_id = $responce["obj_id"];
                $model->count = 1;
                $model->user_by = Yii::$app->getUser()->id;
                if ($model->save()) {
                    $likes = Likes::find()->where(["obj_id" => $model->obj_id])->all();
                    return ["success" => count($likes)];
                }
            }
        } else {
            return ["error" => "only register user"];
        }
    }

    /**
     * Добавление презентации
     * @return array
     */
    public function actionAddPresentation()
    {
        $res = file_get_contents('php://input');
        $arr_data = json_decode($res, true);

        $video_name = ArrayHelper::getValue($arr_data, 'video');

        $token = Yii::$app->request->get('access-token', '');
        /** @var User $user */
        $user = User::findIdentityByAccessToken($token);

        if (!empty($user)) {

            $model = new Files();

            preg_match("/^(?:http(?:s)?:\/\/)?(?:www\.)?(?:m\.)?(?:youtu\.be\/|youtube\.com\/(?:(?:watch)?\?(?:.*&)?v(?:i)?=|(?:embed|v|vi|user)\/))([^\?&\"'>]+)/", $video_name, $matches);

            if (!empty($matches[1])) {
                $model->name = $matches[1];
            }

            $model->title = ArrayHelper::getValue($arr_data, 'title');
            $model->user_id = $user->id;
            $model->cat_id = ArrayHelper::getValue($arr_data, 'cat_id');
            $model->slug = Inflector::slug($model->title);
            $model->status = 1;
            $model->type = Files::TYPE_YOUTUBE;
            $model->desc = ArrayHelper::getValue($arr_data, 'desc');

            if ($model->validate()) {
                if ($model->save()) {
                    return ['status' => 'success', 'id' => $model->id];

                } else {
                    return ['status' => 'error'];
                }
            } else {
                return ['status' => 'error'];
            }
        }
    }

    /**
     * Возвращает список категорий
     * @return array
     */
    public function actionGetCategories()
    {
        return CategoriesDict::getCatsList();
    }

    public function actionSearch($q)
    {
        $search_result = Files::find()
            ->where(['like', 'title', $q . '%', false])
            ->andWhere(['status' => Files::STATUS_ON])
            ->limit(20)
            //->asArray()
            ->all();

        $result = [];

        /** @var Files $pres */
        foreach ($search_result as $pres) {
            $tags = [];
            if (!empty($pres->tags))
                foreach ($pres->tags as $tag)
                    $tags[] = $tag->tag;


            $likes = Likes::find()->where(["obj_id" => $pres->id])->count();

            $result[] = [
                'id' => $pres->id,
                'user_id' => $pres->user_id,
                'cat_id' => $pres->cat_id,
                'type' => $pres->type,
                'title' => $pres->title,
                'description' => $pres->desc,
                'status' => $pres->status,
                'views_count' => $pres->views,
                'tags' => $tags,
                'likes' => $likes,
                'image' => $pres->getImageUrl(),
                'created_at' => strtotime($pres->created_at . ' -5 hours'),
                'updated_at' => strtotime($pres->updated_at . ' -5 hours'),
                'fullname' => User::getFullname($pres->user_id)
            ];
        }

        return $result;
    }
}
