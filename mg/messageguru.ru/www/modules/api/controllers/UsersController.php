<?php

namespace app\modules\api\controllers;

use yii\data\ArrayDataProvider;

use yii\helpers\Url;
use Yii;
use yii\rest\ActiveController;
use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\QueryParamAuth;
use \yii\web\Response;

/**
 * Default controller for the `api` module
 */
class UsersController extends ActiveController
{
    public $modelClass = 'app\modules\admin\models\Posts';

    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $behaviors['authenticator'] = [
            'class' => CompositeAuth::className(),
            'authMethods' => [
                HttpBasicAuth::className(),
                HttpBearerAuth::className(),
                QueryParamAuth::className(),
            ],
        ];

        $behaviors['contentNegotiator'] = [
            'class' => 'yii\filters\ContentNegotiator',
            'formats' => [
                'text/html' => Response::FORMAT_JSON,
                'application/json' => Response::FORMAT_JSON,
                'application/xml' => Response::FORMAT_XML,
            ],
        ];

        $behaviors['corsFilter'] = [
            'class' => \yii\filters\Cors::className(),
            'cors' => [
                'Origin' => ['*'],
                'Access-Control-Request-Method' => ['GET'],
                'Access-Control-Request-Headers' => '*',
                'Access-Control-Allow-Headers' => '*',
                'Access-Control-Max-Age' => 86400,
            ],
        ];

        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'rules' => [
                [
                    'allow' => true,
                    'roles' => ['@'],
                ],
            ],
        ];

        return $behaviors;
    }

    public function actions()
    {
        $actions = parent::actions();
        unset($actions['index']);
        unset($actions['view']);
        return $actions;
    }

    public function actionIndex($cat_id = null)
    {
        //Yii::$app->cache->flush();
        if (!empty($cat_id)) {
            $offset = (int)Yii::$app->request->get('offset', 0);

            $is_first = Yii::$app->request->get('has_first', false);

            $cache_name = $is_first ? 'first_posts_' . $cat_id . '_' . $offset : 'cat_post_' . $cat_id . '_' . $offset;

            $q = Posts::find()
                ->distinct()
                ->joinWith('tags')
                ->joinWith('categories')
                ->where(['posts.status' => Posts::STATUS_PUBLISH])
                ->andWhere(['categories.id' => $cat_id])
                ->orderBy(['created_at' => SORT_DESC, 'order' => SORT_ASC])
                ->offset($offset);

            $is_first ? $q->limit(7) : $q->limit(6);


            if (Yii::$app->cache->get($cache_name) === false) {

                $res = $q->all();
                $process_res = [];

                //print_r(count($res));
                if (!empty($res))
                    foreach ($res as $post) {
                        $tags = [];
                        if (!empty($post->tags))
                            foreach ($post->tags as $tag)
                                $tags[] = $tag->tag;

                        $p_cat_names = [];
                        $p_cat_ids = [];

                        //print_r($post->categories); die();

                        if (!empty($post->categories))
                            foreach ($post->categories as $p_category) {
                                $p_cat_names[] = $p_category['category_name_hebrew'];
                                $p_cat_ids[] = $p_category['id'];
                            }

                        $process_res[] = [
                            'id' => $post->id,
                            'category_id' => $p_cat_ids,
                            'category_name' => $p_cat_names,
                            'header' => $post->header,
                            'description' => $post->description,
                            'youtube_link' => $post->youtube_link,
                            //'youtube_video_link' => $this->prepareVideoLink($post->youtube_link),
                            'status' => $post->status,
                            'post_image' => $this->getImageLink($post->post_image, true),
                            'thumbnails' => $post->thumbnails,
                            'tags' => $tags,
                            'created_at' => strtotime($post->created_at . ' -5 hours'),
                            'updated_at' => strtotime($post->updated_at . ' -5 hours')
                        ];
                    }
                Yii::$app->cache->set($cache_name, $process_res, 3600);
            } else
                $process_res = Yii::$app->cache->get($cache_name);
        } else {
            $offset = (int)Yii::$app->request->get('offset', 0);

            $is_first = Yii::$app->request->get('has_first', false);

            $cache_name = 'cat_post_all_' . $offset;

            $cache_name = $is_first ? 'first_posts_all_' . $offset : 'cat_post_all_' . $offset;

            $q = Posts::find()
                ->joinWith('categories')
                ->joinWith('tags')
                ->where(['posts.status' => Posts::STATUS_PUBLISH])
                ->orderBy(['created_at' => SORT_DESC, 'order' => SORT_ASC])
                ->offset($offset);

            $is_first ? $q->limit(7) : $q->limit(6);

            if (Yii::$app->cache->get($cache_name) === false) {
                $res = $q->all();
                $process_res = [];

                if (!empty($res))
                    foreach ($res as $post) {
                        $tags = [];
                        if (!empty($post->tags))
                            foreach ($post->tags as $tag)
                                $tags[] = $tag->tag;

                        $p_cat_names = [];
                        $p_cat_ids = [];

                        if (!empty($post->categories))
                            foreach ($post->categories as $p_category) {
                                $p_cat_names[] = $p_category['category_name_hebrew'];
                                $p_cat_ids[] = $p_category['id'];
                            }

                        $process_res[] = [
                            'id' => $post->id,
                            'category_id' => $p_cat_ids,
                            'category_name' => $p_cat_names,
                            'header' => $post->header,
                            'description' => $post->description,
                            'youtube_link' => $post->youtube_link,
                            //'youtube_video_link' => $this->prepareVideoLink($post->youtube_link),
                            'status' => $post->status,
                            'post_image' => $this->getImageLink($post->post_image, true),
                            'thumbnails' => $post->thumbnails,
                            'tags' => $tags,
                            'created_at' => strtotime($post->created_at . ' -5 hours'),
                            'updated_at' => strtotime($post->updated_at . ' -5 hours')
                        ];
                    }
                Yii::$app->cache->set($cache_name, $process_res, 3600);
            } else $process_res = Yii::$app->cache->get($cache_name);
        }

        return $process_res;
    }

    public function actionBytag($tag_id = null)
    {
        $process_res = [];

        if (!empty($tag_id)) {
            $q = Posts::find()
                ->joinWith('categories')
                ->joinWith('tags')
                ->where(['posts.status' => Posts::STATUS_PUBLISH, 'tags.id' => $tag_id]);

            $res = $q->all();
            $process_res = [];

            if (!empty($res))
                foreach ($res as $post) {
                    $tags = [];
                    if (!empty($post->tags))
                        foreach ($post->tags as $tag)
                            $tags[] = $tag->tag;

                    $p_cat_names = [];
                    $p_cat_ids = [];

                    if (!empty($post->categories))
                        foreach ($post->categories as $p_category) {
                            $p_cat_names[] = $p_category['category_name_hebrew'];
                            $p_cat_ids[] = $p_category['id'];
                        }

                    $process_res[] = [
                        'id' => $post->id,
                        'category_id' => $p_cat_ids,
                        'category_name' => $p_cat_names,
                        'header' => $post->header,
                        'description' => $post->description,
                        'youtube_link' => $post->youtube_link,
                        //'youtube_video_link' => $this->prepareVideoLink($post->youtube_link),
                        'status' => $post->status,
                        'post_image' => $this->getImageLink($post->post_image, true),
                        'thumbnails' => $post->thumbnails,
                        'tags' => $tags,
                        'created_at' => strtotime($post->created_at . ' -5 hours'),
                        'updated_at' => strtotime($post->updated_at . ' -5 hours')
                    ];
                }
        }
        return new ArrayDataProvider([
            'allModels' => $process_res,
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);
    }

    public function actionView($id)
    {
        Yii::$app->cache->flush();
        if (Yii::$app->cache->get('post_' . $id) === false) {
            $post = Posts::find()
                ->joinWith('categories')
                ->joinWith('tags')
                ->where(['posts.status' => Posts::STATUS_PUBLISH, 'posts.id' => $id])
                ->one();

            $shorter = new BranchIOApi();
            $eya = new Engageya();

            $process_res = [];

            if (!empty($post)) {
                $tags = [];
                if (!empty($post->tags))
                    foreach ($post->tags as $tag)
                        $tags[] = $tag->tag;

                $p_cat_names = [];
                $p_cat_ids = [];

                if (!empty($post->categories))
                    foreach ($post->categories as $p_category) {
                        $p_cat_names[] = $p_category['category_name_hebrew'];
                        $p_cat_ids[] = $p_category['id'];
                    }

                //print_r($this->replaceShortcodes($post->description)); die();

                $thumb = $this->getImageLink($post->post_image);

                //print_r($shorter->shortUrl('post/' . 321, $thumb));die();

                $process_res = [
                    'id' => $post->id,
                    'category_id' => $p_cat_ids,
                    'category_name' => $p_cat_names,
                    'header' => $post->header,
                    'description' => $this->replaceShortcodes($post->description),
                    'youtube_link' => $post->youtube_link,
                    'deep_link' => 'totaapp://post/' . $post->id,
                    'short_deep_link' => $shorter->shortUrl('post/' . $post->id, $thumb, $post->header),
                    //'youtube_video_link' => $this->prepareVideoLink($post->youtube_link),
                    'status' => $post->status,
                    'post_image' => $this->getImageLink($post->post_image),
                    'thumbnails' => $post->thumbnails,
                    'tags' => $tags,
                    'recommendations' => $eya->prepareRecs($post->id),
                    'created_at' => strtotime($post->created_at . ' -5 hours'),
                    //'created_at_2' => date('Y-m-d H:i:s', strtotime($post->created_at . ' +2 hours')),
                    'updated_at' => strtotime($post->updated_at . ' -5 hours')
                ];
            }

            Yii::$app->cache->set('post_' . $id, $process_res, 3600);

            return $process_res;
        } else
            return Yii::$app->cache->get('post_' . $id);
    }

    /**
     * Экшен получения видео
     * @param $id
     * @return array|mixed|string
     */
    public function actionVideo($id)
    {
        if (Yii::$app->cache->get('video_' . $id) === false) {
            $videos = [];

            $post = Posts::find()->where(['status' => Posts::STATUS_PUBLISH, 'posts.id' => $id])->one();
            if (!empty($post)) {
                $videos = $this->prepareVideoLink($post->youtube_link);

                Yii::$app->cache->set('video_' . $id, $videos, 3600);
            }
            return $videos;

        } else
            return Yii::$app->cache->get('video_' . $id);
    }

    /**
     * Подготовливает поток видео
     * @param $id
     * @return array|mixed|string
     */
    private function prepareVideoLink($id)
    {
        if (!empty($id)) {
            $gapi = new GoogleApi();

            //return $gapi->getVideoStream($id);

            if (Yii::$app->cache->get('video_' . $id) === false) {
                $res = $gapi->getVideoStream($id);
                Yii::$app->cache->set('video_' . $id, $res, 360);

                return $res;
            } else
                return Yii::$app->cache->get('video_' . $id);

        } else return '';
    }

    /**
     * Преобразовывает шорткоды
     * @param $desc
     * @return mixed
     */
    private function replaceShortcodes($desc)
    {
        $matches = [];
        $regex = '#\[\[(.+?)\]\]#is';
        //$regex = ' ~\{([^}]*)\}~';
        preg_match_all($regex, $desc, $matches);

        //print_r($matches);die();
        $result_text = $desc;

        if (!empty($matches[1]))
            foreach ($matches[1] as $match) {
                $img = Images::find()->where(['shortcode' => $match, 'status' => Images::STATUS_UPLOAD])->one();

                //$img_src = (!empty($img)) ? Yii::$app->request->hostInfo . Url::base() . '/uploads/library/' . $img->path . '/' . $img->name : '';
                $img_src = (!empty($img)) ? $this->getThumb($img) : '';


                //$th = EasyThumbnailImage::thumbnailFileUrl(Yii::getAlias('@app/web/uploads/library/') . $img->path . '/' . $img->name, 350, 180, EasyThumbnailImage::THUMBNAIL_OUTBOUND);


                $template = '<p><img src="' . $img_src . '" width="100%" /></p>';

                $result_text = str_replace('[[' . $match . ']]', $template, $result_text);
                //print_r($result_text);die();
            }
        //print_r($this->replaceVideoShortcodes($result_text));die();
        return $this->replaceVideoShortcodes($result_text);
    }

    /**
     * Парсинг видео внутри контента (срабатывает коллбек app\modules\admin\components\widgets\YoutubeShortcode)
     * @param $desc
     * @return mixed
     */
    private function replaceVideoShortcodes($desc)
    {
        $res = \Yii::$app->shortcodes->parse($desc);
        //print_r($res);die();
        return $res;
    }

    private function getImageLink($image, $is_list = false)
    {
        if (!empty($image)) {
            if ($is_list)
                return Yii::$app->request->hostInfo . EasyThumbnailImage::thumbnailFileUrl(Yii::getAlias('@app/web/uploads/previews/') . $image, 372, 278, EasyThumbnailImage::THUMBNAIL_INSET);
            else
                return Yii::$app->request->hostInfo . Url::base() . '/uploads/previews/' . $image;
        } else return '';
    }

    private function getThumb($img)
    {
        $img_real = Image::getImagine()->open(Yii::getAlias('@app/web/uploads/library/') . $img->path . '/' . $img->name);

        $size = $img_real->getSize();
        $ratio = $size->getWidth() / $size->getHeight();

        //print_r($img); die();

        $width = 500;
        $height = round($width / $ratio);

        $fileName = 'th_' . $img->name;

        $savePath = Yii::getAlias('@app/web/uploads/library') . '/thumbs/' . $fileName;

        $box = new Box($width, $height);
        $img_real->resize($box)->save($savePath);

        return Yii::$app->request->hostInfo . Url::base() . '/uploads/library/thumbs/' . $fileName;
    }
}
