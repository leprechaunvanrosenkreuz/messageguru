<?php

namespace app\modules\user;

use yii;

/**
 * User module
 */
class Module extends \yii\base\Module
{
    public function init()
    {
        Yii::setAlias('@user', dirname(__FILE__));
        parent::init();
        // custom initialization code goes here
        \Yii::$app->view->theme = new \yii\base\Theme([
            //'pathMap' => ['@app/views' => '@vendor/dmstr/yii2-adminlte-asset/example-views/yiisoft/yii2-app'],
            'pathMap' => [
                '@app/views' => '@app/views/admin_lte',
            ]

            //'baseUrl' => '@web/themes/admin',
        ]);

    }
}
