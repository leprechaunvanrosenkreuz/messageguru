<?php

namespace app\modules\user\components;


use yii;
use yii\web\Controller;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;

/**
 * UserController
 */
class UserController extends Controller
{
    public $layoutPath = '@app/modules/user/views/layouts';
    public $layout = 'main';

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'login', 'logout'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'login', 'reset-password-request', 'test', 'create'],
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['index', 'logout', 'update', 'create', 'delete'],
                        'roles' => ['@'],
                    ],
                ],
                'denyCallback' => function ($rule, $action) {
                    if(Yii::$app->user->isGuest)
                        $this->redirect(['/user/do/login']);
                    else
                        $this->redirect(['/user/profile/index']);
                }
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }


    /**
     * actionIndex
     */
    public function actionIndex()
    {

    }
}