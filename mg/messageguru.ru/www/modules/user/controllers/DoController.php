<?php


namespace app\modules\user\controllers;

use app\modules\user\models\User;
use yii;
use yii\bootstrap\ActiveForm;

use yii\web\Controller;
use yii\web\Response;
use app\modules\user\models\forms\LoginForm;
use app\modules\user\models\forms\RegistrationForm;
use app\modules\user\models\forms\EmailConfirmForm;
use app\models\Subscribers;

use app\modules\user\models\forms\PasswordResetForm;
use app\modules\user\models\forms\PasswordResetRequestForm;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;
use yii\web\HttpException;
use app\modules\user\components\UserController;
use yii\web\UploadedFile;
use app\modules\user\models\UploadForm;
use app\components\VKApi;
use kartik\social\Module;
use app\models\UserAchievements;
use app\models\Achievements;
use Facebook\Facebook;
use Facebook\FacebookRequest;
use yii\helpers\ArrayHelper;


class DoController extends UserController
{
    public $layoutPath = '@app/modules/user/views/layouts';
    public $adminLayoutPath = '@app/modules/admin/views/layouts';

    public function behaviors()
    {
        return [
            'eauth' => [
                // required to disable csrf validation on OpenID requests
                'class' => \nodge\eauth\openid\ControllerBehavior::className(),
                'only' => ['login'],
            ],
        ];
    }

    /**
     * Логин
     * @return string|Response
     */
    public function actionLogin()
    {
        //$serviceName = $service;
        $serviceName = Yii::$app->getRequest()->getQueryParam('service');

        if (isset($serviceName)) {
            /** @var $eauth \nodge\eauth\ServiceBase */
            $eauth = Yii::$app->get('eauth')->getIdentity($serviceName);
            //echo Yii::$app->getUser()->getReturnUrl();
            $eauth->setRedirectUrl(Yii::$app->getUser()->getReturnUrl());
            $eauth->setCancelUrl(Yii::$app->getUrlManager()->createAbsoluteUrl('user/do/login'));

            try {
                if ($eauth->authenticate()) {
                    $identity = User::findByEAuth($eauth);
                    Yii::$app->getUser()->login($identity);

//                    print_r($identity);die();

                    if (!empty(Yii::$app->getUser()->id) && !is_numeric(Yii::$app->getUser()->id)) {
                        $user_id = Yii::$app->getUser()->id;
                        $soc_id = (int)preg_replace('/[^0-9]/', '', $user_id);

                        $existUserId = User::find()
                            ->where(['id' => Yii::$app->getUser()->id])
                            ->orWhere(['social_id' => Yii::$app->getUser()->id])
                            ->one()->id;

                        if (empty($existUserId)) {
                            $name = explode(" ", Yii::$app->user->identity->profile['name']);
                            $user = new User();
                            $user->type = User::TYPE_SPEAKER;
                            $user->status = User::STATUS_ACTIVE;
                            $user->social_id = $user_id;
                            $user->first_name = $name[0];
                            $user->last_name = $name[1];
                            $user->email = $user_id . "@guru.ru";
                            $user->pq = 0;

                            if ($serviceName == 'facebook')
                                $user->avatar = $user->uploadFbAvatar();

                            if ($serviceName == 'vkontakte')
                                $user->avatar = $user->uploadVkAvatar($soc_id);

                            $insert = $user->save();

                            $existUserId = $user->id;

                            $ach = new UserAchievements();
                            $ach->user_id = $user->id;
                            $ach->ach_id = Achievements::getIdByAlias('stranger');
                            $ach->save();

                        }

                        //добавляем ему друзей из вк
                        if ($serviceName == 'vkontakte')
                            $this->addVkFriends($soc_id, $existUserId);

                        //добавляем ему друзей из фб
                        if ($serviceName == 'facebook')
                            $this->addFbFriends($soc_id, $existUserId);

                        // special redirect with closing popup window
                        $eauth->redirect(["profile/index"]);
                    }
                } else {
                    // close popup window and redirect to cancelUrl
                    $eauth->cancel();
                }
            } catch (\nodge\eauth\ErrorException $e) {
                // save error to show it later
                Yii::$app->getSession()->setFlash('error', 'EAuthException: ' . $e->getMessage());

                // close popup window and redirect to cancelUrl
//              $eauth->cancel();
                $eauth->redirect($eauth->getCancelUrl());
            }
        }


        /** @var LoginForm $model */
        $model = new LoginForm();
        //$this->performAjaxValidation($model);

        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->redirect(['profile/index']);
        } else {
            return $this->render('@app/modules/user/views/login', [
                'model' => $model,
                'layoutPath' => $this->layoutPath,
                'out' => ''
            ]);
        }
    }

    public function addVkFriends($soc_id, $user_id)
    {
        $vk = new VKApi();

        if (!empty($vk->getFriends($soc_id)['response'])) {

            foreach ($vk->getFriends($soc_id)['response'] as $friends_id) {

                $vk_user = 'vkontakte-' . $friends_id;
                $friends = User::find()->where(["social_id" => $vk_user])->one();

                if (!empty($friends)) {
                    //echo $friends->id.'test'.$user_id;exit;
                    if($existSubscriber = Subscribers::find()->where(['user_id' => $friends->id, 'subscriber_id' => $user_id])->one())
                        continue;

                    $subscribers = new Subscribers();
                    $subscribers->user_id = $friends->id;
                    $subscribers->subscriber_id = $user_id;
                    $subscribers->insert();
                }
                // new Subscribers
            }
            //exit;
        }
    }

    /**
     * @param $soc_id
     * @param $user_id
     * @return bool
     */
    public function addFbFriends($soc_id, $user_id)
    {
        /** @var $eauth \nodge\eauth\ServiceBase */
        $eauth = Yii::$app->get('eauth')->getIdentity('facebook');

        if ($eauth->getIsAuthenticated()) {
            $rawUsers = ArrayHelper::getValue($eauth->getFriendsList($soc_id), 'data', []);

            if (!empty($rawUsers))
                foreach ($rawUsers as $rawUser) {
                    $id = ArrayHelper::getValue($rawUser, 'id');

                    $fb_user = 'facebook-' . $id;
                    $friends = User::find()->where(['social_id' => $fb_user])->one();

                    if (!empty($friends)) {
                        if($existSubscriber = Subscribers::find()->where(['user_id' => $friends->id, 'subscriber_id' => $user_id])->one())
                            continue;

                        $subscribers = new Subscribers();
                        $subscribers->user_id = $friends->id;
                        $subscribers->subscriber_id = $user_id;
                        $subscribers->insert();
                    }
                }
        }

        return true;
    }

    protected function performAjaxValidation($model)
    {
        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }
    }

    /**
     * Экшен выхода
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Экшен регистрации пользователя
     * @return string|Response
     */
    public function actionRegister()
    {
        //Регистрация
        $model_reg = new RegistrationForm();

        if ($model_reg->load(Yii::$app->request->post())) {
            //Решить, че у нас будет уникальным (пока email)
            $other_user = User::find()->where(['email' => $model_reg->email])->one();

            if ($user = $model_reg->registration($other_user->email)) {
                Yii::$app->getSession()->setFlash('success_reg', 'Успешная регистрация');

                if (!empty($user->email_confirm_token)) {
                    $img_path = Yii::getAlias('@app/modules/user/assets/img/mail/');

                    Yii::$app->mailer->compose('@app/modules/user/views/mail/confirmation', ['user' => $user,
                        'bg' => $img_path . 'bg.png',
                        'body' => $img_path . 'body.png',
                        'footer' => $img_path . 'footer.png',
                        'head' => $img_path . 'head.png',
                    ])
                        ->setFrom(Yii::$app->params['mailFrom'])
                        ->setTo($user->email)
                        ->setSubject('Подтверждение регистрации. ' . Yii::$app->name)
                        ->send();

                    return $this->render('@app/modules/user/views/confirm', [
                        'model_reg' => $model_reg,
                    ]);
                }

            } else {
                Yii::$app->getSession()->setFlash('success_reg', 'Ошибка регистрации');
                return $this->redirect(['/user/do/register']);
            }
        }

        return $this->render('@app/modules/user/views/register', [
            'model_reg' => $model_reg,
        ]);
    }

    public function actionEmailConfirm($token)
    {
        try {
            $model = new EmailConfirmForm($token);
        } catch (InvalidParamException $e) {
            //Yii::$app->getSession()->setFlash('error', 'Ошибка подтверждения Email.');
            return $this->redirect(['/user/do/confirm?err=1']);
        }

        if ($model->confirmEmail()) {
            //Yii::$app->getSession()->setFlash('success', 'Спасибо! Ваш Email успешно подтверждён.');
            return $this->redirect(['/user/do/login']);
        } else {
            //Yii::$app->getSession()->setFlash('error', 'Ошибка подтверждения Email.');
            return $this->redirect(['/user/do/confirm?err=1']);
        }

        return $this->goHome();
    }

    public function actionPasswordResetRequest()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->getSession()->setFlash('success', 'Спасибо! На ваш Email было отправлено письмо со ссылкой на восстановление пароля.');

                return $this->goHome();
            } else {
                Yii::$app->getSession()->setFlash('error', 'Извините. У нас возникли проблемы с отправкой.');
            }
        }

        return $this->render('@app/modules/user/views/passwordResetRequest', [
            'model' => $model,
        ]);
    }

    public function actionPasswordReset($token)
    {
        try {
            $model = new PasswordResetForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->getSession()->setFlash('success', 'Спасибо! Пароль успешно изменён.');

            return $this->goHome();
        }

        return $this->render('@app/modules/user/views/passwordReset', [
            'model' => $model,
        ]);
    }
}
