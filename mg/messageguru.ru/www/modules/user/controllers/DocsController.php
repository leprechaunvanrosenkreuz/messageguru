<?php


namespace app\modules\user\controllers;

use app\modules\user\models\User;
use Yii;
use yii\bootstrap\ActiveForm;

use yii\web\Controller;
use yii\web\Response;
use app\modules\user\components\UserController;

use app\models\Files;
use app\modules\user\models\UploadForm;
use yii\web\UploadedFile;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;

class DocsController extends Controller
{
	public $layoutPath = '@app/modules/user/views/docs/';

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'login', 'logout'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['login', 'reset-password-request', 'test', 'create'],
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['index', 'logout', 'update', 'create', 'delete'],
                        'roles' => ['@'],
                    ],
                ],
                'denyCallback' => function ($rule, $action) {
                    $this->redirect(['/user/do/login']);
                }
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    public function actionUpload()
    {
        //echo User::getId();
        //exit;

        $model = new UploadForm();
        $modelFiles = new Files();

        if (Yii::$app->request->isPost) {
            $model->file = UploadedFile::getInstance($model, 'file');

            if ($model->file && $model->validate()) {                
                $model->file->saveAs(\Yii::$app->basePath.'/web/uploads/' . $model->file->baseName . '.' . $model->file->extension);
                //User::getId();
                $modelFiles->user_id = '13';

                $modelFiles->name = $model->file->baseName.'.'.$model->file->extension;
                
                $modelFiles->save();
            }
        }

        return $this->render($this->layoutPath.'upload', ['model' => $model]);
    }

    public function actionView()
    {
        $model = new Files();

        return $this->render($this->layoutPath.'view', ['model' => $model]);
    }
}
