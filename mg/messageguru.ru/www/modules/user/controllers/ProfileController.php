<?php


namespace app\modules\user\controllers;

use app\models\FilesTags;
use app\models\ProfileFiles;
use app\models\Subscribers;
use app\modules\user\models\User;
use yii;
use app\modules\user\components\UserController;
use app\models\Files;
use app\modules\user\models\UploadForm;
use yii\web\UploadedFile;
use yii\web\NotFoundHttpException;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\web\Response;
use yii\widgets\ActiveForm;
use yii\filters\VerbFilter;
use tpmanc\imagick\Imagick;
use app\components\VKApi;
use yii\helpers\ArrayHelper;

class ProfileController extends UserController
{
    public function behaviors()
    {

        return parent::behaviors();
    }

    public $layoutPath = '@app/modules/user/views/profile/';

    public function getUserId()
    {
        if (!empty(Yii::$app->getUser()->id)) {
            if (!is_numeric(Yii::$app->getUser()->id)) {
                return User::find()
                    ->where(['id' => Yii::$app->getUser()->id])
                    ->orWhere(['social_id' => Yii::$app->getUser()->id])
                    ->one()->id;
            } else {
                return Yii::$app->getUser()->id;
            }
        } else {
            return 0;
        }
    }


    public function getBasePath()
    {
        return \Yii::$app->basePath;
    }


    public function fileUpload($model, $fileAttr = 'files')
    {
        $filemodel = new UploadForm();

        $filemodel->$fileAttr = UploadedFile::getInstance($model, $fileAttr);
        // print_r($_FILES);
        if (!empty($filemodel->$fileAttr)) {
            $uploadfile = $this->getBasePath() . '/web/uploads/' . $this->getUserId() . '/' . $filemodel->$fileAttr->baseName . '.' . $filemodel->$fileAttr->extension;

            $filemodel->$fileAttr->saveAs($uploadfile);


            //Imagick::open($uploadfile)->thumb(200, 200)->saveTo($this->getBasePath() . '/web/uploads/' . $this->getUserId() . '/pdf_img.jpeg');

            return $filemodel->$fileAttr->baseName . '.' . $filemodel->$fileAttr->extension;
        }

        return "";
    }


    public function actionIndex()
    {
        $user_id = $this->getUserId();
        if (!empty($user_id) && $user_id != 1) {
            if ($data = $this->getMessages(6)) {
                return $this->render($this->layoutPath . 'index', $data);
            }
        } else {
            return $this->redirect(['/user/do/login']);
        }

    }

    public function saveTags($files_list)
    {

        $filesTags = new FilesTags();
        if (!empty(Yii::$app->request->post()) && !empty(Yii::$app->request->post()['tags'])) {
            $post_tags = Yii::$app->request->post()['tags'];
            //предварительно удалим все теги, а потом пробежимся и запишем их из post
            foreach ($files_list AS $files) {
                $filesTags::deleteAll(['files_id' => $files->id]);
            }

            foreach ($post_tags AS $key => $tags_arr) {
                foreach ($tags_arr AS $tags_id) {
                    $f_Tags = new FilesTags();
                    $f_Tags->files_id = $key;
                    $f_Tags->tags_id = $tags_id;
                    $f_Tags->save();
                }
            }
        }

    }

    /*public function actionRemove()
    {

        if (!empty($this->getUserId())) {
            $query = Files::find();
            $file = $query->where([
                'id' => Yii::$app->request->get('id'),
                'user_id' => $this->getUserId()
            ])->one();
            $delFile = $this->getBasePath() . '/web/uploads/' . $this->getUserId() . '/' . $file->name;
            if ($file->delete()) {
                unlink($delFile);
            }
            return $this->redirect(['/user/profile/index']);
        } else {
            return $this->redirect(['/user/do/login']);
        }
    }*/

    public function actionView()
    {
        $user_id = Yii::$app->request->get('id');
        $model = User::find()->joinWith('achievements')->where(['user.id' => $user_id])->with("files")->one();
        return $this->render($this->layoutPath . 'view', ['model' => $model]);
    }

    public function actionMessages()
    {
        if (!empty($this->getUserId())) {
            if ($data = $this->getMessages()) {
                return $this->render($this->layoutPath . 'messages', $data);
            }
        } else {
            return $this->redirect(['/user/do/login']);
        }
    }

    public function getMessages($limit = 0)
    {
        $model = User::find()->joinWith('achievements')->where(['user.id' => $this->getUserId()])->with("files")->one();

        $files_model = Files::find();
        $files_model->with('user')->where([
            'user_id' => $this->getUserId(),
            'status' => Files::STATUS_ON
        ]);
        if (!empty($limit))
            $files_model->limit($limit);

        $dataProvider = new ActiveDataProvider([
            'query' => $files_model,
            'pagination' => [
                'pageSize' => 4,
            ],
            'sort' => [
                'defaultOrder' => [
                    'created_at' => SORT_DESC,
                ],
                'attributes' => ['created_at']
            ]
        ]);

        if (!empty($dataProvider)) {
            return ['dataProvider' => $dataProvider, 'model' => $model];
        } else throw new NotFoundHttpException();
    }

    public function actionSettings()
    {
        $user_id = $this->getUserId();

        //Привязка соцсети
        //$serviceName = $service;
        $serviceName = Yii::$app->getRequest()->getQueryParam('service');

        if (isset($serviceName)) {
            /** @var $eauth \nodge\eauth\ServiceBase */
            $eauth = Yii::$app->get('eauth')->getIdentity($serviceName);
            //echo Yii::$app->getUser()->getReturnUrl();
            $eauth->setRedirectUrl(Yii::$app->getUser()->getReturnUrl());
            $eauth->setCancelUrl(Yii::$app->getUrlManager()->createAbsoluteUrl('/user/profile/settings'));

            try {
                if ($eauth->authenticate() && !empty($user_id)) {

                    $user = User::findOne($user_id);
                    if (!empty($user)) {

                        //print_r($eauth->getAttributes()); die();
                        $soc_id = ArrayHelper::getValue($eauth->getAttributes(), 'id');
                        $user->social_id = $serviceName . '-' . $soc_id;

                        switch ($serviceName) {
                            case 'vkontakte':
                                $user->vk = ArrayHelper::getValue($eauth->getAttributes(), 'url');
                                break;
                            case 'facebook':
                                $user->fb = ArrayHelper::getValue($eauth->getAttributes(), 'url');
                                break;
                        }

                        $user->update();

                        //добавляем ему друзей из вк
                        if ($serviceName == 'vkontakte')
                            $this->addVkFriends($soc_id, $user->id);

                    }

                    // special redirect with closing popup window
                    $eauth->redirect(['/user/profile/settings']);
                } else {
                    // close popup window and redirect to cancelUrl
                    $eauth->cancel();
                }
            } catch (\nodge\eauth\ErrorException $e) {
                // save error to show it later
                Yii::$app->getSession()->setFlash('error', 'EAuthException: ' . $e->getMessage());

                // close popup window and redirect to cancelUrl
//              $eauth->cancel();
                $eauth->redirect($eauth->getCancelUrl());
            }
        }

        //Yii::$app->cache->flush();
        if (!empty($user_id)) {

            $model = User::find()->joinWith('achievements')->where(['user.id' => $user_id])->with("files")->one();
            $model->fullname = User::getFullname($user_id);

            $load = $model->load(Yii::$app->request->post());

            if (Yii::$app->request->isPost && $load) {
                //сохраняем тэги
                $files_list = ProfileFiles::find()->where(['user_id' => $user_id])->all();
                $this->saveTags($files_list);

                //сохраняем личные данные пользователя
                $model->avatarFile = UploadedFile::getInstance($model, 'avatarFile');

                if ($filename = $model->uploadAvatar())
                    $model->avatar = $filename;

                if ($model->validate() && $load) {

//                    $file = $this->fileUpload($model, 'avatar');
//                    if(!empty($file)){
//                        $model->avatar = $file;
//                    }
                    //print_r($model->getErrors());die();
                    $model->save();
                    Yii::$app->getSession()->setFlash('success', 'Ваши данные успешно обновлены!');
                    return $this->redirect(['/user/profile/settings']);

                }
            }
            return $this->render($this->layoutPath . 'settings', ['model' => $model]);
        } else {
            return $this->redirect(['/user/do/login']);
        }
    }

    /**
     * Добавление друзей вк к юзеру
     * @param $soc_id
     * @param $user_id
     */
    public function addVkFriends($soc_id, $user_id)
    {
        $vk = new VKApi();

        if (!empty($vk->getFriends($soc_id)['response'])) {

            foreach ($vk->getFriends($soc_id)['response'] as $friends_id) {

                $vk_user = 'vkontakte-' . $friends_id;
                $friends = User::find()->where(["social_id" => $vk_user])->one();

                if (!empty($friends)) {
                    //echo $friends->id.'test'.$user_id;exit;
                    $subscribers = new Subscribers();
                    $subscribers->user_id = $friends->id;
                    $subscribers->subscriber_id = $user_id;
                    $subscribers->insert();
                }
                // new Subscribers
            }
            //exit;
        }

    }

    public function actionSubscribers()
    {
        $model = User::find()->joinWith('achievements')->where(['user.id' => $this->getUserId()])->with("files")->one();

        $subscribers = Subscribers::find()
            ->joinWith('subscriber')
            ->where(['subscribers.user_id' => $this->getUserId()])
            ->orderBy('subscribers.id DESC')
            ->all();

        $my_subscribes = Subscribers::find()
            ->joinWith('user')
            ->where(['subscribers.subscriber_id' => $this->getUserId()])
            ->orderBy('subscribers.id DESC')
            ->all();

        return $this->render($this->layoutPath . 'subscribers', ['model' => $model, 'subscribers' => $subscribers, 'my_subscribes' => $my_subscribes]);
    }

    /**
     * Экшн для редактирования файла/презы
     */

    public function actionEditPresentation()
    {
        $file_id = Yii::$app->request->get('id', '');

        if (!empty($file_id)) {
            $model = Files::findOne($file_id);
        } else {
            Yii::$app->getSession()->setFlash('error', 'Неверное id файла');
        }

        $load = $model->load(Yii::$app->request->post());

        if (Yii::$app->request->isAjax && $load) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }

        if ($model->validate() && $load) {
            $model->user_id = Yii::$app->user->id;

            //если это не ссылка на youtube, то сохраянем файл
            if ($model->type != Files::TYPE_YOUTUBE) {
                $file = $this->fileUpload($model, 'name');
                if (!empty($file)) {
                    $model->name = $file;
                }

            }

            if (!empty($model->name) && $model->save()) {
                Yii::$app->getSession()->setFlash('success', 'Ваша презентация успешно загружена!');
            } else {
                Yii::$app->getSession()->setFlash('error', 'Произошла ошибка при загрузке! Попробуйте снова');
            }
        }

        return $this->render($this->layoutPath . 'edit_presentation', ['model' => $model]);

    }

    /**
     * Экшн для добавления файла/презы
     */

    public function actionAddPresentation()
    {
        $model = new Files();

        $load = $model->load(Yii::$app->request->post());

        if (Yii::$app->request->isAjax && $load) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }

        if ($model->validate() && $load) {
            $model->user_id = Yii::$app->user->id;

            //если это не ссылка на youtube, то сохраянем файл
            if ($model->type != Files::TYPE_YOUTUBE) {
                $file = $this->fileUpload($model, 'name');
                if (!empty($file)) {
                    $model->name = $file;
                }

            }

            if (!empty($model->name) && $model->save()) {
                Yii::$app->getSession()->setFlash('success', 'Ваша презентация успешно загружена!');
            } else {
                Yii::$app->getSession()->setFlash('error', 'Произошла ошибка при загрузке! Попробуйте снова');
            }
        }

        return $this->render($this->layoutPath . 'add_presentation', ['model' => $model]);

    }

    /**
     * Экшн для удаления файла/презы
     */

    public function actionRemovePresentation()
    {
        if (!empty(Yii::$app->request->get('id'))) {
            //TODO выяснить удалять или только менять статус
            //Files::findOne(Yii::$app->request->get('id'))->remove();
            Files::removeFiles(Yii::$app->request->get('id'));
        }
        return $this->redirect(['/user/profile']);
    }
}
