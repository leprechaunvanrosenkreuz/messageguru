<?php

namespace app\modules\user\models;

use Yii;
use yii\base\Model;

class UploadForm extends Model
{
    public $file;
    public $files;
    public $avatar;
    public $name;

    public function rules()
    {
        return [
            [['avatar'],'file', 'extensions' => ['jpg', 'jpeg', 'png'], 'checkExtensionByMimeType'=>false],
            [['name'],'file'],
        ];
    }
}