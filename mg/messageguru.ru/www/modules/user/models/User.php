<?php

namespace app\modules\user\models;

use app\models\Achievements;
use app\models\Files;
use app\models\UserDevices;
use yii;
use yii\base\NotSupportedException;
use yii\web\IdentityInterface;
use app\components\validators\mobileValidator;
use yii\helpers\ArrayHelper;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;
use app\models\Cities;
use yii\base\ErrorException;
use app\models\UserAchievements;
use himiklab\yii2\search\behaviors\SearchBehavior;
use app\components\VKApi;

//use app\models\admin\AuthItem;

/**
 * This is the model class for table "{{%user}}".
 *
 * @property integer $id
 * @property integer $type
 * @property integer $status
 * @property integer $pq
 * @property string $auth_key
 * @property string $access_token
 * @property string $email_confirm_token
 * @property string $uuid
 * @property integer $confirm_pushes
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $avatar
 * @property string $login
 * @property string $first_name
 * @property string $last_name
 * @property string $middle_name
 * @property string $email
 * @property string $phone
 * @property string $place_work
 * @property string $position
 * @property string $desc
 * @property string $site
 * @property string $vk
 * @property string $fb
 * @property string $ok
 * @property integer $city_id
 * @property integer $rating
 * @property string $youtube
 * @property string $profession
 * @property string $created_at
 * @property string $updated_at
 * @property string $social_id
 *
 * @property UserAchievements[] $userAchievements
 * @property Achievements[] $achievements
 * @property Files[] $files
 * @property Files[] $activeFiles
 * @property UserDevices[] $devices
 */
class User extends yii\db\ActiveRecord implements IdentityInterface
{
    //User status
    const STATUS_BLOCKED = 0;
    const STATUS_ACTIVE = 1;
    const STATUS_WAIT = 2;

    public function getStatusName()
    {
        return ArrayHelper::getValue(self::getStatusesArray(), $this->status);
    }

    public static function getStatusesArray()
    {
        return [
            self::STATUS_BLOCKED => 'Заблокирован',
            self::STATUS_ACTIVE => 'Активен',
            self::STATUS_WAIT => 'Ожидает подтверждения',
        ];
    }

    //User types
    const TYPE_SPEAKER = 1;
    const TYPE_VIEWER = 2;

    public $type_name;
    public $city;
    public $currentPassword;
    public $newPassword;
    public $newPasswordConfirm;
    public $fullname;
    public $profile;
    public $authKey;
    public $avatarFile;
    public $username;


    public static $typesNames = [
        self::TYPE_SPEAKER => 'Спикер',
        self::TYPE_VIEWER => 'Пользователь'
    ];
    public static $roleByType = [
        self::TYPE_SPEAKER => 'speaker',
        self::TYPE_VIEWER => 'viewer'
    ];

    public static function getRolesList()
    {
        return AuthItem::getRolesList();
    }

    public function getRole()
    {
        return AuthItem::getRolesList()[$this->type];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user}}';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
                'value' => new Expression('NOW()'),
            ],
            'search' => [
                'class' => SearchBehavior::className(),
                'searchScope' => function ($model) {
                    /** @var \yii\db\ActiveQuery $model */
                    $model->select([
                        'first_name',
                        'last_name',
                        'middle_name',
                        'login',
                        'email',
                        'id',
                        'avatar',
                        'type',
                        'status',
                        'created_at']);
                    //$model->andWhere(['indexed' => true]);
                },
                'searchFields' => function ($model) {
                    /** @var self $model */
                    return [
                        ['name' => 'first_name', 'value' => $model->first_name, 'type' => SearchBehavior::FIELD_TEXT],
                        ['name' => 'last_name', 'value' => $model->last_name, 'type' => SearchBehavior::FIELD_TEXT],
                        ['name' => 'middle_name', 'value' => $model->middle_name, 'type' => SearchBehavior::FIELD_TEXT],
                        ['name' => 'login', 'value' => $model->login],
                        ['name' => 'email', 'value' => $model->email],
                        ['name' => 'model_id', 'value' => $model->id, 'type' => SearchBehavior::FIELD_KEYWORD],
                        ['name' => 'avatar', 'value' => $model->avatar],
                        ['name' => 'type', 'value' => $model->type],
                        ['name' => 'created_at', 'value' => $model->created_at],
                        ['name' => 'model_name', 'value' => 'user'],
                        ['name' => 'status', 'value' => $model->status],
                        //['name' => 'userAchievements', 'value' => $model->userAchievements],
                        // ['name' => 'model', 'value' => 'page', 'type' => SearchBehavior::FIELD_UNSTORED],
                    ];
                }
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type', 'status', 'city_id', 'rating'], 'integer'],
            [['email'], 'required'],
            [['created_at', 'updated_at', 'city_id', 'social_id', 'pq'], 'safe'],

            [['type', 'confirm_pushes'], 'integer'],
            [['desc', 'uuid', 'fullname'], 'string'],

            [['password_hash'], 'string', 'max' => 60],
            [['auth_key'], 'string', 'max' => 32],
            [['email'], 'unique'],

            [['email_confirm_token', 'profession', 'password_hash', 'password_reset_token',
                'avatar', 'login', 'first_name', 'last_name', 'middle_name', 'email',
                'place_work', 'position', 'phone', 'youtube', 'site', 'vk', 'fb', 'ok'], 'string', 'max' => 255],

            ['login', 'match', 'pattern' => '/^[a-z]\w*$/i', 'message' => 'Логин должен начинается с буквы и содержать только буквенные символы,
        или числовые символы и знак подчеркивания'],
            // password rules
            [['newPassword'], 'string', 'min' => 8, 'tooShort' => 'Пароль должен содержать минимум 8 символов'],
            ['newPassword', 'match', 'pattern' => '/([A-Z]|[А-Я]){1}/', 'message' => 'В пароле должны присутствовать строчные и прописные буквы и цифры'],
            [['newPassword'], 'filter', 'filter' => 'trim'],
            [['newPasswordConfirm'], 'compare', 'compareAttribute' => 'newPassword', 'message' => 'Пароль и его подтверждение не совпадают'],

//            [['currentPassword', 'newPasswordConfirm'], 'required', 'when' => function ($this) {
//                return $this->newPassword != '';
//            }, 'whenClient' => "function check_integer(attribute, value) {
//                return $('input[name=\"User[newPassword]\"]').val() != '';
//            }", 'message' => 'Пожалуйста, заполните поле'],

            // user update page
            //  [['currentPassword'], 'required', 'on' => ['user_update']],
            [['currentPassword'], 'validateCurrentPassword', 'on' => ['user_update']],
        ];
    }

    /**
     * Validate current password (account page)
     */
    public function validateCurrentPassword()
    {
        if (!$this->validatePassword($this->currentPassword)) {
            $this->addError("currentPassword", "Текущий пароль введен неверно");
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'status' => 'Status',
            'auth_key' => 'Auth Key',
            'email_confirm_token' => 'Email Confirm Token',
            'password_hash' => 'Password Hash',
            'password_reset_token' => 'Password Reset Token',
            'avatar' => 'Avatar',
            'login' => 'Логин',
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'middle_name' => 'Middle Name',
            'email' => 'Email',
            'phone' => 'Phone',
            'site' => 'Site',
            'vk' => 'Vk',
            'fb' => 'Fb',
            'ok' => 'Ok',
            'desc' => 'Desc',
            'city_id' => 'City ID',
            'rating' => 'Rating',
            'youtube' => 'youtube',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'place_work' => 'Место работы',
            'position' => 'Должность',
            'profession' => 'profession',
            'currentPassword' => 'Текущий пароль',
            'newPassword' => 'Новый пароль',
            'newPasswordConfirm' => 'Подтвердите новый пароль',
        ];
    }


    /**
     * @inheritdoc
     */

    public static function findIdentity($id)
    {
        // return static::findOne(['id' => $id]);


        //if (Yii::$app->getSession()->has('user-'.$id)) {
        //    return new self(Yii::$app->getSession()->get('user-'.$id));
        // }
        // else {
        return self::find()
            ->where(['id' => $id])
            ->orWhere(['social_id' => $id])
            ->one();
        //}
    }

    /**
     * Finds an identity by the given token.
     *
     * @param string $token the token to be looked for
     * @return IdentityInterface|null the identity object that matches the given token.
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        //throw new NotSupportedException('findIdentityByAccessToken is not implemented.');
        return static::findOne(['access_token' => $token]);
    }

    /**
     * Finds user by email
     * @param string $email
     * @return static|null
     */
    public static function findByEmail($email)
    {
        return static::findOne(['email' => $email]);
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }
        return static::findOne([
            'password_reset_token' => $token,
        ]);
    }

    /**
     * @param \nodge\eauth\ServiceBase $service
     * @return User
     * @throws ErrorException
     */
    public static function findByEAuth($service)
    {
        if (!$service->getIsAuthenticated()) {
            throw new ErrorException('EAuth user should be authenticated before creating identity.');
        }

        $id = $service->getServiceName() . '-' . $service->getId();
        $attributes = [
            'id' => $id,
            'first_name' => $service->getAttribute('name'),
            'username' => $service->getAttribute('name'),
            'authKey' => md5($id),
            'profile' => $service->getAttributes(),
        ];
        $attributes['profile']['service'] = $service->getServiceName();
        Yii::$app->getSession()->set('user-' . $id, $attributes);
        return new self($attributes);
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return boolean
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }
        //$expire = Yii::$app->params['user.passwordResetTokenExpire'];
        $expire = 24 * 60 * 60;
        $parts = explode('_', $token);
        $timestamp = (int)end($parts);
        return $timestamp + $expire >= time();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    public function generateAccessToken()
    {
        $this->access_token = Yii::$app->security->generateRandomString();
    }


    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }

    /**
     * @param string $email_confirm_token
     * @return static|null
     */
    public static function findByEmailConfirmToken($email_confirm_token)
    {
        return static::findOne(['email_confirm_token' => $email_confirm_token]);
    }

    /**
     * Generates email confirmation token
     */
    public function generateEmailConfirmToken()
    {
        $this->email_confirm_token = Yii::$app->security->generateRandomString();
    }

    /**
     * Removes email confirmation token
     */
    public function removeEmailConfirmToken()
    {
        $this->email_confirm_token = null;
    }

    public static function isAdmin($id)
    {
        $u = static::findOne(['id' => $id]);
        if ($u->type == 1 || $u->type == 2)
            return true;
        else
            return false;
    }

    /**
     * Этот метод вызывается перед сохранением записи (после проверки, если таковые имеются).
     */
    public function beforeSave($insert)
    {
        $dirtyAttributes = $this->getDirtyAttributes();
        if (isset($dirtyAttributes["password"])) {
            $this->newPassword = $dirtyAttributes["password"];
        }

        // hash new password if set
        if ($this->newPassword) {
            $this->setPassword($this->newPassword);
        }

        // ensure fields are null so they won't get set as empty string
        $nullAttributes = ["email", "fullname", "phone", "type"];
        foreach ($nullAttributes as $nullAttribute) {
            $this->$nullAttribute = $this->$nullAttribute ? $this->$nullAttribute : null;
        }

        if (parent::beforeSave($insert)) {
            if ($this->fullname) {
                $names = explode(" ", $this->fullname);
                $this->first_name = !empty($names[1]) ? $names[1] : "";
                $this->last_name = !empty($names[0]) ? $names[0] : "";
                $this->middle_name = !empty($names[2]) ? $names[2] : "";

            }
            $this->generateAuthKey();
            return true;
        } else {
            return false;
        }
    }


    public function afterSave($insert, $changedAttributes)
    {
        $auth = Yii::$app->authManager;

        //$authorRole = 1;
        //$auth->revokeAll($this->getId());
        //$auth->assign($authorRole, $this->getId());

        parent::afterSave($insert, $changedAttributes);
    }

    public function beforeDelete()
    {
        if (parent::beforeDelete()) {
            $auth = Yii::$app->authManager;
            $auth->revokeAll($this->getId());
            return true;
        } else {
            return false;
        }
    }

    /**
     * Возвращает названия ролей
     * @return array
     */
    public static function getTypesNames()
    {
        return self::$typesNames;
    }

    /**
     * Возврвщает название роли
     * @return mixed
     */
    public function getTypeName()
    {
        return ArrayHelper::getValue(self::getTypesNames(), $this->type);
    }

    /**
     * Возвращает название города
     * @param $id
     * @return string
     */
    public function getCity($id)
    {
        return Cities::findOne($id);
    }

    /**
     * Возвращает login текущего пользователя
     * @param $id
     * @return string
     */
    public static function getLogin($id)
    {
        if (!empty($user = self::findOne(['id' => $id])))
            return $user->login;
        else
            return '';
    }

    /**
     * Возвращает имя текущего пользователя
     * @param $id
     * @return string
     */
    public static function getFullname($id)
    {
        if (!empty($user = self::findOne(['id' => $id])))
            return $user->last_name . ' ' . $user->first_name . ' ' . $user->middle_name;
        else
            return '';
    }

    /**
     * Возвращает короткое имя текущего пользователя
     * @param $id
     * @return string
     */
    public static function getShortFullname($id)
    {
        $user = self::findOne(['id' => $id]);
        if (!empty($user)) {
            $sh = explode(" ", $user->fullname);
            return $sh[0];
        } else
            return '';
    }

    /**
     * Возвращает email текущего пользователя
     * @param $id
     * @return string
     */
    public static function getEmail($id)
    {
        $user = self::find()
            ->where(['id' => Yii::$app->getUser()->id])
            ->orWhere(['social_id' => Yii::$app->getUser()->id])
            ->one();
        if (!empty($user)) {
            return $user->email;
        } else
            return '';
    }


    /**
     * Возвращает email текущего пользователя
     * @param $id
     * @return string
     */
    public static function getType($id)
    {
        $user = self::find()
            ->where(['id' => Yii::$app->getUser()->id])
            ->orWhere(['social_id' => Yii::$app->getUser()->id])
            ->one();
        if (!empty($user)) {
            return $user->type;
        } else
            return '';
    }

    /**
     * Возвращает сформированный урл на аватарку
     * @return string
     */
    public function getAvatarUrl()
    {
        if (!empty($this->avatar)) {
            return Yii::$app->params['uploadsUrl'] . '/profile/' . $this->avatar;
        } else {
            return Yii::$app->params['uploadsUrl'] . '/profile/no_ava.png';
        }
    }

    /**
     * Загружает аватар в хранилище
     * @return bool|string
     */
    public function uploadAvatar()
    {
        if (!empty($this->avatarFile)) {
            $path = Yii::getAlias('@uploads/profile/');

            if (!is_dir($path))
                mkdir($path, 0777, true);

            $filename = 'ava_' . uniqid(time()) . '.' . $this->avatarFile->extension;

            if ($this->avatarFile->saveAs($path . $filename)) {
                return $filename;
            } else
                return false;

        } else
            return false;
    }

    /**
     * Скачивает аватар с FB
     * @return bool|string
     */
    public function uploadFbAvatar()
    {
        if (!empty($this->social_id)) {
            $socExploded = explode('-', $this->social_id);

            $type = ArrayHelper::getValue($socExploded, 0);
            $socId = ArrayHelper::getValue($socExploded, 1);

            if ($type == 'facebook' && !empty($socId)) {
                $img = file_get_contents('https://graph.facebook.com/' . $socId . '/picture?type=large');

                $path = Yii::getAlias('@uploads/profile/');

                if (!is_dir($path))
                    mkdir($path, 0777, true);

                $filename = 'ava_' . uniqid(time()) . '.jpg';

                file_put_contents($path . $filename, $img);

                return $filename;
            }
        }

        return '';
    }

    public function uploadVkAvatar($soc_id)
    {
        $vk = new VKApi();

        if (!empty($result = ArrayHelper::getValue($vk->getUserInfo($soc_id), 'response.0'))) {
            if (!empty($ava_url = ArrayHelper::getValue($result, 'photo_200_orig'))) {
                $img = file_get_contents($ava_url);

                $path = Yii::getAlias('@uploads/profile/');

                if (!is_dir($path))
                    mkdir($path, 0777, true);

                $filename = 'ava_' . uniqid(time()) . '.jpg';

                file_put_contents($path . $filename, $img);

                return $filename;
            } else return '';
        } else return '';
    }

    /**
     * Возвращает кол-во реальных през у автора
     * @param bool $only_active
     * @return int
     */
    public function getFilesCount($only_active = true)
    {
        $q = Files::find()->where(['user_id' => $this->id]);

        if ($only_active)
            $q->andWhere(['status' => Files::STATUS_ON]);

        return count($q->all());
    }

    /**
     * Возвращает текст последнего полученного юзером уровня
     * @return string
     */
    public function getCurrentUserTextLevel()
    {
        if (isset($this->achievements)) {
            //Берем последнее достижение по истории
            /** @var \app\models\Achievements $achieve */
            $achieve = end($this->achievements);
            if (isset($achieve) && $achieve->type == Achievements::TYPE_LEVEL) {
                return $achieve->name;
            }
        }
        return '';
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserAchievements()
    {
        return $this->hasMany(UserAchievements::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAchievements()
    {
        return $this->hasMany(Achievements::className(), ['id' => 'ach_id'])
            ->via('userAchievements');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFiles()
    {
        return $this->hasMany(Files::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActiveFiles()
    {
        return $this->hasMany(Files::className(), ['user_id' => 'id'])->where(['files.status' => Files::STATUS_ON]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDevices()
    {
        return $this->hasMany(UserDevices::className(), ['user_id' => 'id']);
    }
}