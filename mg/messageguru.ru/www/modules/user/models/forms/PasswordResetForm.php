<?php
namespace app\modules\user\models\forms;

use app\modules\user\models\User;
use yii\base\InvalidParamException;
use yii\base\Model;
use Yii;
/**
 * PasswordReset reset form
 */
class PasswordResetForm extends Model
{
    public $password;

    private $_user;
    /**
     * Creates a form model given a token.
     *
     * @param  string                          $token
     * @param  array                           $config name-value pairs that will be used to initialize the object properties
     * @throws \yii\base\InvalidParamException if token is empty or not valid
     */
    public function __construct($token, $config = [])
    {

        if (empty($token) || !is_string($token)) {
            throw new InvalidParamException('Токен не может быть пустым.');
        }

        $this->_user = User::findByPasswordResetToken($token);
        if (!$this->_user) {
            throw new InvalidParamException('Неверный токен.');
        }
        parent::__construct($config);
    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['password', 'required', 'message' => 'Пожалуйста, укажите пароль'],
            ['password', 'string', 'min' => 8, 'tooShort' => 'Пароль должен содержать минимум 8 символов'],
            ['password', 'match', 'pattern'=>'/([A-Z]|[А-Я]){1}/', 'message' => 'В пароле должны присутствовать строчные и прописные буквы и цифры'],
            ['password', 'match', 'pattern'=>'/\d+/', 'message' => 'В пароле должны присутствовать строчные и прописные буквы и цифры'],

        ];
    }

    public function attributeLabels()
    {
        return [
            'password' => 'Пароль',
        ];
    }
    /**
     * Resets password.
     *
     * @return boolean if password was reset.
     */
    public function resetPassword()
    {
        $user = $this->_user;
        $user->setPassword($this->password);
        $user->removePasswordResetToken();
        return $user->save(false);
    }
}