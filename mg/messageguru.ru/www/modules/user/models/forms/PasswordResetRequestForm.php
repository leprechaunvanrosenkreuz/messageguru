<?php

namespace app\modules\user\models\forms;

use app\modules\user\models\User;
use Yii;
use yii\base\Model;

/**
 * Password reset request form
 */
class PasswordResetRequestForm extends Model
{
    public $email;
    public $login;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            //['login', 'filter', 'filter' => 'trim'],
            //['login', 'required'],
            //['login', 'match', 'pattern' => '#^[\w_-]+$#i'],

            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'exist',
                'targetClass' => '\app\modules\user\models\User',
                'filter' => ['status' => User::STATUS_ACTIVE],
                'message' => 'У этой компании не указан E-mail.'
            ],
        ];
    }

    public function attributeLabels()
    {
        return [
            'login' => 'Логин, на который зарегистрирован кабинет',
            'email' => 'Адрес электронной почты',
        ];
    }

    /**
     * Sends an email with a link, for resetting the password.
     *
     * @return boolean whether the email was send
     */
    public function sendEmail()
    {

        $user = User::findOne([
            'status' => User::STATUS_ACTIVE,
            'type' => User::TYPE_SPEAKER,
            'email' => $this->email,
            //'login' => $this->login
        ]);
        if ($user) {
            if (!User::isPasswordResetTokenValid($user->password_reset_token)) {
                $user->generatePasswordResetToken();
            }
            if ($user->save(false)) {
                $img_path = Yii::getAlias('@app/modules/user/assets/img/mail/');
                return Yii::$app->mailer->compose('@app/modules/user/views/mail/passwordReset', ['user' => $user,
                    'bg' => $img_path . 'bg.png',
                    'body' => $img_path . 'body.png',
                    'footer' => $img_path . 'footer.png',
                    'head' => $img_path . 'head.png',
                ])
                    ->setFrom(Yii::$app->params['mailFrom'])
                    ->setTo($this->email)
                    ->setSubject('Сброс пароля для ' . Yii::$app->name)
                    ->send();
            }
        }
        return false;
    }
}