<?php

namespace app\modules\user\models\forms;

use app\modules\user\models\User;
use Yii;
use yii\base\Model;
use app\components\validators\mobileValidator;

/**
 * Registration Form
 */
class RegForm extends Model
{
    public $type;
    public $password;
    public $password_confirm;
    public $phone;
    public $email;
    public $fullname;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['type', 'safe', 'message' => 'Пожалуйста, укажите пароль'],

            ['password', 'required', 'message' => 'Пожалуйста, укажите пароль'],
            ['password', 'string', 'min' => 8, 'tooShort' => 'Пароль должен содержать минимум 8 символов'],
            ['password', 'match', 'pattern' => '/([A-Z]|[А-Я]){1}/', 'message' => 'В пароле должны присутствовать строчные и прописные буквы и цифры'],
            ['password', 'match', 'pattern' => '/\d+/', 'message' => 'В пароле должны присутствовать строчные и прописные буквы и цифры'],

            ['password_confirm', 'required', 'message' => 'Пожалуйста, повторите пароль'],
            ['password_confirm', 'string', 'min' => 6],

            ['phone', 'required', 'message' => 'Пожалуйста, укажите номер мобильного телефона'],
            ['phone', mobileValidator::className(), 'message' => 'Номер мобильного телефона указан неверно'],

            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required', 'message' => 'Пожалуйста, укажите электронный адрес'],
            ['email', 'email', 'message' => 'Пожалуйста, укажите корректный электронный адрес'],
            //['email', 'unique', 'targetClass' => User::className(), 'message' => 'Данный электронный адрес уже был использован.'],

            ['fullname', 'required', 'message' => 'Пожалуйста, укажите фамилию имя отчество'],
            ['fullname', 'match', 'message' => 'Только русские буквы', 'pattern' => '/^[а-яА-ЯёЁ\s-]+$/u'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'type' => 'Тип',
            'password' => 'Пароль',
            'password_confirm' => 'Повторите пароль',
            'phone' => 'Номер телефона',
            'email' => 'Адрес электронной почты',
            'fullname' => 'ФИО',
        ];
    }

    /**
     * Registration user
     *
     * @return User|null the saved model or null if saving fails
     */
    public function register($email = null)
    {
        $model = new User();
        $model->type = $this->type;
        $model->fio = $this->fullname;

        $model->phone = $this->phone;
        //TODO email validaiton issue. Email is not unique
        $model->email = $this->email;

        $model->setPassword($this->password);
        $model->generateAuthKey();
        if ($model->save(false)) {
            if ($this->validate()) {
                return $model;
            } else {
                var_dump('errors: ', $model->errors);
            }
        } else return false;
    }
}