<?php

namespace app\modules\user\models\forms;

use app\models\Achievements;
use app\models\UserAchievements;
use app\modules\user\models\User;
use Yii;
use yii\base\Model;
use app\components\validators\mobileValidator;
use yii\web\UploadedFile;
use app\modules\user\models\UploadForm;

/**
 * Registration Form
 */
class RegistrationForm extends Model
{
    public $login;
    public $password;
    public $password_confirm;
    public $phone;
    public $email;
    public $fullname;
    public $type;
    public $last_name;
    public $first_name;
    public $middle_name;
    public $place_work;
    public $avatar;
    public $file;
    public $avatarFile;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['password', 'required', 'message' => 'Пожалуйста, укажите пароль'],
            ['password', 'string', 'min' => 8, 'tooShort' => 'Пароль должен содержать минимум 8 символов'],
            ['password', 'match', 'pattern' => '/([A-Z]|[А-Я]){1}/', 'message' => 'В пароле должны присутствовать строчные и прописные буквы и цифры'],
            ['password', 'match', 'pattern' => '/\d+/', 'message' => 'В пароле должны присутствовать строчные и прописные буквы и цифры'],

            ['password_confirm', 'required', 'message' => 'Пожалуйста, повторите пароль'],
            ['password_confirm', 'string', 'min' => 6],

            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required', 'message' => 'Пожалуйста, укажите электронный адрес'],
            ['email', 'email', 'message' => 'Пожалуйста, укажите корректный электронный адрес'],
            ['email', 'unique', 'targetClass' => User::className(), 'message' => 'Данный электронный адрес уже был использован.'],
            ['login', 'required', 'message' => 'Пожалуйста, придумайте логин'],
            ['login', 'unique', 'targetClass' => User::className(), 'message' => 'Данный логин уже занят.'],

            [['type', 'login', 'place_work', 'avatar', 'phone'], 'safe'],

            [['middle_name', 'file', 'avatar'], 'safe'],
            [['file'],'file', 'extensions' => 'pdf, pptx, ppt'],
            [['avatar'],'file'],

            ['fullname', 'required', 'message' => 'Пожалуйста, укажите фамилию имя отчество'],
            ['fullname', 'match', 'message' => 'Только русские буквы', 'pattern' => '/^[а-яА-ЯёЁ\s-]+$/u'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'password' => 'Пароль',
            'password_confirm' => 'Повторите пароль',
            'phone' => 'Номер телефона',
            'email' => 'Адрес электронной почты',
            'fullname' => 'ФИО',
            'type' => 'Тип',
            'avatar' => 'Аватар',
        ];
    }

    /**
     * Registration user
     *
     * @return User|null the saved model or null if saving fails
     */
    public function registration($email = null)
    {

        if ($this->validate()) {
            /** @var User $user */
            $user = new User();

            $auth = Yii::$app->authManager;
            $speaker_auth = $auth->getRole('speaker');


            $user->avatarFile = UploadedFile::getInstance($this, 'avatarFile');
            if ($filename = $user->uploadAvatar())
                $user->avatar = $filename;

            $user->first_name = $this->first_name;
            $user->middle_name = $this->middle_name;
            $user->last_name = $this->last_name;
            $user->email = $this->email;
            $user->phone = $this->phone;
            $user->type = $speaker_auth;
            $user->login = $this->login;
            $user->status = User::STATUS_WAIT;
            $user->setPassword($this->password);
            $user->generateAuthKey();
            $user->generateEmailConfirmToken();
            $user->save(false);

            $auth->assign($speaker_auth, $user->getId());

            $ach = new UserAchievements();
            $ach->user_id = $user->id;
            $ach->ach_id = Achievements::getIdByAlias('stranger');
            $ach->save();


            return $user;
            /*if ($user->save(false)) {

                $auth->assign($speaker_auth, $user->getId());

                Yii::$app->mailer->compose('@app/modules/user/views/mail/confirmation', ['user' => $user])
                    ->setFrom(Yii::$app->params['mailFrom'])
                    ->setTo($this->email)
                    ->setSubject('Подтверждение регистрации. ' . Yii::$app->name)
                    ->send();
                return $user;
            }*/
        }
        return null;
    }

    public function beforeValidate()
    {
        $this->last_name = '';
        $this->first_name = $this->fullname;
        $this->middle_name = '';
        /*if (!empty($this->fullname)) {
            $names = explode(' ', $this->fullname);
            $this->last_name = '';
            $this->first_name = '';
            $this->middle_name = '';

            ksort($names);

            foreach ($names as $key => $item) {
                if (empty($item))
                    unset($names[$key]);
                else
                    $bufer[] = $names[$key];
            }

            $names = $bufer;

            if (isset($names[0]))
                $this->last_name = $names[0];
            if (isset($names[1]))
                $this->first_name = $names[1];
            if (isset($names[2]))
                $this->middle_name = $names[2];
            if (isset($names[3]))
                $this->middle_name .= ' ' . $names[3];
        }

        if(empty($this->type)){
            $this->type = 1;
        }*/
        return parent::beforeValidate();
    }

    public function afterValidate()
    {/*
        //$this->getErrors()
        $last = $this->getErrors('last_name');
        //print_r($last); die();
        if (!empty($last[0]))
            $this->addError("name", $last[0]);
        $first = $this->getErrors('first_name');
        if (!empty($first[0]))
            $this->addError("name", $first[0]);
        $middle = $this->getErrors('middle_name');
        if (!empty($middle[0]))
            $this->addError("name", $middle[0]);*/

        parent::afterValidate();
    }
}