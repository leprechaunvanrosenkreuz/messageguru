<?php
/**
 * Created by PhpStorm.
 * User: manchetten
 * Date: 05.04.17
 * Time: 0:28
 */

namespace app\modules\user\models\forms;

use app\modules\user\models\User;
use Yii;
use yii\base\Model;
use app\components\validators\mobileValidator;

/**
 * Registration Form
 */
class UploadFileForm extends Model
{
    public $file;
    public $avatar;
    public $name;
    public $type;

    public function rules()
    {
        return [
            [['file'], 'file', 'extensions' => ['pdf', 'pptx', 'ppt'], 'checkExtensionByMimeType' => true, 'mimeTypes' => ['text/plain'], 'skipOnEmpty' => false],
            ['title', 'required', 'message' => 'Пожалуйста, укажите название презентации'],

        ];
    }
}