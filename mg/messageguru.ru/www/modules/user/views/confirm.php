<?php
/**
 * Created by PhpStorm.
 * User: marduk
 * Date: 15.03.17
 * Time: 20:21
 */

use yii\helpers\Html;
use yii\helpers\Url;

?>

<?php $this->beginContent('@app/views/layouts/layout_no_header.php'); ?>

<div class="register_container login_container flex_container">
    <div class="outline">

        <div class="inline_logo">Message<span>GURU</span></div>

        <h1 class="color_primary">Спасибо за регистрацию</h1>

        <?php
        //TODO сделать форму ввода токена вручную + повторную отправку письма
        if (empty($_GET['err'])) {
            echo '<p>На ваш e-mail отправлено письмо со ссылкой на подтверждение регистрации.</p>';
        } else {
            echo '<p>Неправильный токен</p>';
        } ?>

        <?= Html::a('Вернуться на главную!', Url::home(), ['class' => 'btn bg_primary']) ?>

    </div>
</div>


<?php $this->endContent(); ?>
