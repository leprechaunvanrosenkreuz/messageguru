<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\modules\user\assets\UserAsset;
use kartik\social\Module;
use yii\helpers\Url;
use kartik\social\VKPlugin;
use app\components\widgets\SocialAuthWidget;

UserAsset::register($this);
$assetsUrl = UserAsset::getAssetsUrl();

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \app\modules\user\models\forms\LoginForm */

$this->title = 'Вход';

$fieldOptions1 = [
    'options' => ['class' => 'form-group has-feedback'],
    //'inputOptions' => ['autofocus' => 'autofocus', 'tabindex' => '1', 'placeholder' => $model->getAttributeLabel('email')],
    'template' => "{input}{error}"
];

$fieldOptions2 = [
    'options' => ['class' => 'form-group has-feedback'],
    //'inputOptions' => ['autofocus' => 'autofocus', 'tabindex' => '2', 'placeholder' => $model->getAttributeLabel('password')],
    'template' => "{input}{error}"
];
?>
 
<?php $this->beginContent('@app/views/layouts/layout_no_header.php'); ?>

<div class="register_container login_container flex_container">
    <div class="outline">

        <div class="inline_logo">Message<span>GURU</span></div>

        <div id="alert"></div>
        <?php $form = ActiveForm::begin([
            'id' => 'login-form',
            'enableClientValidation' => true,
            //'enableAjaxValidation' => true
        ]); ?>
        <?= $form
            ->field($model, 'email', $fieldOptions1)
            ->label(false)
            ->textInput(['placeholder' => 'Ваш E-mail...']) ?>

        <?= $form
            ->field($model, 'password', $fieldOptions2)
            ->label(false)
            ->passwordInput(['placeholder' => 'Введите Пароль...']) ?>


        <?= $form->field($model, 'rememberMe')->checkbox(['label' => 'Запомнить меня']) ?>

        <?= Html::submitButton(Yii::t('app', 'Вход'), ['class' => 'btn bg_primary transition', 'name' => 'login-button']) ?>

        <p class="color_primary">Или войти с помощью:</p>

        <?= SocialAuthWidget::widget(['action' => '/user/do/login']); ?>

        <?= Html::a(Yii::t('app', 'Забыли пароль?'), Url::toRoute(['/user/do/password-reset-request', 'email' => $model->email])) ?>

        <?php ActiveForm::end(); ?>


        <?php
        if (Yii::$app->getSession()->hasFlash('error')) {
            echo '<div class="alert alert-danger">' . Yii::$app->getSession()->getFlash('error') . '</div>';
        }
        ?>
        
    </div>
</div>

<?php $this->endContent(); ?>
