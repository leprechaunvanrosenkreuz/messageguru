<?php

/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

use yii\helpers\Html;
use yii\helpers\Url;
use app\modules\user\assets\UserAsset;

/**
 * @var app\modules\user\models\User $user
 */
$resetLink = Yii::$app->urlManager->createAbsoluteUrl(['user/do/password-reset', 'token' => $user->password_reset_token]);

UserAsset::register($this);
$assetsUrl = Yii::$app->request->hostInfo . UserAsset::getAssetsUrl();
?>


<table width="100%" cellpadding="0" cellspacing="0" border="0" data-mobile="true" dir="ltr" align="center"
       data-width="600"
       style="background-image: url('<?= $message->embed($bg); ?>'); background-position: center top; background-repeat: repeat; background-color: rgb(255, 255, 255);">
    <tbody>
    <tr>
        <td align="center" valign="top" style="padding:0;margin:0;">

            <table align="center" bgcolor="transparent" border="0" cellspacing="0" cellpadding="0" width="600"
                   class="wrapper" style="width: 600px; background-color: transparent;">
                <tbody>

                <tr>
                    <td>
                        <table cellpadding="0" cellspacing="0" align="center" data-editable="image"
                               data-mobile-width="1" width="100%" style="max-width: 100% !important;">
                            <tbody>
                            <tr>
                                <td valign="top" align="center"
                                    style="display: inline-block; padding: 0px; margin-top: 70px; background-color: transparent;"
                                    class="tdBlock"><img
                                        src="<?= $message->embed($head); ?>"
                                        width="600"
                                        style="border-width: 0px; border-style: none; border-color: transparent; display: block; width: 100%; max-width: 100% !important;">
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center"
                               data-editable="text">
                            <tbody>
                            <tr>
                                <td align="left" valign="top" class="lh-1"
                                    style="padding: 30px 70px; text-align: center; font-size: 16px; font-family: Arial, Helvetica Neue, Helvetica, sans-serif; line-height: 1.15; background-image: url('<?= $message->embed($body); ?>'); background-position: center top; background-repeat: repeat-y;">


                                    Здравствуйте, <?= $user->first_name . " " . $user->last_name . " " . $user->middle_name ?>
                                    ! <br>

                                    <b>Для получения нового пароля перейдите по ссылке:</b><br>

                                    <?= Html::a(Html::encode($resetLink), $resetLink) ?>
                                    <br><br>


                                    Если ссылка не активна, скопируйте ее в адресную строку браузера<br>

                                    Если вы не просили сбросить пароль – проигнорируйте данное письмо.
                                    <br>
                                    Спасибо!

                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table cellpadding="0" cellspacing="0" align="center" data-editable="image"
                               data-mobile-width="1" width="100%" style="max-width: 100% !important;">
                            <tbody>
                            <tr>
                                <td valign="top" align="center"
                                    style="display: inline-block; padding: 0px; margin-bottom: 70px;" class="tdBlock">
                                    <img
                                        src="<?= $message->embed($footer); ?>"
                                        width="600"
                                        style="border-width: 0px; border-style: none; border-color: transparent; display: block; width: 100%; max-width: 100% !important;">
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    </tbody>
</table>