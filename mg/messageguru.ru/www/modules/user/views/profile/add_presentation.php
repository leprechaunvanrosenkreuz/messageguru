<?php
/**
 * Created by PhpStorm.
 * User: manchetten
 * Date: 28.03.17
 * Time: 10:29
 */


use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use app\models\Tags;
use kartik\file\FileInput;
use yii\helpers\Url;
use app\modules\user\models\User;

?>

<?php $this->beginContent('@app/views/layouts/layout_main.php'); ?>

<?php if (Yii::$app->getSession()->hasFlash('success')) : ?>
    <div class="alert alert-success" role="alert"><?= Yii::$app->getSession()->getFlash('success', null, true) ?></div>
<?php endif; ?>

<?php if (Yii::$app->getSession()->hasFlash('error')) : ?>
    <div class="alert alert-danger" role="alert"><?= Yii::$app->getSession()->getFlash('error', null, true) ?></div>
<?php endif; ?>

<div class="wrapper">
    <?php $form = ActiveForm::begin([
        'options' => ['enctype' => 'multipart/form-data'],
        'enableAjaxValidation' => true,
        'enableClientValidation' => true,
    ]); ?>
    <div class="block overflow add_message_block">
        <div class="col col_l left add_message_zone">

            <div class="form-group field-user-avatar  bg_sub noise_light message_upload_box">
                <input type="hidden" name="User[avatar]" value="">
                            <?= FileInput::widget([
                                'attribute' => 'name',
                                'model' => $model,
                                'options'=>[
                                    'multiple'=>false
                                ],
                                'pluginOptions' => [
                                    'overwriteInitial'=>true,
                                    'uploadUrl' => Url::to(['user/profile/add-presentation']),
                                    'showPreview' => true,
                                    'showCaption' => false,
                                    'showRemove' => false,
                                    'showUpload' => false,
                                    'browseLabel' => 'Добавить!',
                                    'browseClass' => 'btn btn_border color_primary transition',
                                    'language' => 'ru',
                                    'uploadExtraData' => [
                                        'id' => $model->id,
                                        'avatar' => 1,
                                    ],
                                    'maxFileCount' => 1
                                ]
                            ]);

                            ?>
                            <!--<div class="file-drop-zone-title">
                                <i class="fa fa-cloud-upload color_primary" aria-hidden="true"></i>
                                <span>Для загрузки предентации, перенесите файл сюда, либо нажмите кнопку "Добавить":</span>
                            </div>
                            <div class="file-preview-thumbnails">
                            </div>
                            <div class="clearfix"></div>
                            <div class="file-preview-status text-center text-success"></div>
                            <div class="kv-fileinput-error file-error-message" style="display: none;"></div>-->


                 <!--   <button type="button" tabindex="500" title="Abort ongoing upload" class="btn btn-default hide fileinput-cancel fileinput-cancel-button"><i class="glyphicon glyphicon-ban-circle"></i>  <span class="hidden-xs">Cancel</span></button>

                    <div tabindex="500" class="btn btn_border color_primary transition btn-file"><i class="glyphicon glyphicon-folder-open"></i>&nbsp;  <span class="hidden-xs">Добавить!</span><input style="display: none" type="file" id="user-avatar" name="User[avatar]" value="Сова-зевает-живность-милота-2500852.jpeg"></div>
                -->
            </div>
            <div class="bg_sub noise_light message_upload_input" style="display: none">
                <div class="file-drop-zone">
                    <i class="fa fa-youtube-play color_primary" aria-hidden="true"></i>
                    <?= $form->field($model, 'name')->label(false)->textInput(
                        array(
                            'placeholder' => 'Вставьте ссылку на ролик..',
                            'class' => 'form-control',
                            'aria-required' => "true"
                        )
                    )  ?>
                </div>
            </div>
        </div>
        <div class="col col_s right message_form">

                <div class="form-group field-registrationform-email required">
                    <div class="col-sm-12 ">
                        <?= $form->field($model, 'title')->label(false)->textInput(
                                [
                                    'placeholder' => 'Название презентации...',
                                    'class' => 'form-control',
                                    'aria-required' => "true"
                                ]
                        )  ?>
                        <span class="underline_form"></span>
                        <div class="help-block help-block-error "></div>
                    </div>
                    <div class="col-sm-12 ">
                        <?= $form->field($model, 'desc')->label(false)->textInput(
                            [
                                'placeholder' => 'Краткое описание...',
                                'class' => 'form-control',
                                'aria-required' => "true"
                            ]
                        )  ?>
                        <span class="underline_form"></span>
                        <div class="help-block help-block-error "></div>
                    </div>
                    <div class="col-sm-12 ">

                        <?= $form->field($model, 'type')->label(false)->dropDownList(
                            app\models\Files::getTypesList(),
                            [
                                'placeholder' => 'Тип презентации...',
                                'class' => 'form-control',
                                'aria-required' => "true"
                            ]
                        ); ?>

                        <div class="help-block help-block-error "></div>
                    </div>

                </div>
            <?= Html::submitButton('Загрузить!', ['class' => 'btn bg_primary transition']) ?>

        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>
<div class="clear"></div>


<?php $this->endContent(); ?>
