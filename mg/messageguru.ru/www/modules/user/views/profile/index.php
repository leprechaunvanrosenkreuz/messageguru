<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use app\models\Tags;
use kartik\file\FileInput;
use yii\helpers\Url;
use app\modules\user\models\User;
use \app\models\Cities;
use \app\components\widgets\ProfileLeftBar;
use \app\components\widgets\ProfileMenuBar;
use yii\widgets\ListView;
 
/* @var $this yii\web\View */
/* @var $model app\modules\user\models\User */
/* @var $form ActiveForm */

$this->title = 'Профиль';
?>
<?php $this->beginContent('@app/views/layouts/layout_main.php'); ?>

    <div class=" noise_light overflow">
        <div class="wrapper">
            <?= ProfileLeftBar::widget(['model_id' => $model->id]) ?>

            <div class="col right">
                <?= ProfileMenuBar::widget() ?>

                <div class="col col_l left autor_block">
                    <div class="messages_container overflow">
                        <?php
                        echo ListView::widget( [
                            'dataProvider' => $dataProvider,
                            'itemView' => 'items/_item_messages',
                            'layout' => "{items}\n  ",
                        ] ); ?>
                        

                        <a href="profile/messages" class="message_more color_primary">
                            <i class="fa fa-search" aria-hidden="true"></i>
                            Показать все!
                        </a>
                    </div>


                </div>
            </div>
        </div>
        <div class="clear"></div>
    </div>
<?php $this->endContent(); ?>