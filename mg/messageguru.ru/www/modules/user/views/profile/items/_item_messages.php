<?php
/**
 * Created by PhpStorm.
 * User: marduk
 * Date: 29.03.17
 * Time: 21:40
 */

use yii\helpers\Html;
use yii\helpers\Url;
use app\modules\user\models\User;
?>
<div class="message">
    <?= Html::a('', Url::to(['/pres/' . $model->id]), ['style' => 'background-image: url('.$model->getImageUrl().')', 'class' => 'message_view transition', 'title' => '']) ?>


    <h4 class="color_primary"><?= Html::a(User::getFullname($model->user_id), Url::to(['/author/' . $model->user_id]), ['class' => 'color_primary']); ?></h4>
    <p><?= $model->title ?></p>
    <div class="message_meta_inf ">
        <span><?= date("d.m.Y", strtotime($model->created_at)) ?></span>
        <span><?= (!empty($model->views)? $model->views : 0) ?> просмотров</span>
    </div>

    <div class="message_controls color_primary">
        <span class="like" rel="<?= $model->id ?>"><i class="fa fa-gratipay" aria-hidden="true">  </i> <span><?= app\models\Likes::getCountLike($model->id) ?></span></span>
        <?= Html::a('<i class="fa fa-arrow-circle-o-right color_primary" aria-hidden="true"></i>', Url::to(['/pres/' . $model->id]), ['class' => 'letsgo']) ?>
        <?= Html::a('<i class="fa fa-remove color_primary" aria-hidden="true"></i>', Url::to(['/user/profile/remove-presentation?id=' . $model->id]), ['class' => 'remove']) ?>
    </div>
</div>