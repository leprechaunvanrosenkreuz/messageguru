<?php
/**
 * Created by PhpStorm.
 * User: marduk
 * Date: 23.03.17
 * Time: 19:22
 */

/* @var \yii\data\ActiveDataProvider $dataProvider */
?>
<?php
use \app\components\widgets\ProfileLeftBar;
use \app\components\widgets\ProfileMenuBar;
use yii\widgets\ListView;
?>
<?php $this->beginContent('@app/views/layouts/layout_main.php'); ?>

    <div class=" noise_light overflow">
        <div class="wrapper">
            <?= ProfileLeftBar::widget(['model_id' => $model->id]) ?>


            <div class="col right col_l">

                <?= ProfileMenuBar::widget(['model' => $model]) ?>

                <div class="messages_inline_container overflow">
                    <?php
                    echo ListView::widget( [
                        'dataProvider' => $dataProvider,
                        'itemView' => 'items/_item_messages',
                        'layout' => "{items}\n <div class='clear'> {pager}</div> ",
                    ] ); ?>

                 </div>
            </div>
        </div>
        <div class="clear"></div>
    </div>

<?php $this->endContent(); ?>