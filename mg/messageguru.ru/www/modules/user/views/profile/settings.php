<?php
/**
 * Created by PhpStorm.
 * User: marduk
 * Date: 23.03.17
 * Time: 19:22
 */
?>
<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use app\modules\user\assets\UserAsset;
use kartik\select2\Select2;
use app\models\Tags;
use kartik\file\FileInput;
use yii\helpers\Url;
use app\modules\user\models\User;
use \app\components\widgets\ProfileLeftBar;
use \app\components\widgets\ProfileMenuBar;
use \app\components\widgets\Avatar;
use kartik\social\Module;
use kartik\social\VKPlugin;
use app\components\widgets\SocialAuthWidget;

?>

<?php $this->beginContent('@app/views/layouts/layout_main.php'); ?>
<?php
UserAsset::register($this);
$assetsUrl = UserAsset::getAssetsUrl();

?>

<?php if (Yii::$app->getSession()->hasFlash('success')) : ?>
    <div class="alert alert-success" role="alert"><?= Yii::$app->getSession()->getFlash('success', null, true) ?></div>
<?php endif; ?>

<?php if (Yii::$app->getSession()->hasFlash('error')) : ?>
    <div class="alert alert-danger" role="alert"><?= Yii::$app->getSession()->getFlash('error', null, true) ?></div>
<?php endif; ?>

    <div class=" noise_light overflow">
        <div class="wrapper">

            <?= ProfileLeftBar::widget(['model_id' => $model->id]) ?>

            <div class="col right col_l">
                <?= ProfileMenuBar::widget(['model' => $model]) ?>

                <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>


                <div class="block settings_block">
                    <div class="edit_user_form">

                        <h2 style="text-align: center;">Поменять фото: </h2>

                        <div class="file_upload">
                            <button type="button" class="btn bg_primary">Загрузить фото!</button>
                            <?= $form->field($model, 'avatarFile')->fileInput() ?>

                        </div>
                        <div class="change_file">Файл выбран! Нажмите кнопку "Сохранить изменений" для применения
                            изменений.
                        </div>


                    </div>
                </div>
                <div class="block settings_block">


                    <?php //echo $form->field($model, 'avatar')->fileInput()->label('') ?>
                    <div class="edit_user_form">
                        <h2>Личные данные: </h2>
                        <div class="form_grup">
                            <?= $form->field($model, 'fullname')->label('<i class="fa fa-user color_primary"></i> ')->textInput(array('placeholder' => 'Ваше Имя......')) ?>
                            <?= $form->field($model, 'email')->label('<i class="fa fa-envelope color_primary"></i>  ')->textInput(array('placeholder' => 'Ваш E-mail...')) ?>
                        </div>
                        <div class="form_grup">
                            <? /*= $form->field($model, 'city')->label('<i class="fa fa-globe color_primary"></i> ')->textInput(array('placeholder' => 'Страна и город...')) */ ?><!--
                            -->
                            <?= $form->field($model, 'city_id')->widget(Select2::classname(), [
                                'data' => \app\models\Cities::getCitiesList(),
                                'options' => ['placeholder' => 'Страна и город...', 'multiple' => false, 'class' => 'geo_select'],
                                'pluginOptions' => [
                                    'tags' => true,
                                    'tokenSeparators' => [',', ' '],
                                    'maximumInputLength' => 10
                                ],
                            ])->label('<i class="fa fa-globe color_primary"></i> '); ?>
                            <?= $form->field($model, 'desc')->label('<i class="fa fa-heart color_primary"></i>  ')->textInput(['placeholder' => 'Ваши интересы...', 'maxlength' => 100]) ?>
                        </div>
                        <div class="form_grup">
                            <?= $form->field($model, 'place_work')->label('<i class="fa fa-briefcase color_primary"></i>  ')->textInput(['placeholder' => 'Место работы...']) ?>
                            <?= $form->field($model, 'position')->label('<i class="fa fa-star color_primary"></i>  ')->textInput(['placeholder' => 'Должность...', 'maxlength' => 100]) ?>
                        </div>
                    </div>
                </div>


                <div class="block settings_block">

                    <div class="edit_user_form ">
                        <h2>Ваши ссылки: </h2>

                        <div class="form_grup">
                            <?= $form->field($model, 'site')->label('<i class="fa fa-external-link color_primary"></i> ')->textInput(array('placeholder' => 'Ваш сайт...')) ?>
                            <?= $form->field($model, 'lk')->label('<i class="fa fa-linkedin color_primary"></i>  ')->textInput(array('placeholder' => 'Профиль linkedin...')) ?>
                        </div>
                        <div class="form_grup">
                            <?= $form->field($model, 'vk')->label('<i class="fa fa-vk color_primary"></i> ')->textInput(array('placeholder' => 'Профиль VK...')) ?>
                            <?= $form->field($model, 'fb')->label('<i class="fa fa-facebook color_primary"></i>  ')->textInput(array('placeholder' => 'Профиль Facebook...')) ?>
                        </div>
                        <div class="form_grup">
                            <?= $form->field($model, 'youtube')->label('<i class="fa fa-youtube-play color_primary"></i> ')->textInput(array('placeholder' => 'Канал YouTube...')) ?>
                            <?= $form->field($model, 'tw')->label('<i class="fa fa-twitter color_primary"></i>  ')->textInput(array('placeholder' => 'Аккаунт Twitter...')) ?>
                        </div>
                    </div>
                </div>

                <div class="block settings_block">
                    <div class="edit_user_form">
                        <h2>Сменить пароль: </h2>
                        <div class="form_grup">
                            <?= $form->field($model, 'newPassword')->label('<i class="fa fa-lock color_primary"></i> ')->passwordInput(array('placeholder' => 'Придумайте пароль...')) ?>
                            <?= $form->field($model, 'newPasswordConfirm')->label('<i class="fa fa-lock color_primary"></i>  ')->passwordInput(array('placeholder' => 'Повторите пароль...')) ?>
                        </div>
                    </div>
                </div>


                <div class="block settings_block soc_links_profile_settings">

                    <div class="edit_user_form">
                        <h2>Ваши ссылки: </h2>
                        <p>Авторизуйте свой профиль в социальной сети и получите возможность быстрого входа:</p>
                        <?= SocialAuthWidget::widget(['action' => '/user/profile/settings']); ?>
                    </div>
                </div>

                <?= Html::submitButton('Сохранить изменения!', ['class' => 'btn bg_primary']) ?>
                <?php ActiveForm::end(); ?>


            </div>
        </div>
        <div class="clear"></div>
    </div>


<?php $this->endContent(); ?>