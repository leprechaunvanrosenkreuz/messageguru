<?php
/**
 * Created by PhpStorm.
 * User: marduk
 * Date: 23.03.17
 * Time: 19:22
 */
?>
<?php
use yii\helpers\Html;
use yii\helpers\Url;
use app\modules\user\models\User;
use app\models\Subscribers;
use \app\components\widgets\ProfileLeftBar;
use \app\components\widgets\ProfileMenuBar;
use \app\models\Files;
use app\assets\AppAsset;

AppAsset::register($this);
$assetsUrl = AppAsset::getAssetsUrl();

?>
<?php $this->beginContent('@app/views/layouts/layout_main.php'); ?>

    <div class=" noise_light overflow">
        <div class="wrapper">

            <?= ProfileLeftBar::widget(['model_id' => $model->id]) ?>

            <div class="col_l right">
                <?= ProfileMenuBar::widget(['model' => $model]) ?>

                <div class="author_container overflow">

                    <ul class="nav nav-tabs">
                        <li class="active"><a data-toggle="tab" href="#my_subscribes">Мои подписки
                                (<?= count($my_subscribes) ?>)</a></li>
                        <li><a data-toggle="tab" href="#subscribers">Мои подписчики (<?= count($subscribers) ?>)</a>
                        </li>
                    </ul>

                    <div class="tab-content">

                        <div id="my_subscribes" class="tab-pane fade in active">
                            <div class="block overflow add_message_block add_message_zone  noise_light">
                                <?php
                                if (!empty($my_subscribes)) {
                                    /** @var Subscribers $subscribe */
                                    foreach ($my_subscribes as $subscribe) {
                                        if (!empty($user = $subscribe->user)) {

                                            $avatar = $subscribe->user->getAvatarUrl();
                                            echo '<div class="author">';

                                            echo Html::a('<div class="author_photo" style="background-image: url(' . $avatar . ')" ></div>', Url::to(['/author/' . $user->id]), []);
                                            echo '<div class="bg_primary pq_counter">PQ ' . $user->pq . '</div>';
                                            echo '<h3 class="author_name color_primary">';
                                            echo Html::a($user->first_name . ' ' . $user->last_name, Url::to(['/author/' . $user->id]), []);
                                            echo '</h3>';

                                            echo '</div>';
                                        }
                                    }
                                }
                                ?>
                            </div>
                        </div>

                        <div id="subscribers" class="tab-pane fade">
                            <div class="block overflow add_message_block add_message_zone  noise_light">
                                <?php
                                if (!empty($subscribers)) {
                                    /** @var Subscribers $subscriber */
                                    foreach ($subscribers as $subscriber) {
                                        if (!empty($user = $subscriber->subscriber)) {

                                            $avatar = $subscriber->subscriber->getAvatarUrl();
                                            echo '<div class="author">';

                                            echo Html::a('<div class="author_photo" style="background-image: url(' . $avatar . ')" ></div>', Url::to(['/author/' . $user->id]), []);
                                            echo '<div class="bg_primary pq_counter">PQ ' . $user->pq . '</div>';
                                            echo '<h3 class="author_name color_primary">';
                                            echo Html::a($user->first_name . ' ' . $user->last_name, Url::to(['/author/' . $user->id]), []);
                                            echo '</h3>';

                                            echo '</div>';
                                        }
                                    }
                                }
                                ?>
                            </div>
                        </div>

                    </div>

                </div>

            </div>
            <div class="clear"></div>
        </div>
    </div>

<?php $this->endContent(); ?>