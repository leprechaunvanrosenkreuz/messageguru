<?php
/**
 * Created by PhpStorm.
 * User: marduk
 * Date: 23.03.17
 * Time: 18:16
 */
?>

<?php
use yii\helpers\Html;
use yii\helpers\Url;
use app\modules\user\models\User;
?>
<?php $this->beginContent('@app/views/layouts/layout_main.php'); ?>
<?= User::getTypesNames()[$model->type] ?>
<?= $model->fullname ?>
<?= $model->email ?>
<?= $model->phone ?>


<?php $this->endContent(); ?>
