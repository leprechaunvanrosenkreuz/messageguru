<?php
/**
 * Created by PhpStorm.
 * User: manchetten
 * Date: 01.03.17
 * Time: 21:29
 */

/* @var $this yii\web\View */
/* @var $model_reg \app\modules\user\models\forms\RegistrationForm */

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use kartik\file\FileInput;
use yii\helpers\Url;
use yii\bootstrap\Modal;

$this->title = 'Регистрация';

$var = 123;
//начало многосточной строки, можно использовать любые кавычки
$script = <<< JS
    $('.bg_primary').bind("click", function() {
      $('.register_step1 .help-block').each(function() {
        if($(this).text() != ""){
            $('.register_step1').show();
            $('.register_step2').hide();
        }
      })
    });
    
    $('.back').bind("click", function() {
        $('.register_step1').show();
        $('.register_step2').hide();
     });
JS;
//маркер конца строки, обязательно сразу, без пробелов и табуляции
$this->registerJs($script, yii\web\View::POS_READY);

?>

<?php $this->beginContent('@app/views/layouts/layout_no_header.php'); ?>

<?php


?>

<div class="register_container flex_container">
    <div class="left register_bg col">
        <div class="register_cta">
            <span class="register_cta_1">Присоединяйтесь к<br></span>
            <span class="register_cta_2">messageguru.ru<br></span>
            <span class="register_cta_3">и делитесь своими идеями!</span>
        </div>
    </div>
    <?php $form = ActiveForm::begin([
        'options' => [
            'enctype' => 'multipart/form-data'
        ],
        'id' => 'form-signup'
    ]); ?>
    <div class="add_photo_contaner" style="display: none">
        <div class="add_photo">
            <div class="no_photo"></div>
        </div>
        <!-- <input type="submit" value="Загрузить фото" class="btn btn_border"> -->
        <div class="file_upload">
                <button type="button" class="btn btn_border color_secondary">Загрузить фото</button>
            <?= $form->field($model_reg, 'avatarFile')->fileInput()->label(false)  ?>
                
            </div> 
        <?php /*echo FileInput::widget([
            'attribute' => 'avatar',
            'model' => $model_reg,
            'options' => [
                'multiple' => false
            ],
            'pluginOptions' => [
                //   'browseClass' => 'btn btn_border',
                'showUpload' => false,
                'overwriteInitial' => true,
                'uploadUrl' => Url::to(['do/setavatar']),
                'maxFileCount' => 1
            ]
        ]);*/ ?>
    </div>
    <div class="right col">

        <div class="inline_logo">Message<span>GURU</span></div>


        <div class="register_step1">

            <?= $form->field($model_reg, 'email', ['inputOptions' => ['placeholder' => 'Ваш E-mail...']])->label(false) ?>

            <?= $form->field($model_reg, 'password', [
                'inputOptions' => [
                    'placeholder' => 'Придумайте пароль...',
                ],
            ])->label(false)->passwordInput() ?>
            <?= $form->field($model_reg, 'password_confirm', [
                'inputOptions' => [
                    'placeholder' => 'Повторите пароль...',
                ],
            ])->label(false)->passwordInput() ?>


            <input id="check" type="checkbox"> <label for="check">Я принимаю все </label><a
                class="color_primary rules_link" href="javascript:;" data-toggle="modal"
                data-target="#modalUserAgreement"> правила</a>


            <?= Html::button('Далее!', ['class' => 'btn bg_primary transition']) ?>


            <p class="color_primary">Или войти с помощью:</p>
            <?= \app\components\widgets\SocialAuthWidget::widget(['action' => '/user/do/login']); ?>


        </div>

        <div class="register_step2" style="display: none">

            <?= $form->field($model_reg, 'fullname', ['inputOptions' => ['placeholder' => 'Ваше Имя...']])->label(false) ?>
            <?=
            $form->field($model_reg, 'phone')->widget(\yii\widgets\MaskedInput::classname(), [
                'mask' => '+7 (999) 999-99-99',
                'options' => [
                    'placeholder' => '+7 (___) ___-__-__',
                    'class' => 'form-control',
                ],
            ])->label(false) ?>
            <?= $form->field($model_reg, 'login', ['inputOptions' => ['placeholder' => 'Придумайте логин...']])->label(false) ?>


            <?= Html::submitButton('Зарегистрироваться!', ['class' => 'btn bg_primary transition']) ?>


            <a href="#" class="back"><i class="fa fa-angle-left" aria-hidden="true"></i> Назад</a>
        </div>

    </div>

    <?php ActiveForm::end(); ?>
    <span
        class="go2login">Уже есть аккаунт? Воспользуйтесь <?= Html::a('формой входа', Url::to(['/user/do/login'])) ?></span>
</div>

<?php Modal::begin([
    'header' => '<h2>Правила пользования сайтом MessageGuru</h2>',
    'id' => 'modalUserAgreement',
    'size' => Modal::SIZE_LARGE
]);

echo '<h3>Терминология</h3>
<p>Messageguru.ru – расположенный по одноименному адресу сайт, содержащий материалы (информационные, учебные, графические и прочие), Участников и Администрации.</p>
<p>Администрация messageguru.ru (Администрация) – ООО «Издательство «Розничные финансы», являющееся владельцем авторских и иных прав в отношении messageguru.ru, включая права на доменное имя. </p>
<p>Участник – физическое лицо, успешно прошедшее регистрацию на messageguru.ru, получившее возможность размещать материалы (информационные, учебные, графические и прочие), знакомиться с материалами других Участников и Администрации, обмениваться мнениями. Факт регистрации на messageguru.ru подтверждает согласие Участника с Правилами. Полученный Участником при регистрации логин/пароль Стороны (Администрация и Участник) считают средством идентификации Участника. Участник не вправе разглашать логин/пароль, сформулированные при регистрации на messageguru.ru.</p>
<p>Посетитель – физическое лицо, не прошедшее регистрацию, но осуществляющее доступ к messageguru.ru. </p>
<p>Правила – разработанные Администрацией условия договора об использовании messageguru.ru, обязательные для выполнения Участниками и Посетителями. Администрация вправе изменять Правила в одностороннем порядке, путем размещения на messageguru.ru соответствующего сообщения. Новая редакция Правил вступает в силу со дня, следующего за днем размещения Администрацией соответствующего сообщения на сайте. С указанной даты обращение Участника/ Посетителя к messageguru.ru означает его согласие с новой редакцией Правил.</p>
<h3>Правила</h3>
<p>Настоящие Правила являются офертой о заключении договора об использовании сайта messageguru.ru, акцептом которой является дальнейшее обращение Участника/Посетителя к любому материалу /любой странице сайта messageguru.ru. Ссылка на Правила размещена на первой странице messageguru.ru. Фактом получения оферты считается обращение Участника/ Посетителя к любой странице сайта messageguru.ru.</p>
<p>Участник вправе размещать на messageguru.ru только материал, в отношении которого он является обладателем авторских и иных необходимых прав. Фактом размещения каждого материала Участник заверяет Администрацию в том, что обладает всеми необходимыми для такого размещения правами в отношении размещаемого материала, в соответствии размещенного материала Правилам, действующему законодательству РФ, и отсутствии ущемления прав третьих лиц. 
Всю ответственность перед Администрацией, другими Участниками и третьими лицами за размещение материалов и сообщений, нарушающих Правила или законодательство РФ, несет Участник, их разместивший. Участник/Посетитель обязуется своими силами и за свой счет урегулировать претензии / возместить убытки, связанные с размещением материалов и иными его действиями на messageguru.ru.</p>
<p>Размещение материала на messageguru.ru является обнародованием соответствующего материала правообладателем. </p>
<p>Администрация имеет право в любой момент: <br>
- корректировать и удалять материалы, нарушающие требования Правил или законодательства РФ;<br>
- направлять Участнику сообщения по указанным им при регистрации реквизитам.<br>
</p>
<p>Все размещенные на messageguru.ru материалы защищены авторскими правами. Материалы могут быть использованы с согласия авторов (Участников, Администрации), в порядке и случаях, предусмотренных законодательством РФ.</p>
<p>На messageguru.ru категорически запрещено размещение материалов/создание сообщений, пропагандирующих расовую, религиозную, этническую ненависть или вражду, экстремизм, а также иным образом нарушающих законодательство РФ, права Участников/Пользователей, Администрации или третьих лиц.</p>
';

Modal::end();
?>

<?php $this->endContent(); ?>
