<?php
// _list_item.php

/** @var \app\modules\user\models\User $model */
use yii\helpers\Html;
use yii\helpers\Url;
use app\assets\AppAsset;
use \app\models\Files;

AppAsset::register($this);
$assetsUrl = AppAsset::getAssetsUrl();

?>

<div class="author">
    <?php
    $model = (object)$model;
    ?>
    <?= Html::a('<div class="author_photo" style="background-image: url(' . $model->getAvatarUrl() . ')" ></div>', Url::to(['/author/' . $model->id]), ['data-pjax' => 0]) ?>
    <div class="bg_primary pq_counter"><?= $model->getCurrentUserTextLevel() ?></div>
    <h3 class="author_name color_primary"><?= Html::a($model->first_name . ' ' . $model->last_name, Url::to(['/author/' . $model->id]), ['data-pjax' => 0]) ?></h3>
</div>