<?php
/**
 * Created by PhpStorm.
 * User: manchetten
 * Date: 21.03.17
 * Time: 23:09
 */

/* @var \app\modules\user\models\User $author */


use yii\widgets\ListView;
use app\modules\user\models\User;
use yii\helpers\Html;
use yii\helpers\Url;
use \app\components\widgets\ProfileLeftBar;
use \app\components\widgets\ProfileMenuBar;
use \app\components\widgets\SearchWidget;
use yii\widgets\Pjax;

$this->title = User::getFullName($author->id);
?>

<?php $this->beginContent('@app/views/layouts/layout_main.php'); ?>

<div class="search">
    <div class="wrapper">
        <?= SearchWidget::widget() ?>
    </div>
    <div class="search_mask transition"></div>
</div>


<div class="noise_light overflow bordered">
    <div class="wrapper">
        <?= ProfileLeftBar::widget(['model_id' => $author->id]) ?>
        <div class="col col_l right autor_block">
            <?php Pjax::begin(); ?>
            <div class="preheader">
                <h2 class="left"><span class="color_primary">Messages</span>:</h2>
                <div class="filters left">
                    <?= Html::a("За сегодня", ['authors/filter-author-today/' . $author->id], ['class' => 'filter ' . (($curr == 1) ? 'active_filter' : '')]) ?>
                    <?= Html::a("За месяц", ['authors/filter-author-month/' . $author->id], ['class' => 'filter ' . (($curr == 2) ? 'active_filter' : '')]) ?>
                    <?= Html::a("За все время", ['authors/filter-author-all/' . $author->id], ['class' => 'filter ' . (($curr == 3) ? 'active_filter' : '')]) ?>
                </div>
            </div>
            <div class="messages_container overflow">
                <?= ListView::widget([
                    'dataProvider' => $filesProvider,
                    'options' => [
                        'tag' => 'div'
                    ],
                    'layout' => "{items}\n <div class='clear'> {pager}</div> ",
                    'itemView' => function ($model, $key, $index, $widget) {
                        return $this->render('list_item', ['model' => $model]);

                        // or just do some echo
                        // return $model->title . ' posted by ' . $model->author;
                    },
                    'itemOptions' => [
                        'tag' => false,
                    ],
                    'pager' => [
                        'maxButtonCount' => 5,
                        'options' => [
                            'class' => 'color_primary pagination',
                        ],

                        'activePageCssClass' => 'bg_primary active',
                    ],
                ]);
                ?>
            </div>
            <?php Pjax::end(); ?>
        </div>


    </div>
    <div class="clear"></div>
</div>

<?php $this->endContent(); ?>
