<?php
/**
 * Created by PhpStorm.
 * User: manchetten
 * Date: 21.03.17
 * Time: 23:10
 */

/* @var \yii\data\ActiveDataProvider $usersProvider */

use yii\widgets\ListView;
use yii\widgets\Pjax;

use yii\helpers\Html;
use yii\helpers\Url;
use \app\components\widgets\SearchWidget;

$this->title = 'Список авторов';
?>

<?php $this->beginContent('@app/views/layouts/layout_main.php'); ?>
<div class="search">
    <div class="wrapper">
        <?= SearchWidget::widget() ?>
    </div>
    <div class="search_mask transition"></div>
</div>
<div class="noise_light overflow bordered">
    <div class="wrapper">
        <div class="col col_l left autor_block">
            <?php Pjax::begin(); ?>
            <div class="preheader">
                <h2 class="left"><span class="color_primary">Все</span> Авторы:</h2>
                <div class="filters left">
                    <?= Html::a("По новизне", ['authors/index'], ['class' => 'filter ' . (($curr == 1) ? 'active_filter' : '')]) ?>
                    <?= Html::a("По PQ", ['authors/filter-productivity'], ['class' => 'filter ' . (($curr == 2) ? 'active_filter' : '')]) ?>
                    <?= Html::a("По грейдам", ['authors/filter-rating'], ['class' => 'filter ' . (($curr == 3) ? 'active_filter' : '')]) ?>

                </div>
            </div>

            <div class="author_container overflow">
                <?= ListView::widget([
                    'dataProvider' => $usersProvider,
                    'options' => [
                        'tag' => 'div'
                    ],
                    'layout' => "{items}\n <div class='clear'> {pager}</div> ",
                    'itemView' => function ($model, $key, $index, $widget) {
                        return $this->render('author_item', ['model' => $model]);

                    },
                    'itemOptions' => [
                        'tag' => false,
                    ],
                    'pager' => [
                        'maxButtonCount' => 5,
                        'options' => [
                            'class' => 'color_primary pagination',
                        ],

                        'activePageCssClass' => 'bg_primary active',
                    ],
                ]);
                ?>

            </div>
            <?php Pjax::end(); ?>
        </div>


        <div class="col col_s right recommendations">
            <h2 class="bold_title">Рекомендации <span class="color_primary">MG для Вас:</span></h2>
            <?= \app\components\widgets\RecommendBar::widget() ?>
            <div class="tags">
                <h2 class="bold_title">Популярные теги:</h2>
                <?= \app\components\widgets\TagsBar::widget() ?>
            </div>
        </div>
    </div>
</div>


<?php $this->endContent(); ?>
