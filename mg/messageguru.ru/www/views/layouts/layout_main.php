<?php
/**
 * Created by PhpStorm.
 * User: manchetten
 * Date: 21.03.17
 * Time: 23:38
 */
use app\assets\AppAsset;

AppAsset::register($this);
$assetsUrl = AppAsset::getAssetsUrl();
?>

<?= $this->render('../partials/header', ['assetsUrl' => $assetsUrl]); ?>

<?php if (Yii::$app->getSession()->hasFlash('success')) : ?>
    <div class="alert alert-success" role="alert"><?= Yii::$app->getSession()->getFlash('success', null, true) ?></div>
<?php endif; ?>

<?php if (Yii::$app->getSession()->hasFlash('error')) : ?>
    <div class="alert alert-danger" role="alert"><?= Yii::$app->getSession()->getFlash('error', null, true) ?></div>
<?php endif; ?>

<?= $content ?>

<?= $this->render('../partials/footer', ['assetsUrl' => $assetsUrl]); ?>
