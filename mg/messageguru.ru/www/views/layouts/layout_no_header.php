<?php
/**
 * Created by PhpStorm.
 * User: manchetten
 * Date: 21.03.17
 * Time: 23:43
 */
use app\assets\AppAsset;

AppAsset::register($this);
$assetsUrl = AppAsset::getAssetsUrl();
?>

<div class="underline bg_primary"></div>

<div class="bg_sub noise first_bg"></div>
<div class="bg_secondary noise second_bg"></div>

<?= $content ?>

<?= $this->render('../partials/footer', ['assetsUrl' => $assetsUrl]); ?>
