<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;

?>

<?php $this->beginPage() ?>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title . ' | MessageGuru') ?></title>
    <?php $this->head(); ?>
    <link rel="shortcut icon" href="<?= Yii::$app->homeUrl; ?>favicon.ico" type="image/x-icon">
    <link rel="apple-touch-icon" href="<?= Yii::$app->homeUrl; ?>apple-touch-icon.png">
    <link rel="apple-touch-icon-precomposed" href="<?= Yii::$app->homeUrl; ?>apple-touch-icon-precomposed.png">
    <link rel="apple-touch-icon" sizes="76x76" href="<?= Yii::$app->homeUrl; ?>touch-icon-ipad.png">
    <link rel="apple-touch-icon" sizes="120x120" href="<?= Yii::$app->homeUrl; ?>touch-icon-iphone-retina.png">
    <link rel="apple-touch-icon" sizes="152x152" href="<?= Yii::$app->homeUrl; ?>touch-icon-ipad-retina.png">
    <script>

    </script>
</head>
<body>
<?php $this->beginBody() ?>

<?= $content ?>

<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-100963052-1', 'auto');
    ga('send', 'pageview');

</script>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
