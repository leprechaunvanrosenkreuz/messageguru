<?php
/**
 * Created by PhpStorm.
 * User: manchetten
 * Date: 21.03.17
 * Time: 23:28
 */
use yii\widgets\Breadcrumbs;
use yii\helpers\Html;
use yii\helpers\Url;

$url = Url::base();
$script = <<< JS
    var baseUrl = '$url';
JS;
$this->registerJs($script, yii\web\View::POS_HEAD);
?>

<div class="underline bg_primary"></div>

<?= Breadcrumbs::widget([
    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
]) ?>

<div class="header">
    <div class="wrapper">
        <i class="fa fa-bars color_primary mobile_menu" aria-hidden="true"></i>
        <i class="fa fa-times color_primary mobile_menu_off" aria-hidden="true"></i>
        <div class="inline_logo left"><?= Html::a('Message<span>Guru</span>', Url::home()) ?></div>
        <div class="mobile_menu_window">
            <div class="nav_line left">
                <?= Html::a('Авторы', Url::to(['/authors'])) ?>
                <?= Html::a('Сообщения', Url::to(['/presentation'])) ?>
            </div>
            <?= \app\components\widgets\UserRightHeader::widget() ?>
        </div>

    </div>
</div>