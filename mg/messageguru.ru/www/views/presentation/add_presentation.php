<?php
/**
 * Created by PhpStorm.
 * User: manchetten
 * Date: 28.03.17
 * Time: 10:29
 */


use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use app\models\Tags;
use app\models\Files;
use kartik\file\FileInput;
use yii\helpers\Url;
use app\modules\user\models\User;

$this->title = "Добавить презентацию";

$script = <<< JS
    $(document).ready(function(){
        $("#files-presfile").on("change", function() {
          $("#w0").submit();
        })
    })
JS;
//маркер конца строки, обязательно сразу, без пробелов и табуляции
$this->registerJs($script, yii\web\View::POS_READY);

?>

<div class="bg_sub noise_light">
    <?php $this->beginContent('@app/views/layouts/layout_main.php'); ?>
    <div class="wrapper_addform">




        <h1 class="color_primary">Шаг 1:</h1>
        <div class="message_block_underline"></div>

        <ul class="nav nav-tabs">
          <li class="active"><a data-toggle="tab" href="#pres">Презентация</a></li>
          <li><a data-toggle="tab" href="#video">Видео</a></li>
        </ul>

        <div class="tab-content">

            <div id="pres" class="tab-pane fade in active">
                <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
                <div class="block overflow add_message_block add_message_zone  noise_light">
                    <i class="fa color_secondary icon_upload fa-cloud-upload" aria-hidden="true"></i>
                    <?= $form->field($model, 'type')->hiddenInput(['value' => Files::TYPE_PRESENTATION])->label(false) ?>
                    <div class="file_upload">
                        <button type="button" class="btn bg_primary">Выбрать файл!</button>
                        <?= $form->field($model, 'presFile')->fileInput(['accept'=>'application/pdf,application/vnd.oasis.opendocument.presentation,application/vnd.ms-powerpoint,application/vnd.openxmlformats-officedocument.presentationml.presentation']) ?>
                    </div>
                </div>
                <?php ActiveForm::end(); ?>
            </div>

            <div id="video" class="tab-pane fade">
                <?php $form = ActiveForm::begin(); ?>
                <div class="block overflow add_message_block add_message_zone  noise_light">
                    <i class="fa color_secondary icon_upload fa-youtube-square" aria-hidden="true"></i>
                    <div class="video_form">
                        <?= $form->field($model, 'type')->hiddenInput(['value' => Files::TYPE_YOUTUBE])->label(false) ?>
                        <?= $form->field($model, 'name')->label(false)->textInput(
                            [
                                'placeholder' => 'Ссылка на видео с youtube...',
                            ]
                        ) ?>
                        <input type="submit" class="btn bg_primary" value ="Далее!">
                    </div>
                </div>
                <?php ActiveForm::end(); ?>
            </div>

        </div>
    </div>

    <?php $this->endContent(); ?>
</div>

