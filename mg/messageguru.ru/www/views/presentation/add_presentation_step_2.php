<?php
/**
 * Created by PhpStorm.
 * User: manchetten
 * Date: 28.03.17
 * Time: 10:29
 */


use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use app\models\Tags;
use app\models\Files;
use kartik\file\FileInput;
use yii\helpers\Url;
use app\modules\user\models\User;
use yii\widgets\Pjax;

$this->title = "Добавить презентацию - Шаг 2";

$get_images_url = Url::to(['json-gate/get-images', 'id' => $model->id]);
$check_url = Url::to(['json-gate/get-count', 'id' => $model->id]);
$id = $model->id;

$script = <<< JS
var baseUrl = '$check_url';
var countPages;
var currCnt = 0;
var parse = false;

var model_id = '$id';
var isCheck = true;

$(document).ready(function() {
    $("#files-title").val("");
    
    $(document).on("click", '.presentation_slide', function(event) { 
        //console.log($(this));
        $("#files-image").val($(this).attr("rel"));
    });
    
    //Hover select slide
    $('.presentation_prew_item').hover(function () {
            $(this).append('<div class="take_main">Сделать основным слайдом!</div>');
            $(this).find('.take_main').fadeIn(300);
    })
    $('.presentation_prew_item').mouseleave(function () {
            $(this).find('.take_main').remove();
    })
        
    function slideSelector() {
        //Select main slide
        $('.presentation_prew_item').click(function () {
            $('.presentation_prew_item').removeClass('slide_selected');
            $(this).addClass('slide_selected');
            $('.slide_check').remove();
            $(this).append('<div class="slide_check bg_primary"><i class="fa fa-check" aria-hidden="true"></i></div>');
        })
    }
    
    function checkCount() {
        if(isCheck) {
          $.ajax({
                url: '$check_url'
            }).done(function(data) {
                var old_pages_count = window.localStorage.getItem(model_id + 'loaded_pages');
                if(!old_pages_count)
                    window.localStorage.setItem(model_id + 'loaded_pages', data.data.pages_loaded);
                
                console.log('Old pages: ', old_pages_count);
               
                if(old_pages_count < data.data.pages_loaded) {
                    if(data.data.need_conversion && data.data.is_converted == 0) {
                      $('.progress-bar').text('Преобразуем презентацию в правильный формат');
                    } else {
                       $('.progress-bar').text("Загружено " + data.data.pages_loaded + " слайдов из " + data.data.pages_total);
                       //console.log(currCnt*100/countPages +"px");
                       $('.progress-bar').css("width", (data.data.pages_loaded*100/data.data.pages_total) +"%");
                       window.localStorage.setItem(model_id + 'loaded_pages', data.data.pages_loaded);
                       getImages(data.data);
                    }
                } else {
                    $('.progress-bar').text('Преобразуем презентацию в правильный формат');
                }
                    
                if(old_pages_count == data.data.pages_total && !data.data.need_conversion) {
                    isCheck = false;
                    window.localStorage.removeItem(model_id + 'loaded_pages');
                    $('.progress-bar').text('Презентация успешно преобразована! Выберите главную картинку презентации');
                }
               
            });
        }
    }
    
    function getImages(stat) {
      $.ajax({
            url: '$get_images_url'
        }).done(function(data) {
            var imageCollection = $('.presentation_prew_item');
            var template = '';
            currCnt = imageCollection.length;
            
            for(i = imageCollection.length-1; i < Object.keys(data.data).length; i++){
                if(data.data[i] && data.data[i] !='undefined')
                    template += '<div class="presentation_prew_item"><div class="presentation_slide" rel="'+data.data[i]+'" style="background-image: url('+data.data[i]+')"></div><span>Слайд #'+(i+1)+'</span></div>';
            }
            
            if(template && typeof template != 'undefined')
                imageCollection.last().after(template);            
            
            slideSelector();
            
            // if(first){
            //     $('.slide_check').hide();
            //     $('.presentation_prew_item').eq(1).addClass('slide_selected');
            // }
        });
    }
    
    setInterval(function(){
         checkCount();
    }, 3000);
});
   
JS;
$this->registerJs($script, yii\web\View::POS_END);/*    setInterval(function(){
    $("#refreshButton").click();
}, 3000);*/
?>
<div class="bg_sub noise_light">
    <?php $this->beginContent('@app/views/layouts/layout_main.php'); ?>


    <div class="wrapper_addform wrapper_addform_step_two">
        <h1 class="color_primary">Шаг 2:</h1>
        <div class="message_block_underline"></div>


        <div class="col col_l left">
            <div class="progress">
                <div class="progress-bar progress-bar-striped active bg_primary" role="progressbar"
                     aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width:0%">

                </div>
            </div>
            <div class="block presentation_prew_zone">
                <div class="presentation_prew_item" style="display: none"></div>
            </div>
            <div class="line_hide"></div>
        </div>

        <div class="col col_s right">
            <div class="block">
                <?php $form = ActiveForm::begin([
                    'enableAjaxValidation' => true,
                    'enableClientValidation' => false,
                    'validateOnChange' => true,
                ]); ?>
                <div class="form-group field-registrationform-email required add_message_form">
                    <?= $form->field($model, 'type')->hiddenInput(['value' => Files::TYPE_PRESENTATION])->label(false) ?>
                    <?= $form->field($model, 'image')->hiddenInput()->label(false) ?>
                    <?= $form->field($model, 'title')->label(false)->textInput(
                        [
                            'placeholder' => 'Название презентации...',
                            'class' => 'form-control',
                            'aria-required' => "true"
                        ]
                    ) ?>


                    <?= $form->field($model, 'cat_id')->label(false)->dropDownList(
                        \app\models\CategoriesDict::getCatsList(),
                        [
                            'placeholder' => 'Категория',
                            'class' => 'form-control',
                            'aria-required' => "true"
                        ]
                    ); ?>

                    <!--  <?= $form->field($model, 'type')->label(false)->dropDownList(
                        app\models\Files::getTypesList(),
                        array(
                            'placeholder' => 'Тип презентации...',
                            'class' => 'form-control',
                            'aria-required' => "true"
                        )
                    ); ?> -->
                    <?= $form->field($model, 'desc')->label(false)->textarea(
                        [
                            'placeholder' => 'Краткое описание...',
                            'class' => 'form-control',
                            'aria-required' => "true"
                        ]
                    ) ?>

                    <?= $form->field($model, 'tags')->widget(Select2::classname(), [
                        'data' => Tags::getTagsList(),
                        'showToggleAll' => false,
                        'options' => ['placeholder' => 'Выберите теги ...', 'multiple' => true],
                        'pluginOptions' => [
                            'tags' => true,
                            'tokenSeparators' => [',', ' '],
                            'maximumInputLength' => 30
                        ],
                    ])->label(''); ?>
                    <?= Html::submitButton('Сохранить', ['class' => 'btn bg_primary transition']) ?>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>

    </div>
    <div class="clear"></div>


    <?php $this->endContent(); ?>

</div>