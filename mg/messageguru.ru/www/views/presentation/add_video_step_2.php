<?php
/**
 * Created by PhpStorm.
 * User: manchetten
 * Date: 28.03.17
 * Time: 10:29
 */


use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use app\models\Tags;
use app\models\Files;
use kartik\file\FileInput;
use yii\helpers\Url;
use app\modules\user\models\User;
use yii\widgets\Pjax;

$this->title = "Добавить презентацию - Шаг 2";

$get_images_url = Url::to(['json-gate/get-images', 'id' => $model->id]);
$check_url = Url::to(['json-gate/get-count', 'id' => $model->id]);
$id = $model->id;

?>
<div class="bg_sub noise_light">
    <?php $this->beginContent('@app/views/layouts/layout_main.php'); ?>


    <div class="wrapper_addform wrapper_addform_step_two">
        <h1 class="color_primary">Шаг 2:</h1>
        <div class="message_block_underline"></div>


        <div class="col col_l left">
            <iframe class="presentation_prew_zone_video block" width="100%" height="100%"
                    src="https://www.youtube.com/embed/<?= $model->name ?>" frameborder="0" allowfullscreen></iframe>
        </div>

        <div class="col col_s right">

            <div class="block">
                <?php $form = ActiveForm::begin([
                    'enableAjaxValidation' => true,
                    'enableClientValidation' => false,
                    'validateOnChange' => true,
                ]); ?>
                <div class="form-group field-registrationform-email required add_message_form_video">
                    <?= $form->field($model, 'type')->hiddenInput(['value' => Files::TYPE_YOUTUBE])->label(false) ?>
                    <?= $form->field($model, 'image')->hiddenInput()->label(false) ?>
                    <?= $form->field($model, 'title')->label(false)->textInput(
                        [
                            'placeholder' => 'Название презентации...',
                            'class' => 'form-control',
                            'aria-required' => "true"
                        ]
                    ) ?>


                    <?= $form->field($model, 'cat_id')->label(false)->dropDownList(
                        \app\models\CategoriesDict::getCatsList(),
                        [
                            'placeholder' => 'Категория',
                            'class' => 'form-control',
                            'aria-required' => "true"
                        ]
                    ); ?>

                    <!--  <?= $form->field($model, 'type')->label(false)->dropDownList(
                        app\models\Files::getTypesList(),
                        array(
                            'placeholder' => 'Тип презентации...',
                            'class' => 'form-control',
                            'aria-required' => "true"
                        )
                    ); ?> -->
                    <?= $form->field($model, 'desc')->label(false)->textarea(
                        [
                            'placeholder' => 'Краткое описание...',
                            'class' => 'form-control',
                            'aria-required' => "true"
                        ]
                    ) ?>

                    <?= $form->field($model, 'tags')->widget(Select2::classname(), [
                        'data' => Tags::getTagsList(),
                        'showToggleAll' => false,
                        'options' => ['placeholder' => 'Выберите теги ...', 'multiple' => true,],
                        'pluginOptions' => [
                            'tags' => true,
                            'tokenSeparators' => [',', ' '],
                            'maximumInputLength' => 30
                        ],
                    ])->label(''); ?>
                    <?= Html::submitButton('Сохранить', ['class' => 'btn bg_primary transition']) ?>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>

    </div>
    <div class="clear"></div>


    <?php $this->endContent(); ?>

</div>