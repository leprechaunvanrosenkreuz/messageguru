<?php
/**
 * Created by PhpStorm.
 * User: marduk
 * Date: 25.03.17
 * Time: 13:18
 */

/* @var \app\models\Files $model */
/* @var User $user */

use yii\helpers\Url;
use app\components\widgets\ProfileLeftBar;
use app\modules\user\models\User;
use \app\models\Files;

;
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use \app\components\widgets\SearchWidget;

$this->registerJsFile('https://vk.com/js/api/share.js?93', ['position' => yii\web\View::POS_HEAD]);

$images = $model->getImages($model->id);

Yii::$app->view->registerMetaTag(['property' => 'og:title', 'content' => $model->title]);
Yii::$app->view->registerMetaTag(['property' => 'og:url', 'content' => Url::base(true)]);
Yii::$app->view->registerMetaTag(['property' => 'og:description', 'content' => $model->desc]);
Yii::$app->view->registerMetaTag(['property' => 'og:image', 'content' => $images[0]]);


$this->title = $model->title;
?>



<?php
$this->title = "Презентация " . $model->title;
$this->beginContent('@app/views/layouts/layout_main.php');
?>
<?php
$url = Yii::getAlias('@uploads_http/presentations/') . $model->name;
?>
<div class="search">
    <div class="wrapper">
        <?= SearchWidget::widget() ?>
    </div>
    <div class="search_mask transition"></div>
</div>

<?php if (Yii::$app->getSession()->hasFlash('success')) : ?>
    <div class="alert alert-success" role="alert"><?= Yii::$app->getSession()->getFlash('success', null, true) ?></div>
<?php endif; ?>

<?php if (Yii::$app->getSession()->hasFlash('error')) : ?>
    <div class="alert alert-danger" role="alert"><?= Yii::$app->getSession()->getFlash('error', null, true) ?></div>
<?php endif; ?>

<div class="noise_light overflow bordered message_view_container">
    <div class="wrapper">
        <div class="col col_l left message_block">

            <div class="block ">
                <h2 class="color_primary"><?= $model->title ?></h2>
                <div class="message_block_underline"></div>
                <p><?= $model->desc ?></p>

                <div class="message_counters">
                    <div>
                        <h3 class="color_primary"><i class="fa fa-calendar-o" aria-hidden="true"></i></h3><span
                            class="color_primary"><?= date('d.m.Y', strtotime($model->created_at)) ?></span>
                    </div>
                    <!--
                                        <div>
                                            <h3 class="color_primary">300</h3><span class="color_primary">PQ</span>
                                        </div>
                    -->
                    <div>
                        <h3 class="color_primary"><i class="fa fa-eye" aria-hidden="true"></i></h3><span
                            class="color_primary"><?= $model->views ?></span>
                    </div>
                </div>
                <?php if ($model->type == Files::TYPE_PRESENTATION) { ?>

                    <ul id="lightSlider">
                        <?php
                        foreach ($images AS $image) { ?>
                            <li data-thumb="<?= $image ?>" data-src="<?= $image ?>">
                                <img src="<?= $image ?>"/>
                            </li>
                        <?php } ?>
                    </ul>

                    <div class="count_slider_full">
                        <span id="current">1</span> из <span id="total"></span>
                    </div>
                <?php } ?>
                <?php if ($model->type == Files::TYPE_YOUTUBE) { ?>
                    <!--    <iframe width="480" height="360" src="<?= $model->name ?>" frameborder="0" allowfullscreen ></iframe> -->
                    <iframe width="100%" class="video_view" src="https://www.youtube.com/embed/<?= $model->name ?>"
                            frameborder="0" allowfullscreen></iframe>
                <?php } ?>

                <div class="message_block_controls overflow">

                    <?php if (!empty($model->filesTags)): ?>
                        <div class="message_block overflow" style="padding-bottom: 30px">
                            <div class="left">
                                <span class="color_primary">Хэштеги: </span>
                                <?php
                                /** @var \app\models\Tags $tag */
                                foreach ($model->filesTags as $tag):
                                    ?>
                                    <?= Html::a('#' . $tag->name, Url::to(['/presentation/tags/' . $tag->alias]), ['class' => 'label label-default']) ?>
                                <?php endforeach; ?>
                            </div>
                        </div>
                    <?php endif; ?>

                    <?php if ($model->type != Files::TYPE_YOUTUBE) : ?>
                        <a href="<?= $url ?>" class="left btn bg_primary">Скачать!</a>
                    <?php endif; ?>
                    <a href="#" class="left btn btn_border color_primary like" rel="<?= $model->id ?>">
                        Мне нравится
                        <i class="fa fa-thumbs-o-up" aria-hidden="true"></i>
                        <span><?= app\models\Likes::getCountLike($model->id) ?></span>
                    </a>

                    <div class="right share_message">
                        <span class="color_primary">Поделиться:</span>

                        <a href="#" class="transition" target="_blank" data-toggle="modal" data-target="#sendEmail"><i
                                class="fa fa-envelope-o"></i></a>
                        <?php
                        $params = http_build_query([
                            'url' => Url::to(['/pres/' . $model->id], true),
                            'title' => $model->title,
                            'image' => $model->getImageUrl(),
                            'description' => $model->desc
                        ]);
                        $vk_link = 'https://vk.com/share.php?' . $params;
                        echo Html::a('<i class="fa fa-vk"></i>', $vk_link, ['class' => 'transition', 'target' => '_blank'])
                        ?>

                        <?php
                        $params_fb = http_build_query([
                            'u' => Url::to(['/pres/' . $model->id], true),
                            'src' => 'sdkpreparse'
                        ]);
                        $fb_link = 'https://www.facebook.com/sharer/sharer.php?' . $params_fb;
                        ?>

                        <div class="fb-share-button" data-href="https://developers.facebook.com/docs/plugins/"
                             data-layout="button_count" data-size="small" data-mobile-iframe="true">
                            <?= Html::a('<i class="fa fa-facebook"></i>', $fb_link, ['class' => 'fb-xfbml-parse-ignore transition', 'target' => '_blank']) ?>
                        </div>
                    </div>
                </div>

            </div>


            <!-- Modal -->
            <div class="modal fade " id="sendEmail" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                 aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <?php $form = ActiveForm::begin(); ?>
                            <?= $form->field($model, 'mail')->label(false)->textInput(
                                [
                                    'placeholder' => 'Введите E-mail на который отправить презентацию...',
                                    'class' => 'form-control',
                                    'aria-required' => "true"
                                ]
                            ) ?>

                            <?= Html::submitButton('Отправить презентацию!', ['class' => 'btn bg_primary']) ?>
                            <?php ActiveForm::end(); ?>
                        </div>
                    </div>
                </div>
            </div>


        </div>
        <div class="col col_s right  ">
            <?= ProfileLeftBar::widget(['model_id' => $user->id, 'views' => "File_presentation_view"]) ?>

            <div class="recommendations noborder">
                <h2 class="bold_title">Рекомендации <span class="color_primary">MG для Вас:</span></h2>

                <?= \app\components\widgets\RecommendBar::widget() ?>
            </div>

        </div>
    </div>
    <div class="clear"></div>
</div>

<?php
$this->registerJs('

        var autoplaySlider = $(\'#lightSlider\').lightSlider({
            gallery:true,
            item:1,
            loop:false,
            thumbItem:6,
            slideMargin:0,
            mode: \'fade\',
            keyPress: true,
            thumbMargin: 7,
            enableDrag: false,
            currentPagerPosition:\'left\',
            onBeforeSlide: function (el) {
                $(\'#current\').text(el.getCurrentSlideCount());
            },
            onSliderLoad: function(el) {
                el.lightGallery({
                    selector: \'#lightSlider .lslide\'
                });
            }
        });
        $(\'#total\').text(autoplaySlider.getTotalSlideCount());

', yii\web\View::POS_READY);
?>

<?php $this->endContent(); ?>
