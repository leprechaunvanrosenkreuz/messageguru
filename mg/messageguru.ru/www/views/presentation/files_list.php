<?php
/**
 * Created by PhpStorm.
 * User: marduk
 * Date: 25.03.17
 * Time: 11:10
 */

/* @var \yii\data\ActiveDataProvider $filesProvider */


use yii\widgets\ListView;
use app\modules\user\models\User;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;
use \app\components\widgets\SearchWidget; 

$this->title = 'Все презентации';
?>

<?php $this->beginContent('@app/views/layouts/layout_main.php'); ?>


<div class="search">
    <div class="wrapper">
        <?= SearchWidget::widget() ?>
    </div>
    <div class="search_mask transition"></div>
</div>

<div class="noise_light overflow bordered">
    <div class="wrapper">
        <h1 style="display: none"><?= $this->title ?></h1>
        <?php Pjax::begin(); ?>
        <div class="preheader">
            <h2 class="left"><span class="color_primary"> </span> Messages:</h2>
            <div class="filters left">
                <?= Html::a("Все", ['presentation/index'], ['class' => 'filter ' . (($curr == 1) ? 'active_filter' : '')]) ?>
                <?= Html::a("Новые", ['presentation/filter-new'], ['class' => 'filter ' . (($curr == 2) ? 'active_filter' : '')]) ?>
                <?= Html::a("По читаемости", ['presentation/filter-popular'], ['class' => 'filter ' . (($curr == 3) ? 'active_filter' : '')]) ?>
            </div>


            <?= Html::a("Рулетка | Cлучайные 6", ['presentation/filter-rand'], ['class' => 'btn bg_primary transition get_random left']) ?>

            <div class="gun left fa fa-refresh"></div>
        </div>

        <div class="col col_l left autor_block">
            <div class="messages_container overflow">
                <?= ListView::widget([
                    'dataProvider' => $filesProvider,
                    'options' => [
                        'tag' => 'div'
                    ],
                    'layout' => "{items}\n <div class='clear'> {pager}</div> ",
                    'itemView' => function ($model, $key, $index, $widget) {
                        return $this->render('list_item',['model' => $model]);

                        // or just do some echo
                        // return $model->title . ' posted by ' . $model->author;
                    },
                    'itemOptions' => [
                        'tag' => false,
                    ],
                    'pager' => [
                        'maxButtonCount' => 5,
                        'options' => [
                            'class' => 'color_primary pagination',
                        ],

                        'activePageCssClass' => 'bg_primary active',
                    ],
                ]);
                ?>
            </div>
        </div>
        <div class="col col_s right recommendations">
            <h2 class="bold_title">Рекомендации <span class="color_primary">MG для Вас:</span></h2>
            <?= \app\components\widgets\RecommendBar::widget() ?>
            <div class="tags">
                <h2 class="bold_title">Популярные теги:</h2>

                <?= \app\components\widgets\TagsBar::widget() ?>
            </div>
        </div>
        <?php Pjax::end(); ?>
    </div>
</div>






<?php $this->endContent(); ?>
