<?php
/**
 * Created by PhpStorm.
 * User: marduk
 * Date: 25.03.17
 * Time: 11:10
 */

/* @var \yii\data\ActiveDataProvider $filesProvider */


use yii\widgets\ListView;
use app\modules\user\models\User;
use yii\helpers\Html;
use yii\helpers\Url;
use \app\components\widgets\ProfileLeftBar;
use \app\components\widgets\ProfileMenuBar;
use \app\components\widgets\SearchWidget; 

$this->title = "Спикер ". $model->getFullName($model->id);

?>

<?php $this->beginContent('@app/views/layouts/layout_main.php'); ?>
<div class="search">
    <div class="wrapper">
        <?= SearchWidget::widget() ?>
    </div>
    <div class="search_mask transition"></div>
</div>

<div class="noise_light overflow bordered">
    <div class="wrapper">
        <h1><?= $this->title ?></h1>
        <?= ProfileLeftBar::widget(['model_id' => $model->id]) ?>
        <div class="col col_l right autor_block">
            <div class="preheader">
                <h2 class="left"><span class="color_primary">Messages</span>:</h2>
                <div class="filters left">
                    <a class="filter active_filter" href="#">За сегодня</a>
                    <a class="filter" href="#">За месяц</a>
                    <a class="filter" href="#">За все время</a>
                </div>
            </div>
            <div class="messages_container overflow">
                <?= ListView::widget([
                    'dataProvider' => $usersProvider,
                    'options' => [
                        'tag' => 'div'
                    ],
                    'layout' => "{items}\n <div class='clear'> {pager}</div> ",
                    'itemView' => function ($model, $key, $index, $widget) {
                        return $this->render('list_item',['model' => $model]);

                        // or just do some echo
                        // return $model->title . ' posted by ' . $model->author;
                    },
                    'itemOptions' => [
                        'tag' => false,
                    ],
                    'pager' => [
                        'maxButtonCount' => 5,
                        'options' => [
                            'class' => 'color_primary pagination',
                        ],

                        'activePageCssClass' => 'bg_primary active',
                    ],
                ]);
                ?>
            </div>
        </div>


    </div>
</div>











<?php $this->endContent(); ?>
