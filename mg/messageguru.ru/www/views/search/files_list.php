<?php
/**
 * Created by PhpStorm.
 * User: marduk
 * Date: 25.03.17
 * Time: 11:10
 */

/* @var \yii\data\ActiveDataProvider $filesProvider */


use yii\widgets\ListView;
use app\modules\user\models\User;
use yii\helpers\Html;
use yii\helpers\Url;
use \app\components\widgets\SearchWidget;

?>

<?php $this->beginContent('@app/views/layouts/layout_main.php'); ?>


<div class="search">
    <div class="wrapper">
        <?= SearchWidget::widget() ?>
    </div>
    <div class="search_mask transition"></div>
</div>

<div class="noise_light overflow bordered">
    <div class="wrapper">
        <div class="col col_l left autor_block">
            <div class="messages_container overflow">
                <?php if(!empty($searchProvider)) : ?>
                <?= ListView::widget([
                    'dataProvider' => $searchProvider,
                    'options' => [
                        'tag' => 'div'
                    ],
                    'layout' => "{items}\n <div class='clear'> {pager}</div> ",
                    'itemView' => function ($model, $key, $index, $widget) {
                        return $this->render('list_item', ['model' => $model]);

                        // or just do some echo
                        // return $model->title . ' posted by ' . $model->author;
                    },
                    'itemOptions' => [
                        'tag' => false,
                    ],
                    'pager' => [
                        'maxButtonCount' => 5,
                        'options' => [
                            'class' => 'color_primary pagination',
                        ],

                        'activePageCssClass' => 'bg_primary active',
                    ],
                ]);
                ?>
                <?php else: ?>
                    <h2>По запросу <?= $query ?> ничего не найдено</h2>
                <?php endif; ?>
            </div>
        </div>
        <div class="col col_s right recommendations">
            <h2 class="bold_title">Рекомендации <span class="color_primary">MG для Вас:</span></h2>
            <?= \app\components\widgets\RecommendBar::widget() ?>
            <div class="tags">
                <h2 class="bold_title">Популярные теги:</h2>

                <?= \app\components\widgets\TagsBar::widget() ?>
            </div>
        </div>


    </div>
</div>


<?php $this->endContent(); ?>
