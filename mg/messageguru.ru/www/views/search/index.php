<?php
/**
 * Created by PhpStorm.
 * User: manchetten
 * Date: 28.06.17
 * Time: 16:49
 */

/* @var $this yii\web\View */
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;
use yii\widgets\ListView;
use app\modules\user\models\User;
use \app\components\widgets\SearchWidget;
use app\assets\AppAsset;

AppAsset::register($this);
$assetsUrl = AppAsset::getAssetsUrl();
?>

<?php $this->beginContent('@app/views/layouts/layout_main.php'); ?>


<div class="search">
    <div class="wrapper">
        <?= SearchWidget::widget() ?>
    </div>
    <div class="search_mask transition"></div>
</div>

<div class="noise_light overflow bordered">
    <div class="wrapper">
        <div class="col col_l left autor_block">
            <div class="messages_container overflow">
                <?php
                $cats_slugs = \app\models\CategoriesDict::getCatsList();
                if (!empty($hits)) {

                    echo ListView::widget([
                        'dataProvider' => $searchProvider,
                        'options' => [
                            'tag' => 'div'
                        ],
                        'layout' => "{items}\n <div class='clear'> {pager}</div> ",
                        'itemView' => '_file_item',
                        'itemOptions' => [
                            'tag' => false,
                        ],
                        'pager' => [
                            'maxButtonCount' => 5,
                            'options' => [
                                'class' => 'color_primary pagination',
                            ],

                            'activePageCssClass' => 'bg_primary active',
                        ],
                    ]);

//                    foreach ($hits as $hit) {
//                        echo $this->render('_file_item', ['hit' => (object)$hit]);
//                    }
                } else {
                    echo '<div class="block post_content">';
                    echo '<h1 class="not_found">По запросу "' . $query . '" ничего не найдено!</h1>';
                    echo '</div>';
                }
                ?>
            </div>
        </div>
        <div class="col col_s right recommendations">
            <h2 class="bold_title">Рекомендации <span class="color_primary">MG для Вас:</span></h2>
            <?= \app\components\widgets\RecommendBar::widget() ?>
            <div class="tags">
                <h2 class="bold_title">Популярные теги:</h2>

                <?= \app\components\widgets\TagsBar::widget() ?>
            </div>
        </div>


    </div>
</div>


<?php $this->endContent(); ?>
