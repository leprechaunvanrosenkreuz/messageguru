<?php
// _list_item.php

/* @var \app\models\Files $model */
use yii\helpers\Html;
use yii\helpers\Url;
use app\modules\user\models\User;
use app\components\widgets\LikesWidget;

echo \app\components\widgets\RenderFileItemWidget::widget(['model' => $model]);
?>
