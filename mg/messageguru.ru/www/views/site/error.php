<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;
use yii\helpers\Url;

$this->title = $name;
?>

<?php Html::encode($this->title) ?>

<?php nl2br(Html::encode($message)) ?>
<?php $this->beginContent('@app/views/layouts/layout_no_header.php'); ?>


    <div class="register_container login_container flex_container">
        <div class="outline">

            <div class="inline_logo">Message<span>GURU</span></div>

            <h1 class="error_page_numbers color_primary"><?= Html::encode($exception->statusCode) ?></h1>
            <?php if($exception->statusCode == 404)
            {
                echo '<p>К сожалению, запрашиваемая страница не найдена.</p>';
            } else {
                echo Html::encode(preg_replace("/[.]+$/", "", trim($exception->getMessage())));
            }
            ?>


            <?= Html::a('Вернуться на главную!', Url::home(), ['class' => 'btn bg_primary']) ?>

        </div>
    </div>


<?php $this->endContent(); ?>