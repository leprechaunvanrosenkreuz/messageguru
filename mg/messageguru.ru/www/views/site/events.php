<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'Events';
$this->params['breadcrumbs'][] = $this->title;
?>
<?php $this->beginContent('@app/views/layouts/layout_main.php'); ?>

    <div class="site-about">
        <h1><?= Html::encode($this->title) ?></h1>

        <p>
            This is the page. You may modify the following file to customize its content:
        </p>

        <code><?= __FILE__ ?></code>
    </div>
<?php $this->endContent(); ?>