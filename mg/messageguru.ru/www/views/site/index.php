<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;
use app\assets\AppAsset;
use app\modules\user\models\User;
use \app\models\Files;
use \app\components\widgets\SearchWidget;
use app\components\widgets\LikesWidget;

AppAsset::register($this);
$assetsUrl = AppAsset::getAssetsUrl();

/* @var $this yii\web\View */

$this->title = 'Сервис обмена презентациями';
?>

<?php $this->beginContent('@app/views/layouts/layout_main.php'); ?>

    <div class="search">
        <div class="wrapper">
            <?= SearchWidget::widget() ?>
        </div>
        <div class="search_mask transition"></div>
    </div>

    <div class="noise_light bg_sub overflow bordered">
        <div class="wrapper">
            <?php Pjax::begin(); ?>
            <div class="preheader">
                <h2 class="left"><span class="color_primary">Лучшие:</span></h2>
                <div class="filters left">
                    <?= Html::a("За сегодня", ['site/filter-today'], ['class' => 'filter ' . (($curr == 1) ? 'active_filter' : '')]) ?>
                    <?= Html::a("За месяц", ['site/filter-month'], ['class' => 'filter ' . (($curr == 2) ? 'active_filter' : '')]) ?>
                    <?= Html::a("За все время", ['site/filter-all'], ['class' => 'filter ' . (($curr == 3) ? 'active_filter' : '')]) ?>

                </div>

                <?= Html::a("Рулетка | Cлучайные 6", ['site/filter-rand'], ['class' => 'btn bg_primary transition get_random left']) ?>

                <div class="gun color_primary left fa fa-refresh"></div>
            </div>

            <div class="col col_l left ">
                <div class="messages_container overflow">
                    <?php foreach ($messages AS $file) {
                        echo \app\components\widgets\RenderFileItemWidget::widget(['model' => $file]);
                    } ?>
                </div>
            </div>
            <?php Pjax::end(); ?>
            <div class="col col_s right recommendations">
                <h2 class="bold_title">Рекомендации <span class="color_primary">MG для Вас:</span></h2>
                <?= \app\components\widgets\RecommendBar::widget() ?>


                <div class="tags">
                    <h2 class="bold_title">Популярные теги:</h2>
                    <?= \app\components\widgets\TagsBar::widget() ?>
                </div>
            </div>
            <div class="load_more">
                <span class="load_more_left left"></span>
                <div class="load_selector left">
                    <div></div>
                    <?= Html::a('Показать больше', Url::to(['/presentation']), ['class' => 'color_primary', 'data-pjax' => 0]) ?>
                </div>
                <span class="load_more_right left contain_right"></span>
            </div>
        </div>
    </div>

    <div class=" noise_light overflow">
        <div class="wrapper">
            <?php Pjax::begin(); ?>
            <div class="col col_l left autor_block">
                <div class="preheader">
                    <h2 class="left"><span class="color_primary">Топ</span> Авторов:</h2>
                    <div class="filters left">
                        <?= Html::a("За сегодня", ['site/filter-author-today'], ['class' => 'filter ' . (($curr_author == 1) ? 'active_filter' : '')]) ?>
                        <?= Html::a("За месяц", ['site/filter-author-month'], ['class' => 'filter ' . (($curr_author == 2) ? 'active_filter' : '')]) ?>
                        <?= Html::a("За все время", ['site/filter-author-all'], ['class' => 'filter ' . (($curr_author == 3) ? 'active_filter' : '')]) ?>

                    </div>
                </div>

                <div class="author_container overflow">
                    <?php
                    /** @var \app\modules\user\models\User $user */
                    foreach ($modelAuthor AS $user) { ?>
                        <div class="author">
                            <?= Html::a('<div class="author_photo" style="background-image: url(' . $user->getAvatarUrl() . ')" ></div>', Url::to(['/author/' . $user->id]), ['data-pjax' => 0]) ?>
                            <div class="bg_primary pq_counter"><?= $user->getCurrentUserTextLevel() ?></div>
                            <h3 class="author_name color_primary"><?= Html::a($user->first_name . ' ' . $user->last_name, Url::to(['/author/' . $user->id]), ['data-pjax' => 0]) ?></h3>
                        </div>
                    <?php } ?>
                </div>


                <div class="load_more">
                    <span class="load_more_left left"></span>
                    <div class="load_selector left">
                        <div></div>
                        <?= Html::a('Показать больше', Url::to(['/authors']), ['class' => 'color_primary', 'data-pjax' => 0]) ?>
                    </div>
                    <span class="load_more_right left"></span>
                </div>

            </div>
            <?php Pjax::end(); ?>
            <div class="col col_s right promo">
                <div class="block fst_adv" style="visibility: hidden">
                    Рекламный блок
                </div>
                <div class="block scn_adv bg_secondary" style="visibility: hidden"></div>
            </div>

        </div>

    </div>


<?php $this->endContent(); ?>