<?php

namespace app\modules\user\assets;


use yii;
use yii\web\AssetBundle;

class UserAsset extends AssetBundle
{
    public $sourcePath = '@app/modules/user/assets';

    public $css = [
        'css/style.css',
    ];
    public $js = [
        'js/script.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\web\JqueryAsset'
    ];

    public $jsOptions = [
        'position' => \yii\web\View::POS_END
    ];

    public static function getAssetsUrl()
    {
        return Yii::$app->assetManager->getPublishedUrl((new self())->sourcePath);
    }
}