<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;
use Yii;

class AppAsset extends AssetBundle
{
    public $sourcePath = '@app/assets';
    //public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $css = [
//        'css/bootstrap-custom.css',
          'css/reset.css',
        'css/global.css',
        'css/fonts/fonts.css',
        'css/mobile.css',
        'css/lightslider.css',
        'css/lightgallery.css',
//        'css/slick-theme.css',
//        'css/font-awesome/font-awesome.css',
        //'css/ion.rangeSlider.skinSKB.css',
    ];
    public $js = [
        'js/script.js',
        'js/geometry.js',
        'js/lightgallery.js',
        'js/lightslider.js',
//        'js/jquery.mousewheel.min.js',
//        'js/jquery.ba-throttle-debounce.min.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap\BootstrapPluginAsset',
        'yii\web\JqueryAsset',
        '\rmrevin\yii\fontawesome\AssetBundle'
    ];

    public $jsOptions = [
        'position' => \yii\web\View::POS_END
    ];

    public static function getAssetsUrl()
    {
        return Yii::$app->assetManager->getPublishedUrl((new self())->sourcePath);
    }
}